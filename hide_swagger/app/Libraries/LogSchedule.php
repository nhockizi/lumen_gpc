<?php
    namespace App\Libraries;
    class LogSchedule
    {
        public static function Log($message,$data = null){
            return [
                'time' => date('Y-m-d H:i:s'),
                'message' => $message,
                'data' => $data
            ];
        }
    }
