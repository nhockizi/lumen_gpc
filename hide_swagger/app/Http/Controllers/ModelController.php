<?php

namespace App\Http\Controllers;

use App\Http\Controllers\TraitController\ErrorTrait;
use App\Repositories\ModelRepo;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Validator;

class ModelController extends Controller
{
    protected $_models;
    use ErrorTrait;
    public function __construct(
        ModelRepo $modelsRepo
    ) {
        $this->_models = $modelsRepo;
    }
    /**
     * @OA\Get(
     *     path="/model/list",
     *     summary="List all Model",
     *     tags={"Model"},
     *     operationId="model_list",
     *     security={{"Bearer":{}}},
     *     @OA\Parameter(
     *         description="page",
     *         in="query",
     *         name="page",
     *         @OA\Schema(
     *           type="integer",
     *           format="int32"
     *         )
     *     ),
     *     @OA\Parameter(
     *         description="number item length",
     *         in="query",
     *         name="length",
     *         @OA\Schema(
     *           type="integer",
     *           format="int32"
     *         )
     *     ),
     *     @OA\Parameter(
     *         description="brand id",
     *         in="query",
     *         name="brand_id",
     *         @OA\Schema(
     *           type="string",
     *         )
     *     ),
     *     @OA\Parameter(
     *         description="parent id",
     *         in="query",
     *         name="parent_id",
     *         @OA\Schema(
     *           type="string",
     *         )
     *     ),
     *     @OA\Response(
     *          response="200",
     *          description="{
                    'status': true,
                    'message': null,
                    'data': object
                }",
     *     ),
     *     @OA\Response(
     *          response="401",
     *          description="{
                    'status': false,
                    'message': 'Invalid token',
                    'data': []
                }",
     *     ),
     * )
     */
    public function list(Request $request)
    {
        try {
            $request_all = $request->all();
            $data        = [
                'length'        => array_get($request_all, 'length', null),
                'filter_search' => array_get($request_all, 'filter_search', null),
                'order'         => array_get($request_all, 'order', null),
                'type'          => array_get($request_all, 'type', null),
            ];
            $result      = $this->_models->list($data);

            return $this->response($result);
        } catch (\Exception $e) {

            return $this->response($e->getMessage(), Response::HTTP_BAD_REQUEST);
        }
    }

    public function delete($id)
    {
        DB::beginTransaction();
        try {
            $result = $this->_models->find($id);
            if (!$result) {
                throw new \Exception(trans('messages.data_not_found'));
            }
            $result->delete();
            DB::commit();
            $message = trans('messages.delete_success');

            return $this->response($message);
        } catch (\Exception $e) {
            DB::rollBack();

            return $this->response($e->getMessage(), Response::HTTP_BAD_REQUEST);
        }
    }

    public function create(Request $request)
    {
        $data    = $this->_data();
        $message = '';
        $this->_validate($data, $message);
        if ($message != '') {
            return $this->response(
                $message,
                Response::HTTP_INTERNAL_SERVER_ERROR
            );
        }
        DB::beginTransaction();
        try {
            $result = $this->_models->_save($data);
            DB::commit();
            $message = trans('messages.create_success');

            return $this->response($result, Response::HTTP_OK, $message);
        } catch (\Exception $e) {
            DB::rollBack();

            return $this->response($e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    private function _data($id = null)
    {
        $request     = Request::capture();
        $request_all = $request->all();
        $result      = [
            'id'           => $id,
            'name'         => array_get($request_all, 'name', null),
            'brand'        => array_get($request_all, 'brand', null),
            'model_parent' => array_get($request_all, 'model_parent', null),
        ];

        return $result;
    }

    private function _validate($data, &$message)
    {
        $rules = [
            'name'         => [
                'required',
            ],
            'brand'        => [
                'required',
                'exists:brand,brand_ID',
            ],
        ];
        $this->_validate_error($data, $rules, $message);

        return $message;
    }

    public function update($id)
    {
        $data    = $this->_data($id);
        $message = '';
        $this->_validate($data, $message);
        if ($message != '') {
            return $this->response(
                $message,
                Response::HTTP_INTERNAL_SERVER_ERROR
            );
        }
        DB::beginTransaction();
        try {
            $result = $this->_models->_save($data);
            DB::commit();
            $message = trans('messages.update_success');

            return $this->response($result, Response::HTTP_OK, $message);
        } catch (\Exception $e) {
            DB::rollBack();

            return $this->response($e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
}