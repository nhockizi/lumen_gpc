<?php

namespace App\Http\Controllers;

use App\Http\Controllers\TraitController\ErrorTrait;
use App\Repositories\ActionsRoleRepo;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Validator;

class RolesPermissionController extends Controller
{
    public $_action_role;

    public function __construct(
        ActionsRoleRepo $actionsRoleRepo
    ) {
        $this->_action_role = $actionsRoleRepo;
    }

    use ErrorTrait;

    public function list(Request $request)
    {
        try {
            $request_all = $request->all();
            $data        = [
                'length' => array_get($request_all, 'length', null),
                'order'  => array_get($request_all, 'order', null),
                'type'   => array_get($request_all, 'type', null),
            ];
            $result      = $this->_action_role->list($data);

            return $this->response($result);
        } catch (\Exception $e) {
            return $this->response($e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    private function _validate($data)
    {
        $rules = [
            'name' => [
                'required',
            ],
        ];
        $this->_validate_error($data, $rules, $message);
        return $message;
    }

    private function _data($id = null)
    {
        $request_all = Request::capture()->all();
        $data        = [
            'id'   => $id,
            'name' => array_get($request_all, 'name', null),
        ];
        return $data;
    }

    public function create()
    {
        $data    = $this->_data();
        $message = $this->_validate($data);
        if ($message != '') {
            return $this->response(
                $message,
                HTTP_INTERNAL_SERVER_ERROR
            );
        }
        try {
            $result  = $this->_action_role->create($data);
            $message = trans('messages.create_success');
            return $this->response($result, HTTP_OK, $message);
        } catch (\Exception $e) {
            return $this->response($e->getMessage(), HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function update($id)
    {
        $check = $this->_action_role->find($id);
        if (!$check) {
            return $this->response(trans('data_not_found'), HTTP_BAD_REQUEST);
        }
        $data    = $this->_data($id);
        $message = $this->_validate($data);
        if ($message != '') {
            return $this->response(
                $message,
                HTTP_INTERNAL_SERVER_ERROR
            );
        }
        try {
            $result  = $this->_action_role->update($data, $id);
            $message = trans('messages.update_success');
            return $this->response($result, HTTP_OK, $message);
        } catch (\Exception $e) {
            return $this->response($e->getMessage(), HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function get($roleID)
    {
        try {
            $result = $this->_action_role->find($roleID);
            if (!$result) {
                return $this->response(trans('data_not_found'), HTTP_BAD_REQUEST);
            }
            return $this->response($result);
        } catch (\Exception $e) {

            return $this->response($e->getMessage(), HTTP_BAD_REQUEST);
        }
    }

}