<?php

namespace App\Http\Controllers;

use App\Repositories\BrandRepo;
use App\Repositories\ProductRepo;
use App\Repositories\ShippingMethodRepo;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Validator;

class ShippingMethodController extends Controller
{
    protected $_shippingMethod;

    public function __construct(
        ShippingMethodRepo $shippingMethodRepo
    ) {
        $this->_shippingMethod = $shippingMethodRepo;
    }
    /**
     * @OA\Get(
     *     path="/shipping-method/list",
     *     summary="List all Shipping Method",
     *     tags={"Shipping Method"},
     *     operationId="shipping_method_list",
     *     security={{"Bearer":{}}},
     *     @OA\Parameter(
     *         description="page",
     *         in="query",
     *         name="page",
     *         @OA\Schema(
     *           type="integer",
     *           format="int32"
     *         )
     *     ),
     *     @OA\Parameter(
     *         description="number item length",
     *         in="query",
     *         name="length",
     *         @OA\Schema(
     *           type="integer",
     *           format="int32"
     *         )
     *     ),
     *     @OA\Parameter(
     *         description="name",
     *         in="query",
     *         name="name",
     *         @OA\Schema(
     *           type="string",
     *         )
     *     ),
     *     @OA\Response(
     *          response="200",
     *          description="{
    'status': true,
    'message': null,
    'data': object
    }",
     *     ),
     *     @OA\Response(
     *          response="401",
     *          description="{
    'status': false,
    'message': 'Invalid token',
    'data': []
    }",
     *     ),
     * )
     */
    public function list(){
        try {
            $result = $this->_shippingMethod->list();

            return $this->response($result);
        } catch (\Exception $e) {

            return $this->response($e->getMessage(), Response::HTTP_BAD_REQUEST);
        }
    }
}