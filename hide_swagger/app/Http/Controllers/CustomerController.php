<?php

namespace App\Http\Controllers;

use App\Models\Customer;
use App\Repositories\CustomerRepo;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Validator;

class CustomerController extends Controller
{
    protected $_customer;

    public function __construct(
        CustomerRepo $customerRepo
    ) {
        $this->_customer = $customerRepo;
    }

    /**
     * @OA\Get(
     *     path="/customer/list",
     *     summary="List all Customer",
     *     tags={"Customer"},
     *     operationId="customer_list",
     *     security={{"Bearer":{}}},
     *     @OA\Parameter(
     *         description="page",
     *         in="query",
     *         name="page",
     *         @OA\Schema(
     *           type="integer",
     *           format="int32"
     *         )
     *     ),
     *     @OA\Parameter(
     *         description="number item length",
     *         in="query",
     *         name="length",
     *         @OA\Schema(
     *           type="integer",
     *           format="int32"
     *         )
     *     ),
     *     @OA\Parameter(
     *         description="customer id",
     *         in="query",
     *         name="customer_id",
     *         @OA\Schema(
     *           type="string",
     *         )
     *     ),
     *     @OA\Parameter(
     *         description="customer name",
     *         in="query",
     *         name="customer_name",
     *         @OA\Schema(
     *           type="string",
     *         )
     *     ),
     *     @OA\Response(
     *          response="200",
     *          description="{
    'status': true,
    'message': null,
    'data': object
    }",
     *     ),
     *     @OA\Response(
     *          response="401",
     *          description="{
    'status': false,
    'message': 'Invalid token',
    'data': []
    }",
     *     ),
     * )
     */
    public function list()
    {
        try {
            $result = $this->_customer->list();

            return $this->response($result);
        } catch (\Exception $e) {

            return $this->response($e->getMessage(), Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * @OA\Post(
     *     path="/customer/check-email",
     *     summary="Check Email Customer",
     *     tags={"Customer"},
     *     operationId="customer_checkEmail",
     *     security={{"Bearer":{}}},
     *     @OA\Parameter(
     *         description="Email",
     *         in="query",
     *         name="email",
     *         @OA\Schema(
     *           type="string",
     *         )
     *     ),
     *     @OA\Response(
     *          response="200",
     *          description="{
    'status': true,
    'message': null,
    'data': object
    }",
     *     ),
     *     @OA\Response(
     *          response="401",
     *          description="{
    'status': false,
    'message': 'Invalid token',
    'data': []
    }",
     *     ),
     * )
     */
    public function checkEmail(Request $request)
    {
        try {
            $request_all = $request->all();
            $email       = array_get($request_all, 'email', null);
            $check       = Customer::where('email', $email)->first();
            if ($check) {
                $message = trans('messages.email_has_existed_already');
            }
            else {
                $message = trans('messages.email_available');
            }

            return $this->response($check, HTTP_OK, $message);
        } catch (\Exception $e) {

            return $this->response($e->getMessage(), Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * @OA\Post(
     *     path="/customer/check-post-code",
     *     summary="Check Post Code",
     *     tags={"Customer"},
     *     operationId="customer_checkPostCode",
     *     security={{"Bearer":{}}},
     *     @OA\Parameter(
     *         description="Post Code",
     *         in="query",
     *         name="post_code",
     *         @OA\Schema(
     *           type="string",
     *         )
     *     ),
     *     @OA\Parameter(
     *         description="Address",
     *         in="query",
     *         name="address",
     *         @OA\Schema(
     *           type="string",
     *         )
     *     ),
     *     @OA\Response(
     *          response="200",
     *          description="{
    'status': true,
    'message': null,
    'data': object
    }",
     *     ),
     *     @OA\Response(
     *          response="401",
     *          description="{
    'status': false,
    'message': 'Invalid token',
    'data': []
    }",
     *     ),
     * )
     */
    public function checkPostCode(Request $request)
    {
        try {
            $request_all = $request->all();
            $post_code   = array_get($request_all, 'post_code', null);
            $address     = array_get($request_all, 'address', null);
            $curl        = curl_init();
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt(
                $curl,
                CURLOPT_URL,
                'https://api.postcode.nl/rest/addresses/' . $post_code . '/' . $address
            );
            curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt(
                $curl,
                CURLOPT_USERPWD,
                "tR0HN9R5exfekc8BKt5ekVk0hyQnfzyXdrtqmtKpGu2:C5auzgWTYTV7ScdSkq8ZUe31rsoemOWMbpUEWXxWJYFBWGOU3r"
            );
            $return_data = curl_exec($curl);
            curl_close($curl);

            $message = null;
            if (isset($return_data->postcode)) {
                if ($return_data->postcode != $post_code) {
                    $message = 'post code is not exists';
                }
            }
            else {
                $message = 'post code is not exists';
            }

            $return_data = json_decode($return_data);

            return $this->response($return_data, HTTP_OK, $message);
        } catch (\Exception $e) {

            return $this->response($e->getMessage(), Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * @OA\Get(
     *     path="/customer/detail/{customer_id}",
     *     summary="Detail Customer",
     *     tags={"Customer"},
     *     operationId="customer_detail",
     *     security={{"Bearer":{}}},
     *     @OA\Parameter(
     *         description="Customer id",
     *         in="path",
     *         name="customer_id",
     *         @OA\Schema(
     *           type="integer",
     *           format="int32"
     *         )
     *     ),
     *     @OA\Response(
     *          response="200",
     *          description="{
    'status': true,
    'message': null,
    'data': object
    }",
     *     ),
     *     @OA\Response(
     *          response="401",
     *          description="{
    'status': false,
    'message': 'Invalid token',
    'data': []
    }",
     *     ),
     * )
     */
    public function get($id)
    {
        try {
            $result = $this->_customer->detail($id);
            if (!$result) {
                throw new \Exception(trans('messages.data_not_found'));
            }

            return $this->response($result);
        } catch (\Exception $e) {

            return $this->response($e->getMessage(), Response::HTTP_BAD_REQUEST);
        }
    }

    public function update($id)
    {
        $request     = Request::capture();
        $request_all = $request->all();
        DB::beginTransaction();
        try {
            $data = [
                'name'          => array_get($request_all, 'name', null),
                'VAT'           => array_get($request_all, 'vat', null),
                'contact_name'  => array_get($request_all, 'contact_name', null),
                'email'         => array_get($request_all, 'email', null),
                'billing_email' => array_get($request_all, 'billing_email', null),
            ];

            $result = $this->_customer->_updateById($id, $data);

            DB::commit();
            $messages = trans('messages.update_success');

            return $this->response($result, Response::HTTP_OK, $messages);
        } catch (\Exception $e) {
            DB::rollBack();

            return $this->response($e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function delete($id)
    {
        DB::beginTransaction();
        try {
            $result = $this->_customer->delete($id);

            DB::commit();
            $messages = trans('messages.delete_success');

            return $this->response($result, Response::HTTP_OK, $messages);
        } catch (\Exception $e) {
            DB::rollBack();

            return $this->response($e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
}