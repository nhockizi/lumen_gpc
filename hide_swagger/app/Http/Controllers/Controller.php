<?php

namespace App\Http\Controllers;

use App\Libraries\Helpers;
use App\Services\Response\ResponseFacade;
use App\Services\UserLoggedIn\UserLoggedInFacade;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request;
use Illuminate\Validation\ValidationException;
use Laravel\Lumen\Routing\Controller as BaseController;
use Tymon\JWTAuth\Facades\JWTAuth;
use Illuminate\Http\Response;
use Illuminate\Validation\Validator;

class Controller extends BaseController
{
    /**
     * @param array|object|string|\Exception $data
     * @param int $code
     * @param null $message
     * @return mixed
     */
    public function response($data, $code=HTTP_OK, $message=null)
    {
        return ResponseFacade::send($data, $code, $message);
    }

    public function responseForward($result)
    {
        $statusCode = $result['statusCode']??Response::HTTP_OK;
        if($statusCode != Response::HTTP_OK){
            $response = $result['message']??'';
        }else{
            $response = $result['data']??'';
        }
        return response()->json($response);
    }

    /**
     * @param \Exception $exception
     * @return string
     */
    public function getExceptionMessage($exception)
    {
        return $exception->getMessage().' in '.$exception->getFile().' at '.$exception->getLine();
    }

    public function authUser()
    {
        // and you can continue to chain methods
        $user = UserLoggedInFacade::getUser();
        return $user;
    }

    /**
     * @return Client
     */
    public function createRestful(){
        return Helpers::createRestFul();
    }

    public function createRestfulRequest($method, $url){
        return Helpers::createRestfulRequest($method, $url);
    }

    /**
     * @param Validator|\Illuminate\Contracts\Validation\Validator $validator
     * @return mixed
     */
    public function getValidatorMessage($validator)
    {
        if( $validator->fails() )
        {
            $messages = $validator->errors()->all();
            foreach( $messages as $message )
            {
                $msg = $message;
                break;
            }
        }

        return $msg;
    }
}
