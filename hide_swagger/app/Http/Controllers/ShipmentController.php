<?php

namespace App\Http\Controllers;

use App\Repositories\ShipmentRepo;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Validator;

class ShipmentController extends Controller
{
    protected $_shipment;

    public function __construct(
        ShipmentRepo $shipmentRepo
    ) {
        $this->_shipment = $shipmentRepo;
    }

    /**
     * @OA\Get(
     *     path="/shipment/list",
     *     summary="List all Shipment",
     *     tags={"Shipment"},
     *     operationId="shipment_list",
     *     security={{"Bearer":{}}},
     *     @OA\Parameter(
     *         description="page",
     *         in="query",
     *         name="page",
     *         @OA\Schema(
     *           type="integer",
     *           format="int32"
     *         )
     *     ),
     *     @OA\Parameter(
     *         description="number item length",
     *         in="query",
     *         name="length",
     *         @OA\Schema(
     *           type="integer",
     *           format="int32"
     *         )
     *     ),
     *     @OA\Parameter(
     *         description="Invoice id",
     *         in="query",
     *         name="invoice_id",
     *         @OA\Schema(
     *           type="string",
     *         )
     *     ),
     *     @OA\Parameter(
     *         description="Customer email",
     *         in="query",
     *         name="customer_email",
     *         @OA\Schema(
     *           type="string",
     *         )
     *     ),
     *     @OA\Parameter(
     *         description="Order by",
     *         in="query",
     *         name="order",
     *         @OA\Schema(
     *           type="string",
     *         )
     *     ),
     *     @OA\Parameter(
     *         description="Order type",
     *         in="query",
     *         name="type",
     *         @OA\Schema(
     *           type="string",
     *         )
     *     ),
     *     @OA\Response(
     *          response="200",
     *          description="{
    'status': true,
    'message': null,
    'data': object
    }",
     *     ),
     *     @OA\Response(
     *          response="401",
     *          description="{
    'status': false,
    'message': 'Invalid token',
    'data': []
    }",
     *     ),
     * )
     */
    public function list(Request $request)
    {
        try {
            $request_all = $request->all();
            $data        = [
                'length'        => array_get($request_all, 'length', null),
                'filter_search' => array_get($request_all, 'filter_search', null),
                'order'         => array_get($request_all, 'order', null),
                'type'          => array_get($request_all, 'type', null),
            ];
            $result      = $this->_shipment->list($data);

            return $this->response($result);
        } catch (\Exception $e) {

            return $this->response($e->getMessage(), Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * @OA\Get(
     *     path="/shipment/detail/{shipment_id}",
     *     summary="Detail Shipment",
     *     tags={"Shipment"},
     *     operationId="shipment_detail",
     *     security={{"Bearer":{}}},
     *     @OA\Parameter(
     *         description="Shipment id",
     *         in="path",
     *         name="shipment_id",
     *         @OA\Schema(
     *           type="integer",
     *           format="int32"
     *         )
     *     ),
     *     @OA\Response(
     *          response="200",
     *          description="{
    'status': true,
    'message': null,
    'data': object
    }",
     *     ),
     *     @OA\Response(
     *          response="401",
     *          description="{
    'status': false,
    'message': 'Invalid token',
    'data': []
    }",
     *     ),
     * )
     */
    public function get($id)
    {
        try {
            $result = $this->_shipment->detail($id);
            if (!$result) {
                throw new \Exception(trans('messages.data_not_found'));
            }

            return $this->response($result);
        } catch (\Exception $e) {

            return $this->response($e->getMessage(), Response::HTTP_BAD_REQUEST);
        }
    }

    public function delete($id)
    {
        DB::beginTransaction();
        try {
            $result = $this->_shipment->find($id);
            if (!$result) {
                throw new \Exception(trans('messages.data_not_found'));
            }
            $result->delete();
            DB::commit();
            $message = trans('messages.delete_success');

            return $this->response($message);
        } catch (\Exception $e) {
            DB::rollBack();

            return $this->response($e->getMessage(), Response::HTTP_BAD_REQUEST);
        }
    }
}