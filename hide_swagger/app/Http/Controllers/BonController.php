<?php

namespace App\Http\Controllers;

use App\Http\Controllers\TraitController\ErrorTrait;
use App\Jobs\SentMailJob;
use App\Libraries\Helpers;
use App\Mail\Proforma;
use App\Mail\ProformaMail;
use App\Models\OrderStatus;
use App\Models\ProformaInvoiceEmail;
use App\Models\ProformaInvoiceSetting;
use App\Models\ProformaInvoiceTerms;
use App\Models\smtp;
use App\Repositories\BonRepo;
use App\Repositories\CustomerAddressRepo;
use App\Repositories\CustomerRepo;
use App\Repositories\InvoiceRepo;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Mpdf\Mpdf;
use Validator;

class BonController extends Controller
{
    protected $_bon;
    protected $_path;
    use ErrorTrait;

    public function __construct(
        BonRepo $bonRepo
    ) {
        $this->_bon  = $bonRepo;
        $this->_path = storage_path('app/public/');
    }

    public function generalPostOrderToday()
    {
        try {
            $result = $this->_bon->generalPostOrderToday();

            return $this->response($result);
        } catch (\Exception $e) {

            return $this->response($e->getMessage(), Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * @OA\Get(
     *     path="/bon/list-post-todo",
     *     summary="List Post Todo",
     *     tags={"Invoice"},
     *     operationId="invoice_listPostTodo",
     *     security={{"Bearer":{}}},
     *     @OA\Response(
     *          response="200",
     *          description="{
    'status': true,
    'message': null,
    'data': object
    }",
     *     ),
     *     @OA\Response(
     *          response="401",
     *          description="{
    'status': false,
    'message': 'Invalid token',
    'data': []
    }",
     *     ),
     * )
     */
    public function listPostTodo()
    {
        try {
            $result = $this->_bon->list(INVOICE_TYPE_TODO);

            return $this->response($result);
        } catch (\Exception $e) {

            return $this->response($e->getMessage(), Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * @OA\Get(
     *     path="/bon/list-post-doing",
     *     summary="List Post Doing",
     *     tags={"Invoice"},
     *     operationId="invoice_listPostDoing",
     *     security={{"Bearer":{}}},
     *     @OA\Response(
     *          response="200",
     *          description="{
    'status': true,
    'message': null,
    'data': object
    }",
     *     ),
     *     @OA\Response(
     *          response="401",
     *          description="{
    'status': false,
    'message': 'Invalid token',
    'data': []
    }",
     *     ),
     * )
     */
    public function listPostDoing()
    {
        try {
            $result = $this->_bon->list(INVOICE_TYPE_DOING);

            return $this->response($result);
        } catch (\Exception $e) {

            return $this->response($e->getMessage(), Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * @OA\Get(
     *     path="/bon/list-post-review",
     *     summary="List Post Review",
     *     tags={"Invoice"},
     *     operationId="invoice_listPostReview",
     *     security={{"Bearer":{}}},
     *     @OA\Response(
     *          response="200",
     *          description="{
    'status': true,
    'message': null,
    'data': object
    }",
     *     ),
     *     @OA\Response(
     *          response="401",
     *          description="{
    'status': false,
    'message': 'Invalid token',
    'data': []
    }",
     *     ),
     * )
     */
    public function listPostReview()
    {
        try {
            $result = $this->_bon->list(INVOICE_TYPE_REVIEW);

            return $this->response($result);
        } catch (\Exception $e) {

            return $this->response($e->getMessage(), Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * @OA\Get(
     *     path="/bon/list-post-done",
     *     summary="List Post Done",
     *     tags={"Invoice"},
     *     operationId="invoice_listPostDone",
     *     security={{"Bearer":{}}},
     *     @OA\Response(
     *          response="200",
     *          description="{
    'status': true,
    'message': null,
    'data': object
    }",
     *     ),
     *     @OA\Response(
     *          response="401",
     *          description="{
    'status': false,
    'message': 'Invalid token',
    'data': []
    }",
     *     ),
     * )
     */
    public function listPostDone()
    {
        try {
            $result = $this->_bon->list(INVOICE_TYPE_DONE);

            return $this->response($result);
        } catch (\Exception $e) {

            return $this->response($e->getMessage(), Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * @OA\Get(
     *     path="/bon/list-verkoop-orders",
     *     summary="List Verkoop Order",
     *     tags={"Invoice"},
     *     operationId="invoice_listVerkoopOrders",
     *     security={{"Bearer":{}}},
     *     @OA\Response(
     *          response="200",
     *          description="{
    'status': true,
    'message': null,
    'data': object
    }",
     *     ),
     *     @OA\Response(
     *          response="401",
     *          description="{
    'status': false,
    'message': 'Invalid token',
    'data': []
    }",
     *     ),
     * )
     */
    public function listVerkoopOrders()
    {
        try {
            $result = $this->_bon->list(INVOICE_TYPE_VERKOOP_ORDER);

            return $this->response($result);
        } catch (\Exception $e) {

            return $this->response($e->getMessage(), Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * @OA\Get(
     *     path="/bon/list-post-orders",
     *     summary="List Post Order",
     *     tags={"Invoice"},
     *     operationId="invoice_listPostOrders",
     *     security={{"Bearer":{}}},
     *     @OA\Response(
     *          response="200",
     *          description="{
    'status': true,
    'message': null,
    'data': object
    }",
     *     ),
     *     @OA\Response(
     *          response="401",
     *          description="{
    'status': false,
    'message': 'Invalid token',
    'data': []
    }",
     *     ),
     * )
     */
    public function listPostOrders()
    {
        try {
            $result = $this->_bon->list(INVOICE_TYPE_POST_ORDER);

            return $this->response($result);
        } catch (\Exception $e) {

            return $this->response($e->getMessage(), Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * @OA\Get(
     *     path="/bon/list-invoice-magento",
     *     summary="List Invoice Magento",
     *     tags={"Invoice"},
     *     operationId="invoice_listInvoiceMagento",
     *     security={{"Bearer":{}}},
     *     @OA\Response(
     *          response="200",
     *          description="{
    'status': true,
    'message': null,
    'data': object
    }",
     *     ),
     *     @OA\Response(
     *          response="401",
     *          description="{
    'status': false,
    'message': 'Invalid token',
    'data': []
    }",
     *     ),
     * )
     */
    public function listInvoiceMagento()
    {
        try {
            $result = $this->_bon->list(INVOICE_TYPE_MAGENTO_ORDER);

            return $this->response($result);
        } catch (\Exception $e) {

            return $this->response($e->getMessage(), Response::HTTP_BAD_REQUEST);
        }
    }

    public function general()
    {
        try {
            $result = $this->_bon->general();

            return $this->response($result);
        } catch (\Exception $e) {

            return $this->response($e->getMessage(), Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * @OA\Get(
     *     path="/bon/list-transaction-invoice",
     *     summary="List all transaction invoice",
     *     tags={"Invoice"},
     *     operationId="invoice_listTransactionInvoice",
     *     security={{"Bearer":{}}},
     *     @OA\Parameter(
     *         description="page",
     *         in="query",
     *         name="page",
     *         @OA\Schema(
     *           type="integer",
     *           format="int32"
     *         )
     *     ),
     *     @OA\Parameter(
     *         description="number item length",
     *         in="query",
     *         name="length",
     *         @OA\Schema(
     *           type="integer",
     *           format="int32"
     *         )
     *     ),
     *     @OA\Response(
     *          response="200",
     *          description="{
    'status': true,
    'message': null,
    'data': object
    }",
     *     ),
     *     @OA\Response(
     *          response="401",
     *          description="{
    'status': false,
    'message': 'Invalid token',
    'data': []
    }",
     *     ),
     * )
     */
    public function listTransactionInvoice(Request $request)
    {
        try {
            $request_all = $request->all();
            $data        = [
                'length'       => array_get($request_all, 'length', null),
                'product_name' => array_get($request_all, 'product_name', null),
            ];
            $result      = $this->_bon->listTransactionInvoice($data);

            return $this->response($result);
        } catch (\Exception $e) {

            return $this->response($e->getMessage(), Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * @OA\Get(
     *     path="/bon/terms",
     *     summary="Get Terms",
     *     tags={"Invoice"},
     *     operationId="invoice_terms",
     *     security={{"Bearer":{}}},
     *     @OA\Response(
     *          response="200",
     *          description="{
    'status': true,
    'message': null,
    'data': object
    }",
     *     ),
     *     @OA\Response(
     *          response="401",
     *          description="{
    'status': false,
    'message': 'Invalid token',
    'data': []
    }",
     *     ),
     * )
     */
    public function terms()
    {
        try {
            $result = ProformaInvoiceTerms::first();

            return $this->response($result);
        } catch (\Exception $e) {

            return $this->response($e->getMessage(), Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * @OA\Get(
     *     path="/bon/email-content",
     *     summary="Get Terms",
     *     tags={"Invoice"},
     *     operationId="invoice_emailContent",
     *     security={{"Bearer":{}}},
     *     @OA\Response(
     *          response="200",
     *          description="{
    'status': true,
    'message': null,
    'data': object
    }",
     *     ),
     *     @OA\Response(
     *          response="401",
     *          description="{
    'status': false,
    'message': 'Invalid token',
    'data': []
    }",
     *     ),
     * )
     */
    public function emailContent()
    {
        try {
            $result = ProformaInvoiceEmail::first();

            return $this->response($result);
        } catch (\Exception $e) {

            return $this->response($e->getMessage(), Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * @OA\Get(
     *     path="/bon/list-transaction-invoice/general",
     *     summary="Master Data list transaction invoice",
     *     tags={"Invoice"},
     *     operationId="invoice_masterData",
     *     security={{"Bearer":{}}},
     *     @OA\Response(
     *          response="200",
     *          description="{
    'status': true,
    'message': null,
    'data': object
    }",
     *     ),
     *     @OA\Response(
     *          response="401",
     *          description="{
    'status': false,
    'message': 'Invalid token',
    'data': []
    }",
     *     ),
     * )
     */
    public function masterData()
    {
        try {
            $result = $this->_bon->masterData();

            return $this->response($result);
        } catch (\Exception $e) {

            return $this->response($e->getMessage(), Response::HTTP_BAD_REQUEST);
        }
    }

    public function downloadPDF(Request $request)
    {
        $request_all = $request->all();
        $data        = $this->setDataPDF($request_all);
        $messages    = '';
        $this->_validateGeneratePDF($data, $messages);
        if (
            $messages !=
            ''
        ) {
            return $this->response(
                $messages,
                Response::HTTP_INTERNAL_SERVER_ERROR
            );
        }
        try {
            $result    = $this->_bon->generatePDF($data);
            $pdf       = $this->_generatePDF($result);
            $timeNow   = date('YmdHis');
            $file_name = $timeNow.
                         '-'.
                         strtotime('now').
                         '.pdf';
            header('Access-Control-Allow-Origin: *');

            return $pdf->Output(
                $this->_path.$file_name,
                'D'
            );
        } catch (\Exception $e) {

            return $this->response($e->getMessage(), Response::HTTP_BAD_REQUEST);
        }
    }

    private function setDataPDF($request, $id = null)
    {
        $data = [
            'id'                   => $id,
            'location'             => array_get($request, 'location', null),
            'date'                 => array_get($request, 'date', null),
            'note'                 => array_get($request, 'note', null),
            'type'                 => array_get($request, 'type', null),
            'customer_id'          => array_get($request, 'customer_id', null),
            'customer_name'        => array_get($request, 'customer_name', null),
            'company_name'         => array_get($request, 'company_name', null),
            'email'                => array_get($request, 'email', null),
            'shipping_type'        => array_get($request, 'shipping_type', null),
            'street'               => array_get($request, 'street', null),
            'city'                 => array_get($request, 'city', null),
            'post_code'            => array_get($request, 'post_code', null),
            'country'              => array_get($request, 'country', null),
            'invoice'              => array_get($request, 'invoice', null),
            'shipping'             => array_get($request, 'shipping', null),
            'payment'              => array_get($request, 'payment', null),
            'view_price'           => array_get($request, 'view_price', false),
            'new_shipping_address' => array_get($request, 'new_shipping_address', false),
            'shipping_address'     => array_get($request, 'shipping_address', false),
            'telephone'            => array_get($request, 'telephone', false),
            'invoice_type'         => array_get($request, 'invoice_type', 0),
        ];

        return $data;
    }

    private function _validateGeneratePDF($data, &$messages)
    {
        $rules = [
            'location'   => [
                'required',
            ],
            'date'       => [
                'required',
                'date_format:Y-m-d',
            ],
            'type'       => [
                'required',
                'in:'.
                VERKOOP_OP_REKENING
                .
                ','.
                VERKOOP_BETAALD
                .
                ','.
                POST_BETAALD
                .
                ','.
                POST_OP_REKENING,
            ],
            'invoice'    => [
                'required',
                'array',
            ],
            'view_price' => [
                'required',
                'boolean',
            ],
        ];
        if (
            $data['type'] ==
            3
            || $data['type'] ==
               4
        ) {
            $rules_type = [
                'type'          => [
                    'required',
                    'in:'.
                    VERKOOP_OP_REKENING
                    .
                    ','.
                    VERKOOP_BETAALD
                    .
                    ','.
                    POST_BETAALD
                    .
                    ','.
                    POST_OP_REKENING,
                ],
                'customer_name' => [
                    'required',
                ],
                'company_name'  => [
                    'required',
                ],
                'email'         => [
                    'required',
                    'email',
                ],
                'shipping_type' => [
                    'required',
                    'exists:shipping_method,code',
                ],
                'street'        => [
                    'required',
                ],
                'city'          => [
                    'required',
                ],
                'post_code'     => [
                    'required',
                ],
                'country'       => [
                    'required',
                    'exists:countries,country_code',
                ],
            ];
            $rules      = array_merge($rules, $rules_type);
        }
        if (
            $data['customer_id'] !=
            null
        ) {
            $rules['customer_id'] = [
                'required',
                'exists:customer,customer_ID',
            ];
        }
        if (
            $data['shipping'] !=
            null
        ) {
            $rules['shipping'] = [
                'required',
                'numeric',
            ];
        }
        if (
            $data['payment'] !=
            null
        ) {
            $rules['payment'] = [
                'required',
                'numeric',
            ];
        }
        $this->_validate_error($data, $rules, $messages);
        if (is_array($data['invoice'])) {
            foreach ($data['invoice'] as $invoice) {
                $invoice_data = [
                    'is_product'   => array_get($invoice, 'is_product', true),
                    'product_id'   => array_get($invoice, 'product_id', null),
                    'product_name' => array_get($invoice, 'product_name', null),
                    'price'        => array_get($invoice, 'price', null),
                    'number'       => array_get($invoice, 'number', null),
                    'storage'      => array_get($invoice, 'storage', null),
                ];
                $rules        = [
                    'price'  => [
                        'required',
                        'numeric',
                    ],
                    'number' => [
                        'required',
                        'numeric',
                    ],
                ];
                if (
                    $invoice_data['is_product'] ==
                    true
                ) {
                    $rules['product_id'] = [
                        'required',
                        'exists:product,product_ID',
                    ];
                } else {
                    $rules['product_name'] = [
                        'required',
                    ];
                }
                $this->_validate_error($invoice_data, $rules, $messages);
            }
        }
    }

    private function _generatePDF($data)
    {
        $pdf = new Mpdf(
            [
                'c',
                'A4',
                $default_font_size = 0,
                $default_font = '',
                $mgl = 0,
                $mgr = 0,
                $mgt = 0,
                $mgb = 0,
                $mgh = 0,
                $mgf = 0,
                $orientation = 'P',
            ]
        );
        $pdf->SetAuthor(PDF['AUTHOR']);
        $pdf->SetTitle(PDF['TITLE']);
        $pdf->SetSubject(PDF['SUBJECT']);
        $pdf->SetKeywords(PDF['KEY']);

        $html = (string)view(
            'bon.pdf',
            [
                'data' => $data,
            ]
        );
        $pdf->WriteHTML($html);

        return $pdf;
    }

    public function sendEmail(Request $request)
    {
        try {
            $smtp = smtp::first();
            Config::set('mail.from.address', $smtp->email);
            Config::set('mail.from.name', PDF['AUTHOR']);
            Config::set('mail.username', $smtp->email);
            Config::set('mail.password', $smtp->password);
            Config::set('mail.encryption', $smtp->secure);
            Config::set('mail.host', $smtp->host);
            Config::set('mail.port', $smtp->port);
            Config::set('mail.driver', $smtp->transport_type);

            $request  = $request->all();
            $data     = [
                'to'       => array_get($request, 'email', null),
                'cc_email' => array_get($request, 'cc_email', null),
                'subject'  => array_get($request, 'subject', null),
                'content'  => array_get($request, 'content', null),
                'file'     => array_get($request, 'url_file', null),
            ];
            $pathinfo = pathinfo($data['file']);
            $url_file = 'pdf';
            if (
                $pathinfo['dirname'] ==
                env('APP_URL').
                '/'.
                $url_file
            ) {
                $data['file'] = public_path(
                    $url_file.
                    '/'.
                    $pathinfo['basename']
                );
            }
            dispatch(
                new SentMailJob(
                    $data,
                    'bon.proforma_email'
                )
            );
            $messages = trans('messages.send_mail_success');

            return $this->response(null, HTTP_OK, $messages);
        } catch (\Exception $e) {
            return $this->response($e->getMessage(), HTTP_BAD_REQUEST);
        }
    }

    public function generateProformaPDF()
    {
        $request = Request::capture()
                          ->all();
        $data    = [
            'id'              => array_get($request, 'id', null),
            'company_name'    => array_get($request, 'company_name', null),
            'country'         => array_get($request, 'country', null),
            'customer_name'   => array_get($request, 'customer_name', null),
            'date'            => array_get($request, 'date', null),
            'email'           => array_get($request, 'email', null),
            'invoice_type'    => array_get($request, 'invoice_type', null),
            'phone'           => array_get($request, 'phone', null),
            'postalcode_city' => array_get($request, 'postalcode_city', null),
            'shipping'        => array_get($request, 'shipping', null),
            'street'          => array_get($request, 'street', null),
            'terms'           => array_get($request, 'terms', null),
            'vat'             => array_get($request, 'vat', null),
            'bon_has_product' => array_get($request, 'bon_has_product', null),
            'sub_total'       => 0,
            'tax'             => 0,
            'total'           => 0,
        ];
        if (
            $data['date'] !=
            null
        ) {
            $data['date'] = Helpers::formatDate($data['date']);
        }
        if (
            $data['bon_has_product'] !=
            null
        ) {
            foreach ($data['bon_has_product'] as $key => $product) {
                $amount                                  = Helpers::mul_value($product['price'], $product['quantity']);
                $data['bon_has_product'][$key]['amount'] = $amount;
                $data['sub_total']                       = Helpers::sum_value($data['sub_total'], $amount);
            }
            $data['total'] = Helpers::sum_value($data['sub_total'], $data['shipping']);
            $data['tax']   = Helpers::percent_value($data['total'], $data['invoice_type']);
            $data['total'] = Helpers::sum_value($data['total'], $data['tax']);
        }
        try {
            $file_name = 'proforma-invoice-'.
                         strtotime('now').
                         '.pdf';
            $pdf       = new Mpdf(
                [
                    'c',
                    'A4',
                    $default_font_size = 0,
                    $default_font = '',
                    $mgl = 0,
                    $mgr = 0,
                    $mgt = 0,
                    $mgb = 0,
                    $mgh = 0,
                    $mgf = 0,
                    $orientation = 'P',
                ]
            );
            $pdf->SetAuthor(PDF['AUTHOR']);
            $pdf->SetTitle(PDF['TITLE']);
            $pdf->SetSubject(PDF['SUBJECT']);
            $pdf->SetKeywords(PDF['KEY']);
            $pdf->SetHTMLFooter('<p style="text-align:center;">{PAGENO}/<b>{nbpg}</b></p>');
            $setting = ProformaInvoiceSetting::first()
                                             ->toArray();
            $html    = (string)view(
                'bon.proforma',
                [
                    'data'    => $data,
                    'setting' => $setting,
                ]
            );
            $pdf->WriteHTML($html);
            $pdf->Output(
                $this->_path.$file_name,
                'F'
            );
            $url_file = 'pdf';
            File::makeDirectory(public_path($url_file), 0777, true, true);
            $storage_path = $this->_path.$file_name;
            $new_url      = public_path(
                $url_file.
                '/'.
                $file_name
            );
            File::move($storage_path, $new_url);

            $messages = trans('messages.generate_pdf_success');
            $result   = [
                'url' => env('APP_URL')."/".$url_file."/$file_name",
            ];

            return $this->response($result, HTTP_OK, $messages);
        } catch (\Exception $e) {

            return $this->response($e->getMessage(), HTTP_BAD_REQUEST);
        }
    }

    public function generatePDF(Request $request)
    {
        $request_all = $request->all();
        $data        = $this->setDataPDF($request_all);
        $messages    = '';
        $this->_validateGeneratePDF($data, $messages);
        if (
            $messages !=
            ''
        ) {
            return $this->response(
                $messages,
                HTTP_INTERNAL_SERVER_ERROR
            );
        }
        try {
            $timeNow   = date('YmdHis');
            $file_name = $timeNow.'-'.strtotime('now').'.pdf';
            $this->_createPdf($data, $file_name);

            $messages = trans('messages.generate_pdf_success');
            $result   = [
                'url' => env('APP_URL')."/tmp/".$file_name,
            ];

            return $this->response($result, HTTP_OK, $messages);
        } catch (\Exception $e) {

            return $this->response($e->getMessage(), HTTP_BAD_REQUEST);
        }
    }

    private function _createPdf($data, $file_name, $url_file = 'tmp')
    {
        $result_pdf = $this->_bon->generatePDF($data);
        $pdf        = $this->_generatePDF($result_pdf);

        $pdf->Output(
            $this->_path.$file_name,
            'F'
        );
        File::makeDirectory(public_path($url_file), 0777, true, true);
        $storage_path = $this->_path.$file_name;
        $new_url      = public_path($url_file.'/'.$file_name);
        File::move($storage_path, $new_url);
    }

    public function update($id, Request $request)
    {
        $request_all = $request->all();
        $data        = $this->setDataPDF($request_all, $id);
        $messages    = '';
        $this->_validateGeneratePDF($data, $messages);
        if (
            $messages !=
            ''
        ) {
            return $this->response(
                $messages,
                HTTP_INTERNAL_SERVER_ERROR
            );
        }

        DB::beginTransaction();
        try {
            $timeNow   = date('YmdHis');
            $file_name = $timeNow.'-'.strtotime('now');
            //generate pdf view price = true
            $data['view_price'] = true;
            $this->_createPdf(
                $data,
                $file_name.'.pdf',
                'pdf'
            );
            //generate pdf view price = false
            $data['view_price'] = false;
            $this->_createPdf(
                $data,
                $file_name.'_without_price'.'.pdf',
                'pdf'
            );

            $result = $this->_save($data);

            $result->bon_file = $file_name.'.pdf';
            $result->save();

            DB::commit();
            $messages = trans('messages.update_success');

            return $this->response($result, HTTP_OK, $messages);
        } catch (\Exception $e) {
            DB::rollBack();

            return $this->response($e->getMessage(), HTTP_BAD_REQUEST);
        }
    }

    private function _save($data)
    {
        if (!$data['customer_id']) {
            $_customer    = CustomerRepo::getInstance();
            $customerData = [
                'name'          => $data['customer_name'],
                'city'          => $data['city'],
                'country'       => $data['country'],
                'postalcode'    => $data['post_code'],
                'street'        => $data['street'],
                'email'         => $data['email'],
                'created_date'  => date('Y-m-d'),
                'customer_name' => $data['customer_name'],
                'billing_email' => $data['email'],
            ];
            if ($data['telephone']) {
                $customerData['phone'] = $data['telephone'];
            }
            $customer            = $_customer->create($customerData);
            $data['customer_id'] = $customer->customer_ID;
        }
        if (
            $data['type'] ==
            3
            || $data['type'] ==
               4
        ) {
            if (!$data['shipping_address']) {
                $_customerAdress    = CustomerAddressRepo::getInstance();
                $customerAdressData = [
                    'customer_id'   => $data['customer_id'],
                    'customer_name' => $data['customer_name'],
                    'company_name'  => $data['company_name'],
                    'street'        => $data['street'],
                    'country'       => $data['country'],
                    'city'          => $data['city'],
                    'postal_code'   => $data['post_code'],
                    'address_type'  => $data['shipping_type'],
                ];
                if ($data['telephone']) {
                    $customerAdressData['telephone'] = $data['telephone'];
                }
                $customerAdress           = $_customerAdress->create($customerAdressData);
                $data['shipping_address'] = $customerAdress->id;
            }
        }

        return $this->_bon->_save($data);
    }

    public function create(Request $request)
    {
        $request_all = $request->all();
        $data        = $this->setDataPDF($request_all);
        $messages    = '';
        $this->_validateGeneratePDF($data, $messages);
        if ($messages != '') {
            return $this->response(
                $messages,
                HTTP_INTERNAL_SERVER_ERROR
            );
        }
        DB::beginTransaction();
        try {
            $timeNow   = date('YmdHis');
            $file_name = $timeNow.'-'.strtotime('now');
            //generate pdf view price = true
            $data['view_price'] = true;
            $this->_createPdf(
                $data,
                $file_name.'.pdf',
                'pdf'
            );
            //generate pdf view price = false
            $data['view_price'] = false;
            $this->_createPdf(
                $data,
                $file_name.'_without_price'.'.pdf',
                'pdf'
            );

            $result = $this->_save($data);

            $result->bon_file = $file_name.'.pdf';
            $result->save();

            DB::commit();
            $messages = trans('messages.create_success');

            return $this->response($result, HTTP_OK, $messages);
        } catch (\Exception $e) {
            DB::rollBack();

            return $this->response($e->getMessage(), HTTP_BAD_REQUEST);
        }
    }

    public function revert($id)
    {
        DB::beginTransaction();
        try {
            $result = $this->_bon->find($id);
            if (!$result) {
                throw new \Exception(trans('messages.data_not_found'));
            }
            if ($result->invoice_status == ACTIVE) {
                throw new \Exception(trans('messages.data_not_found'));
            }
            $result->invoice_status = ACTIVE;
            $result->save();
            DB::commit();
            $messages = trans('messages.revert_success');

            return $this->response($result, HTTP_OK, $messages);
        } catch (\Exception $e) {
            DB::rollBack();

            return $this->response($e->getMessage(), HTTP_BAD_REQUEST);
        }
    }

    /**
     * @OA\Get(
     *     path="/bon/detail/{invoice_id}",
     *     summary="Detail Bon",
     *     tags={"Invoice"},
     *     operationId="invoice_get",
     *     security={{"Bearer":{}}},
     *     @OA\Parameter(
     *         description="Invoice id",
     *         in="path",
     *         name="invoice_id",
     *         @OA\Schema(
     *           type="integer",
     *           format="int32"
     *         )
     *     ),
     *     @OA\Response(
     *          response="200",
     *          description="{
    'status': true,
    'message': null,
    'data': object
    }",
     *     ),
     *     @OA\Response(
     *          response="401",
     *          description="{
    'status': false,
    'message': 'Invalid token',
    'data': []
    }",
     *     ),
     * )
     */
    public function get($bonID)
    {
        try {
            $result = $this->_bon->detail($bonID);
            if (!$result) {
                throw new \Exception(trans('messages.data_not_found'));
            }

            return $this->response($result);
        } catch (\Exception $e) {

            return $this->response($e->getMessage(), HTTP_BAD_REQUEST);
        }
    }

    public function delete($id)
    {
        DB::beginTransaction();
        try {
            $invoice = $this->_bon->find($id);
            if (!$invoice) {
                throw new \Exception(trans('messages.data_not_found'));
            }
            if (
                $invoice->invoice_status ==
                ACTIVE
            ) {
                throw new \Exception(trans('messages.invoice_cannot_deleted'));
            }

            foreach ($invoice->bon_has_product as $bon_has_product) {
                $this->_bon->refundQty($bon_has_product);
            }
            if ($invoice->is_magento) {
                $orderStatus           = OrderStatus::where('order_id', $invoice->magento_order_id)
                                                    ->first();
                $orderStatus->order_id = $invoice->magento_order_id;
                $orderStatus->status   = ORDER_STATUS_PROCESSING;
                $orderStatus->save();
            }
            $invoice->delete();
            DB::commit();

            $message = trans('messages.delete_success');

            return $this->response($message);
        } catch (\Exception $e) {
            DB::rollBack();

            return $this->response($e->getMessage(), Response::HTTP_BAD_REQUEST);
        }
    }

    public function confirm()
    {
        try {
            $request = Request::capture()->all();
            $id      = array_get($request, 'id', null);
            $result  = $this->_bon->confirm($id);

            return $this->response($result);
        } catch (\Exception $e) {
            return $this->response($e->getMessage(), Response::HTTP_BAD_REQUEST);
        }
    }

    public function confirmShipments()
    {
        DB::beginTransaction();
        try {
            $request = Request::capture()->all();
            $id      = array_get($request, 'id', null);
            $result  = $this->_bon->confirmShipments($id);
            DB::commit();

            return $this->response($result);
        } catch (\Exception $e) {
            DB::rollBack();

            return $this->response($e->getMessage(), Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * @OA\Get(
     *     path="/bon/list-by-customer/{customer_id}",
     *     summary="Get list data by customer",
     *     tags={"Invoice"},
     *     operationId="invoice_listByCustomer",
     *     security={{"Bearer":{}}},
     *     @OA\Parameter(
     *         description="Customer id",
     *         in="path",
     *         name="customer_id",
     *         @OA\Schema(
     *           type="integer",
     *           format="int32"
     *         )
     *     ),
     *     @OA\Response(
     *          response="200",
     *          description="{
    'status': true,
    'message': null,
    'data': object
    }",
     *     ),
     *     @OA\Response(
     *          response="401",
     *          description="{
    'status': false,
    'message': 'Invalid token',
    'data': []
    }",
     *     ),
     * )
     */
    public function listByCustomer($customerID)
    {
        try {
            $result = $this->_bon->listByCustomer($customerID);
            if (!$result) {
                throw new \Exception(trans('messages.data_not_found'));
            }

            return $this->response($result);
        } catch (\Exception $e) {

            return $this->response($e->getMessage(), Response::HTTP_BAD_REQUEST);
        }
    }
}