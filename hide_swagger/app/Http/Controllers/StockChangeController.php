<?php

namespace App\Http\Controllers;

use App\Repositories\BrandRepo;
use App\Repositories\LocationRepo;
use App\Repositories\ProductRepo;
use App\Repositories\ProductTypeRepo;
use App\Repositories\StockChangeRepo;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Validator;

class StockChangeController extends Controller
{
    protected $_stockChange;
    public function __construct(
        StockChangeRepo $stockChangeRepo
    )
    {
        $this->_stockChange = $stockChangeRepo;
    }
    /**
     * @OA\Get(
     *     path="/stock-change/get-by-product/{product_id}",
     *     summary="List Stock Change by Product",
     *     tags={"Stock Change"},
     *     operationId="stock_change_getByProduct",
     *     security={{"Bearer":{}}},
     *     @OA\Parameter(
     *         description="Product id",
     *         in="path",
     *         name="product_id",
     *         @OA\Schema(
     *           type="integer",
     *           format="int32"
     *         )
     *     ),
     *     @OA\Response(
     *          response="200",
     *          description="{
    'status': true,
    'message': null,
    'data': object
    }",
     *     ),
     *     @OA\Response(
     *          response="401",
     *          description="{
    'status': false,
    'message': 'Invalid token',
    'data': []
    }",
     *     ),
     * )
     */
    public function getByProduct($id){
        try {
            $result = $this->_stockChange->getByProduct($id);

            return $this->response($result);
        } catch (\Exception $e) {

            return $this->response($e->getMessage(), Response::HTTP_BAD_REQUEST);
        }
    }
}