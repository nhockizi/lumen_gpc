<?php

namespace App\Http\Controllers;

use App\Repositories\BrandRepo;
use App\Repositories\CountryRepo;
use App\Repositories\CustomerAddressRepo;
use App\Repositories\CustomerRepo;
use App\Repositories\LocationRepo;
use App\Repositories\ProductRepo;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Validator;

class CountryController extends Controller
{
    protected $_country;
    public function __construct(
        CountryRepo $countryRepo
    )
    {
        $this->_country = $countryRepo;
    }
    /**
     * @OA\Get(
     *     path="/country/list",
     *     summary="List all Country",
     *     tags={"Country"},
     *     operationId="country_list",
     *     security={{"Bearer":{}}},
     *     @OA\Parameter(
     *         description="page",
     *         in="query",
     *         name="page",
     *         @OA\Schema(
     *           type="integer",
     *           format="int32"
     *         )
     *     ),
     *     @OA\Parameter(
     *         description="number item length",
     *         in="query",
     *         name="length",
     *         @OA\Schema(
     *           type="integer",
     *           format="int32"
     *         )
     *     ),
     *     @OA\Parameter(
     *         description="name",
     *         in="query",
     *         name="name",
     *         @OA\Schema(
     *           type="string",
     *         )
     *     ),
     *     @OA\Response(
     *          response="200",
     *          description="{
    'status': true,
    'message': null,
    'data': object
    }",
     *     ),
     *     @OA\Response(
     *          response="401",
     *          description="{
    'status': false,
    'message': 'Invalid token',
    'data': []
    }",
     *     ),
     * )
     */
    public function list(){
        try {
            $result = $this->_country->list();

            return $this->response($result);
        } catch (\Exception $e) {

            return $this->response($e->getMessage(), Response::HTTP_BAD_REQUEST);
        }
    }
}