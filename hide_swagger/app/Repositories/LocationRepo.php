<?php

namespace App\Repositories;

use App\Libraries\Helpers;
use App\Models\Location;

class LocationRepo extends BaseRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        // TODO: Implement model() method.
        return Location::class;
    }

    public function list($data)
    {
        $item_length  = $data['length'] ?? ITEM_LENGHT;
        $fieldsSearch = [
            'name' => ['location', 'name', 'LIKE'],
        ];
        $query        = Location::select('*');
        $query        = Helpers::searchFieldsMapping($query, $fieldsSearch);
        $fieldsSort   = [
            'location_ID' => [
                'location',
                'location_ID',
            ],
            'name'        => [
                'location',
                'name',
            ],
            'country'     => [
                'location',
                'country',
            ],
            'province'    => [
                'location',
                'province',
            ],
            'postalcode'  => [
                'location',
                'postalcode',
            ],
        ];
        $query        = Helpers::sortFieldsMapping($query, $fieldsSort, $data['order'], $data['type']);
        $result       = $query->paginate($item_length);
        $result       = Helpers::formatPagination($result);

        return $result;
    }

    public function _save($data)
    {
        $attributes = [
            'location_ID' => $data['id'],
        ];
        $values     = [
            'name'       => (string)$data['name'],
            'country'    => (string)$data['country'],
            'province'   => (string)$data['province'],
            'postalcode' => (string)$data['postalcode'],
            'street'     => (string)$data['street'],
            'additional' => (string)$data['additional'],
        ];
        $result     = $this->updateOrCreate($attributes, $values);

        return $result;
    }
}