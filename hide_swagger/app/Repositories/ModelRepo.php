<?php

namespace App\Repositories;

use App\Libraries\Helpers;
use App\Models\Model;

class ModelRepo extends BaseRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        // TODO: Implement model() method.
        return Model::class;
    }

    public function list($data)
    {
        $item_length  = $data['length'] ?? ITEM_LENGHT;
        $fieldsSearch = [
            'brand_id'  => ['model', 'brand_brand_ID', '='],
            'parent_id' => ['model', 'parent_ID', '='],
            'name'      => ['model', 'name', 'LIKE'],
        ];
        $query        = Model::select('model.*')
                             ->with(
                                 [
                                     'brand',
                                     'child',
                                     'parent',
                                 ]
                             )
                             ->leftJoin('brand', 'brand.brand_ID', 'model.brand_brand_ID');
        $query        = Helpers::searchFieldsMapping($query, $fieldsSearch);
        $fieldsSort   = [
            'model_ID'    => [
                'model',
                'model_ID',
            ],
            'name'        => [
                'model',
                'name',
            ],
            'brand_name'  => [
                'brand',
                'name',
            ],
            'parent_name' => [
                'brand',
                'name',
            ],
        ];
        $query        = Helpers::sortFieldsMapping($query, $fieldsSort, $data['order'], $data['type']);
        $result       = $query->paginate($item_length);
        $result       = Helpers::formatPagination($result);

        return $result;
    }

    public function _save($data)
    {
        $attributes = [
            'model_ID' => $data['id'],
        ];
        $values     = [
            'name'           => (string)$data['name'],
            'brand_brand_ID' => (string)$data['brand'],
            'parent_ID'      => (string)$data['model_parent'],
        ];
        $result     = $this->updateOrCreate($attributes, $values);

        return $result;
    }
}