<?php

namespace App\Repositories;

use App\Libraries\Helpers;
use App\Models\ProductType;

class ProductTypeRepo extends BaseRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        // TODO: Implement model() method.
        return ProductType::class;
    }

    public function list($data)
    {
        $item_length  = $data['length'] ?? ITEM_LENGHT;
        $fieldsSearch = [
            'name' => ['product_type', 'product_type_name', 'LIKE'],
            'code' => ['product_type', 'product_type_code', 'LIKE'],
        ];
        $query        = ProductType::select('*');
        $query        = Helpers::searchFieldsMapping($query, $fieldsSearch);
        $fieldsSort   = [
            'product_type_ID'   => [
                'product_type',
                'product_type_ID',
            ],
            'product_type_name' => [
                'product_type',
                'product_type_name',
            ],
            'product_type_code' => [
                'product_type',
                'product_type_code',
            ],
        ];
        $query        = Helpers::sortFieldsMapping($query, $fieldsSort, $data['order'], $data['type']);
        $result       = $query->paginate($item_length);
        $result       = Helpers::formatPagination($result);

        return $result;
    }

    public function _save($data)
    {
        $attributes = [
            'product_type_ID' => $data['id'],
        ];
        $values     = [
            'product_type_name' => (string)$data['name'],
            'product_type_code' => (string)$data['code'],
            'description_nl'    => (string)$data['description_nl'],
            'description_en'    => (string)$data['description_en'],
        ];
        $result     = $this->updateOrCreate($attributes, $values);

        return $result;
    }
}