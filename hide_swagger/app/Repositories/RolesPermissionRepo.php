<?php

namespace App\Repositories;

use App\Libraries\Helpers;
use App\Models\Member;
use App\Models\Roles;
use App\Models\RolesPermission;
use App\Models\User;
use App\Repositories\BaseRepository;

class RolesPermissionRepo extends BaseRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        // TODO: Implement model() method.
        return RolesPermission::class;
    }

}