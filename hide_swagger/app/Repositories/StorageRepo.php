<?php

namespace App\Repositories;

use App\Libraries\Helpers;
use App\Models\Brand;
use App\Models\Location;
use App\Models\Storage;
use App\Models\StorageHasProduct;

class StorageRepo extends BaseRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        // TODO: Implement model() method.
        return Storage::class;
    }

}