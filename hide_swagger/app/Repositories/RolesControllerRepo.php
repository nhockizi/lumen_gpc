<?php

namespace App\Repositories;

use App\Libraries\Helpers;
use App\Models\Roles;
use App\Models\RolesController;
use App\Models\RolesFunction;
use App\Models\User;

class RolesControllerRepo extends BaseRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        // TODO: Implement model() method.
        return RolesController::class;
    }

    public function list($data)
    {
        $item_length  = $data['length'] ?? ITEM_LENGHT;
        $fieldsSearch = [
            'id' => [
                'role',
                'id',
                '=',
            ],
        ];
        $query        = RolesController::select('*');
        $query        = Helpers::searchFieldsMapping($query, $fieldsSearch);
        //        $fieldsSort   = [
        //            'brand_ID'   => [
        //                'brand',
        //                'brand_ID',
        //            ],
        //            'name'       => [
        //                'brand',
        //                'name',
        //            ],
        //            'brand_CODE' => [
        //                'brand',
        //                'brand_CODE',
        //            ],
        //        ];
        //        $query        = Helpers::sortFieldsMapping($query, $fieldsSort, $data['order'], $data['type']);
        $result = $query->paginate($item_length);
        $result = Helpers::formatPagination($result);

        return $result;
    }

    public function _detail($id)
    {
        $data   = RolesController::with(
            [
                'roles_controller_function',
            ]
        )
                                 ->where('id', $id)
                                 ->first();
        $result = (object)[
            'id'             => $data->id,
            'name'           => $data->name,
            'roles_function' => [],
        ];
        foreach ($data->roles_controller_function as $roles_controller_function) {
            $result->roles_function[] = $roles_controller_function->roles_function;
        }

        return $result;
    }

    public function listRoles($rolesControllerID)
    {
        $data = Roles::with(
            [
                'roles_permission',
            ]
        )
                     ->get();

        $dataFunction = RolesFunction::whereHas(
            'roles_controller_function',
            function ($q) use ($rolesControllerID) {
                $q->where('roles_controller_id', $rolesControllerID);
            }
        )
                                     ->get();
        $result       = [];
        foreach ($data as $item) {
            $array = [];
            foreach ($dataFunction as $f) {
                $tmp = (object)[
                    'id'      => $f->id,
                    'name'    => $f->name,
                    'checked' => false,
                ];
                foreach ($item->roles_permission as $roles_permission) {
                    $roles_function_id = $roles_permission->roles_function_id ?? null;
                    if($roles_permission->roles_controller_id == $rolesControllerID) {
                        if ($f->id == $roles_function_id) {
                            $tmp->checked = true;
                        }
                    }
                }
                $array[] = $tmp;
            }

            $result[] = (object)[
                'id'       => $item->id,
                'name'     => $item->name,
                'function' => $array,
            ];
        }

        return $result;
    }
}