<?php

namespace App\Repositories;

use App\Libraries\Helpers;
use App\Models\Shipments;

class ShipmentRepo extends BaseRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        // TODO: Implement model() method.
        return Shipments::class;
    }

    public function list($data)
    {
        $request      = app('request')->all();
        $item_length  = $request['length'] ?? ITEM_LENGHT;
        $fieldsSearch = [
            'invoice_id'     => [
                'shipments',
                'invoice_ID',
                'Like',
            ],
            'customer_email' => [
                'shipments',
                'customer_email',
                'Like',
            ],
        ];
        $query        = Shipments::select('*');
        $query        = Helpers::searchFieldsMapping($query, $fieldsSearch);
        $fieldsSort   = [
        ];
        $query        = Helpers::sortFieldsMapping(
            $query,
            $fieldsSort,
            $data['order'] ?? null,
            $data['type'] ?? null
        );
        $result       = $query->paginate($item_length);
        $result       = Helpers::formatPagination($result);

        return $result;
    }

    public function detail($id)
    {
        $result = $this->find($id);
        return $result;
    }
}