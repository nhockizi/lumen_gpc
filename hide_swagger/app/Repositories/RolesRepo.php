<?php

namespace App\Repositories;

use App\Libraries\Helpers;
use App\Models\Member;
use App\Models\Roles;
use App\Models\User;
use App\Repositories\BaseRepository;

class RolesRepo extends BaseRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        // TODO: Implement model() method.
        return Roles::class;
    }

    public function list($data)
    {
        $item_length  = $data['length'] ?? ITEM_LENGHT;
        $fieldsSearch = [
            'id' => [
                'role',
                'id',
                '='
            ],
        ];
        $query        = Roles::select('*');
        $query        = Helpers::searchFieldsMapping($query, $fieldsSearch);
//        $fieldsSort   = [
//            'brand_ID'   => [
//                'brand',
//                'brand_ID',
//            ],
//            'name'       => [
//                'brand',
//                'name',
//            ],
//            'brand_CODE' => [
//                'brand',
//                'brand_CODE',
//            ],
//        ];
//        $query        = Helpers::sortFieldsMapping($query, $fieldsSort, $data['order'], $data['type']);
        $result = $query->paginate($item_length);
        $result = Helpers::formatPagination($result);

        return $result;
    }

    public function listPermission()
    {
        $data = Roles::with(
            [
                'roles_permission',
                'roles_permission.roles_controller_function'
            ]
        )
                     ->get();
        return $data;
    }
}