<?php

namespace App\Services\UserLoggedIn;

use App\Models\User;
use App\Services\UserLoggedIn\Src\UserLoggedInService;
use Illuminate\Support\Facades\Facade;
use Tymon\JWTAuth\Token;

/**
 * Class UserLoggedInFacade
 *
 * @method static null|User getUser()                    Get Model Account
 * @method static string getToken()                  Get Token
 *
 * @package App\Services\UserLoggedIn
 */
class UserLoggedInFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return UserLoggedInService::class;
    }
}