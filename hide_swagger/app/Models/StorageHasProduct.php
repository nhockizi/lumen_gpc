<?php

namespace App\Models;

class StorageHasProduct extends BaseModel
{
    public $timestamps = false;
    protected $table = 'storage_has_product';
    protected $primaryKey = 'id';

    protected $fillable
        = [
            'id',
            'storage_storage_ID',
            'product_product_ID',
            'amount',
            'location_ID',
        ];

    protected function storage()
    {
        return $this->hasOne(Storage::class, 'storage_ID', 'storage_storage_ID');
    }

    protected function location()
    {
        return $this->hasOne(Location::class, 'location_ID', 'location_ID');
    }
}
