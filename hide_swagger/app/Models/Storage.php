<?php

namespace App\Models;

class Storage extends BaseModel
{
    public    $timestamps = false;
    protected $table      = 'storage';
    protected $primaryKey = 'storage_ID';
    protected $fillable
                          = [
            'storage_ID',
            'rack',
            'row',
            'column',
            'deleted',
        ];

}
