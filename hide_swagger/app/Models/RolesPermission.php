<?php

namespace App\Models;

class RolesPermission extends BaseModel
{
    protected $table = 'roles_permission';
    public $timestamps = false;

    protected $fillable = [
        'roles_id',
        'roles_controller_id',
        'roles_function_id'
    ];

    public function roles()
    {
        return $this->hasOne(Roles::class, 'id', 'roles_id');
    }

    public function roles_controller()
    {
        return $this->hasOne(RolesController::class, 'id', 'roles_controller_id');
    }

    public function roles_function()
    {
        return $this->hasOne(RolesFunction::class, 'id', 'roles_function_id');
    }
}