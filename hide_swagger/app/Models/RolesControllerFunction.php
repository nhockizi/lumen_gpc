<?php

namespace App\Models;

class RolesControllerFunction extends BaseModel
{
    protected $table = 'roles_controller_function';
    protected $primaryKey = 'id';
    public $timestamps = false;

    protected $fillable = [
        'roles_controller_id',
        'roles_function_id',
    ];

    public function roles_controller(){
        return $this->hasOne(RolesController::class, 'id', 'roles_controller_id');
    }
    public function roles_function(){
        return $this->hasOne(RolesFunction::class, 'id', 'roles_function_id');
    }
}