<?php

namespace App\Models;

class ProformaInvoiceTerms extends BaseModel
{
    public    $incrementing = false;
    public    $timestamps   = false;
    protected $table        = 'proforma_invoice_terms';
    protected $primaryKey   = null;
    protected $fillable     = ['terms'];

}
