<?php

namespace App\Models;

class Bon extends BaseModel
{
    protected $table = 'bon';
    protected $primaryKey = 'bon_ID';
    public $timestamps = false;
    protected $fillable = [
        'bon_datum',
        'price',
        'price_express_shipping',
        'price_shipping',
        'price_total',
        'bon_status',
        'bon_file',
        'customer_customer_ID',
        'billing_billing_ID',
        'member_member_ID',
        'invoice_status',
        'bon_type',
        'is_factuur',
        'location_ID',
        'note',
        'created_date',
        'updated_date',
        'sub_total',
        'status_process',
        'is_magento',
        'magento_order_id',
        'tax_amount',
        'special_client',
        'magento_method_payment',
        'country_id',
        'customer_email',
        'address_id',
        'shipping_method',
        'is_updated',
        'delete_on_kz',
    ];

    public function customer()
    {
        return $this->hasOne(Customer::class, 'customer_ID', 'customer_customer_ID');
    }

    public function customer_address()
    {
        return $this->hasOne(CustomerAddress::class, 'id', 'address_id');
    }

    public function bon_has_product()
    {
        return $this->hasMany(BonHasProduct::class, 'bon_bon_ID', 'bon_ID');
    }

    public function shipping_method_data(){
        return $this->hasOne(ShippingMethod::class, 'code', 'shipping_method');
    }

}
