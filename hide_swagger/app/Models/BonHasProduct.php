<?php

namespace App\Models;

class BonHasProduct extends BaseModel
{
    protected $table      = 'bon_has_product';
    protected $primaryKey = 'id';
    public    $timestamps = false;

    protected $fillable   = [
        'bon_bon_ID',
        'product_product_ID',
        'brand_brand_ID',
        'amount',
        'price',
        'add_custom',
        'order_display',
        'storage_ID',
        'storage_amount',
        'before_stock',
        'after_stock',
        'invoice_storage',
    ];
    public function product()
    {
        return $this->hasOne(Product::class, 'product_ID', 'product_product_ID');
    }

}
