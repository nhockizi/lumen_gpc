<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Tymon\JWTAuth\Facades\JWTAuth;
use Carbon\Carbon;

class BaseModel extends Model
{
    public function getTime()
    {
        return date(FORMAT_DATE_TIME_DB);
    }

    public function authUser()
    {
        // and you can continue to chain methods
        $user = JWTAuth::parseToken()->authenticate();
        return $user;
    }

    public static function getTableName()
    {
        return (new static)->getTable();
    }

    public static function getPriKeyName()
    {
        return (new static)->getKeyName();
    }

    public static function getColumnName($column)
    {
        return self::getTableName() . '.' . $column;
    }

    public function getCreatedAtAttribute($attr)
    {
        return  Carbon::parse($attr)->format(FORMAT_DATE_TIME);
    }

    public function getUpdatedAtAttribute($attr)
    {
        return  Carbon::parse($attr)->format(FORMAT_DATE_TIME);
    }

    protected function getDateTimeTypeValue($attr){
        if($attr instanceof \DateTime){
            return $attr->format(FORMAT_DATE_TIME);
        }
        return Carbon::parse($attr)->format(FORMAT_DATE_TIME);
    }
}
