<?php

namespace App\Models;

class ProformaInvoiceSetting extends BaseModel
{
    public    $incrementing = false;
    public    $timestamps   = false;
    protected $table        = 'proforma_invoice_setting';
    protected $primaryKey   = null;
    protected $fillable     = [];

}
