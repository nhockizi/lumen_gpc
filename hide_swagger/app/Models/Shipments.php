<?php

namespace App\Models;

class Shipments extends BaseModel
{
    protected $table = 'shipments';
    protected $primaryKey = 'shipments_ID';
    public $timestamps = false;
    protected $fillable = [];

    public function bon(){
        return $this->hasOne(Bon::class, 'bon_ID', 'invoice_ID');
    }
}
