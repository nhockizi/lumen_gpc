<?php

namespace App\Models;

class Roles extends BaseModel
{
    protected $table = 'roles';
    protected $primaryKey = 'id';
    public $timestamps = false;

    protected $fillable = [
        'name',
        'created_date'
    ];

    public function roles_permission(){
        return $this->hasMany(RolesPermission::class,'roles_id', 'id');
    }
}