<?php

define('ORDER_BY_DESC', 'DESC');
define('ORDER_BY_ASC', 'ASC');

define('DEFAULT_PAGINATION_LIMIT_LENGTH', 10);

define('DEFAULT_CREATED_USER_FIELD', 'created_by');
define('DEFAULT_UPDATED_USER_FIELD', 'updated_by');
define('DEFAULT_CREATED_DATE_FIELD', 'created_at');
define('DEFAULT_UPDATED_DATE_FIELD', 'updated_at');

define('LENGTH_BEST_SELLER_CATEGORIES', 3);

define('ARRAY_PRICE', [
    1     => [
        'id' => 1,
        'name'  => '< 50'
    ],
    2     => [
        'id' => 2,
        'name'  => '50 - 100'
    ],
    3     => [
        'id' => 3,
        'name'  => '100 - 300'
    ],
    4     => [
        'id' => 4,
        'name'  => '300 - 500'
    ],
    5     => [
        'id' => 5,
        'name'  => '> 500'
    ],
]);

define('ARRAY_RATING', [
   1    => [
       'id'     => 1,
       'name'   => 1
   ],
   2    => [
       'id'     => 2,
       'name'   => 2
   ],
   3    => [
       'id'     => 3,
       'name'   => 3
   ],
   4    => [
       'id'     => 4,
       'name'   => 4
   ],
   5    => [
       'id'     => 5,
       'name'   => 5
   ]
]);