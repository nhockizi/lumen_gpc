<?php
return [
    'sync_success'      => 'Synchronized successful!',
    'create_success'    => 'Create successful!',
    'update_success'    => 'Update successful!',
    'delete_success'    => 'Delete successful!',
    'save_success'      => 'Save successful!',
    'send_mail_success' => 'Send email successful!',
    'create_fail'       => 'Create fail!',
    'update_fail'       => 'Update fail!',
    'delete_fail'       => 'Delete fail!',
    'save_fail'         => 'Save fail!',
    'send_mail_fail'    => 'Send email fail!',
    'data_blank'        => 'Data is not blank',
    'product_not_found' => 'Product not found',
];