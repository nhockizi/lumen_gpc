<?php
return [
    'permission_denied'         => 'Permission denied',
    'login_successfull'         => 'Login successfull',
    'sync_success'              => 'Synchronized successful!',
    'generate_pdf_success'      => 'Generate pdf successful!',
    'rollback_success'          => 'Roll Back successful!',
    'revert_success'            => 'Revert successful!',
    'create_success'            => 'Create successful!',
    'update_success'            => 'Update successful!',
    'delete_success'            => 'Delete successful!',
    'save_success'              => 'Save successful!',
    'send_mail_success'         => 'Send email successful!',
    'create_fail'               => 'Create fail!',
    'update_fail'               => 'Update fail!',
    'delete_fail'               => 'Delete fail!',
    'save_fail'                 => 'Save fail!',
    'send_mail_fail'            => 'Send email fail!',
    'data_blank'                => 'Data is not blank',
    'data_not_found'            => 'Data not found',
    'product_not_found'         => 'Product not found',
    'email_has_existed_already' => 'Email has existed already',
    'email_available'           => 'Email available',
    'post_code_not_exists'      => 'Post Code is not exists',
    'invoice_cannot_deleted'    => 'Invoice cannot be deteted for some reasons.',
];