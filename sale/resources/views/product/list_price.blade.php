<html>
<body>
<tr>
    <td style="background-color: #DBEAF9"><strong>SKU</strong></td>
    <td style="background-color: #DBEAF9" width="100"><strong>Name</strong></td>
    <td style="background-color: #DBEAF9"><strong>Brand</strong></td>
    <td style="background-color: #DBEAF9"><strong>Brand Child</strong></td>
    <td style="background-color: #DBEAF9"><strong>Category</strong></td>
    <td style="background-color: #DBEAF9"><strong>Group</strong></td>
    <td style="background-color: #DBEAF9"><strong>Price Purchased</strong></td>
    <td style="background-color: #DBEAF9"><strong>Price Sell</strong></td>
</tr>

@foreach ($product as $item)
    <tr>
        <td>{{ $item->sku }}</td>
        <td>{{ $item->name }}</td>
        <td>{{ $item->brand->name ?? null }}</td>
        <td>{{ $item->model_parent->name ?? null }}</td>
        <td>{{ $item->model->name ?? null }}</td>
        <td>{{ $item->group ?? null }}</td>
        <td>{{ $item->price_purchase }}</td>
        <td>{{ $item->price }}</td>
    </tr>
@endforeach
</body>
</html>