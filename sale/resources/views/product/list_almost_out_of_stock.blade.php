<html>
<body>
<tr>
    <td style="background-color: #DBEAF9"><strong>SKU</strong></td>
    <td style="background-color: #DBEAF9" width="100"><strong>Name</strong></td>
    <td style="background-color: #DBEAF9"><strong>Amount</strong></td>
</tr>

@foreach ($product as $item)
    <tr>
        <td>{{ $item->sku }}</td>
        <td>{{ $item->name }}</td>
        <td>{{ $item->storage_has_product->sum('amount') }}</td>
    </tr>
@endforeach
</body>
</html>