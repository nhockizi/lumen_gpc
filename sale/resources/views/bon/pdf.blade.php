<!DOCTYPE html>
<html>

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <meta charset="utf-8"/>
    <title>Invoice</title>
</head>

<body style="background-color: white; color: black; font-size: 10px; font-family: sans-serif; border: 0px solid gray; color: dimgrey;">
<table style="width: 100%;">
    <tr>
        <td style="font-weight: bold;font-size: 20px;text-align: right">
            {{$data['commercial']}}
        </td>
    </tr>
</table>
<br><br>
<table border="1" cellspacing="0" cellpadding="4" style="width: 100%;">
    <tr>
        <th align="center" bgcolor="#585858" color="#FFFFFF">Klant</th>
        <th align="center" bgcolor="#585858" color="#FFFFFF">Datum</th>
        <th align="center" bgcolor="#585858" color="#FFFFFF">Type</th>
        <th align="center" bgcolor="#585858" color="#FFFFFF">Location</th>
    </tr>
    <tr>
        <td align="center">
            {{$data['customer_name']}}
        </td>
        <td align="center">
            {{$data['date']}}
        </td>
        <td align="center">
            {{$data['type']}}
        </td>
        <td align="center">
            {{$data['location']}}
        </td>
    </tr>
</table>
<br><br>
@if ($data['note'] != null)
    <p style="font-size:12px;">
        <b>Notitie:</b>
        {{--{{$data['note']}}--}}
    </p>
    <br><br>
@endif
@if ($data['type'] == 3 || $data['type'] == 4)
<table border="1" cellspacing="0" cellpadding="4" style="width: 100%;">
    <tr>
        <th align="center" bgcolor="#585858" color="#FFFFFF">Street</th>
        <th align="center" bgcolor="#585858" color="#FFFFFF">City</th>
        <th align="center" bgcolor="#585858" color="#FFFFFF">Postal code</th>
        <th align="center" bgcolor="#585858" color="#FFFFFF">Country</th>
        <th align="center" bgcolor="#585858" color="#FFFFFF">Type</th>
    </tr>
    <tr>
        <td align="center">
            {{$data['street']}}
        </td>
        <td align="center">
            {{$data['city']}}
        </td>
        <td align="center">
            {{$data['post_code']}}
        </td>
        <td align="center">
            {{$data['country']}}
        </td>
        <td align="center">
            {{$data['shipping_type']}}
        </td>
    </tr>
</table>
<br><br>
@endif
<table border="1" cellspacing="0" cellpadding="4" style="width: 100%;">
    <tr>
        <th align="center" bgcolor="#585858" color="#FFFFFF">Check</th>
        <th align="center" bgcolor="#585858" color="#FFFFFF">Omschrijving</th>
        <th align="center" bgcolor="#585858" color="#FFFFFF">Locatie</th>
        @if ($data['view_price'] == true)
        <th align="center" bgcolor="#585858" color="#FFFFFF">Prijs per stuk</th>
        @endif
        <th align="center" bgcolor="#585858" color="#FFFFFF">Aantal</th>
        @if ($data['view_price'] == true)
        <th align="center" bgcolor="#585858" color="#FFFFFF">Bedrag</th>
        @endif
    </tr>
    @foreach ($data['invoice'] as $invoice)
        <tr>
            <td align="center">
                {{$invoice['check']}}
            </td>
            <td align="center">
                {{$invoice['name']}}
            </td>
            <td align="center">
                @if (is_array($invoice['location']))
                    @foreach ($invoice['location'] as $key => $location)
                        @if ($key > 0)
                            <br/>
                        @endif
                        {{$location['rack']}}
                        |
                        {{$location['row']}}
                        |
                        {{$location['column']}}
                    @endforeach
                @endif
            </td>
            @if ($data['view_price'] == true)
            <td align="center">
                {{$invoice['price']}}
            </td>
            @endif
            <td align="center">
                {{$invoice['number']}}
            </td>
                @if ($data['view_price'] == true)
            <td align="center">
                {{$invoice['amount']}}
            </td>
            @endif
        </tr>
    @endforeach
    @if ($data['view_price'] == true)
        <tr>
            <td colspan="3" style="border-color:#fff;"></td>
            <td colspan="2" >
                Subtotaal &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b><span style="font-size:13px;">{{$data['total_number']}}</span></b>
            </td>
            <td style="text-align: center;">
                <span>{{$data['sub_total']}}</span>
            </td>
        </tr>
        <tr>
            <td colspan="3" style="border-color:#fff;"></td>
            <td colspan="2" >
                Tax amount
            </td>
            <td style="text-align: center;">
                <span>{{$data['tax']}}</span>
            </td>
        </tr>
        <tr>
            <td colspan="3" style="border-color:#fff;"></td>
            <td colspan="2" >
                Verzendkosten
            </td>
            <td style="text-align: center;">
                <span>{{$data['shipping']}}</span>
            </td>
        </tr>

        <tr>
            <td colspan="3" style="border-color:#fff;"></td>
            <td colspan="2" >
                Aanbetaling
            </td>
            <td style="text-align: center;">
                <span>{{$data['payment']}}</span>
            </td>
        </tr>

        <tr style="font-size:13px;">
            <td colspan="3" style="border-color:#fff;"></td>
            <td colspan="2" >
                <b>Totaal</b>
            </td>
            <td style="text-align: center;">
                <span><b>€ {{$data['total']}}</b></span>
            </td>
        </tr>
    @else
        <tr>
            <td colspan="2" style="border-color:#fff;"></td>
            <td>
                <b>
                    Totaal Qty
                </b>
            </td>
            <td style="text-align: center;">
                <span>
                    <b>
                        {{$data['total_number']}}
                    </b>
                </span>
            </td>
        </tr>
    @endif
</table>
<br><br>

<table border="1" cellspacing="0" cellpadding="4" style="font-size:10px;width: 100%">
    <tr>
        <th align="center" bgcolor="#585858  " color="#FFFFFF">Uw bestelling is met zorg verwerkt door</th>
    </tr>
    <tr>
        <td align="left">&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp; Akoord medewerker: </td>
    </tr>

</table>
</body>

</html>