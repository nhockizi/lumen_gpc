<html>
<body>
<tr>
    <td style="background-color: #DBEAF9"><strong>SKU</strong></td>
    <td style="background-color: #DBEAF9" width="100"><strong>Location</strong></td>
    <td style="background-color: #DBEAF9"><strong>Name</strong></td>
    <td style="background-color: #DBEAF9"><strong>Quantity</strong></td>
</tr>

@foreach ($data as $item)
    @foreach ($item->product as $product)
        <tr>
            <td>{{ $product->sku }}</td>
            <td>{{ $item->rack .'-'.$item->row .'-'.$item->column }}</td>
            <td>{{ $product->name }}</td>
            <td>{{ $product->amount }}</td>
        </tr>
    @endforeach
@endforeach
</body>
</html>