<?php

namespace App\Repositories;

use App\Models\Storage;
use App\Models\StorageAbsolute;

class StorageRepo extends BaseRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        // TODO: Implement model() method.
        return Storage::class;
    }

    public function listRack($locationID)
    {
        $result = [];
        switch ($locationID) {
            case 4:
                $rack = [
                    'CXA' => 'Rack CXA',
                    'CXB' => 'Rack CXB',
                    'CXC' => 'Rack CXC',
                    'CXD' => 'Rack CXD',
                    'CXE' => 'Rack CXE',
                    'CXF' => 'Rack CXF',
                    'CXG' => 'Rack CXG',
                    'CXH' => 'Rack CXH',
                    'CXI' => 'Rack CXI',
                    'CXJ' => 'Rack CXJ',
                    'CXK' => 'Rack CXK',
                    'CXL' => 'Rack CXL',
                    'CXM' => 'Rack CXM',
                ];
                break;
            default:
                $rack = [
                    'A'  => 'Rack A',
                    'AA' => 'Rack AA',
                    'B'  => 'Rack B',
                    'B1' => 'Rack B1',
                    'BB' => 'Rack BB',
                    'C'  => 'Rack C',
                    'D'  => 'Rack D',
                    'E'  => 'Rack E',
                    'F'  => 'Rack F',
                    'G'  => 'Rack G',
                    'H'  => 'Rack H',
                    'I'  => 'Rack I',
                    'II' => 'Rack II',
                    'J'  => 'Rack J',
                    'JJ' => 'Rack JJ',
                    'K'  => 'Rack K',
                    'L'  => 'Rack L',
                    'M'  => 'Rack M',
                    'N'  => 'Rack N',
                    'O'  => 'Rack O',
                    'P'  => 'Rack P',
                    'Q'  => 'Rack Q',
                ];
                break;
        }
        foreach ($rack as $key => $value) {
            $result[] = [
                'key'   => $key,
                'value' => $value,
            ];
        }

        return $result;
    }

    public function list($data)
    {
        $storageAbsolute = StorageAbsolute::where('rack', $data['rack']);
        if(isset($data['rack_index'])){
            $storageAbsolute = $storageAbsolute->whereIn('row',$data['rack_index']);
        }
        $storageAbsolute = $storageAbsolute->orderBy('row')
                                           ->orderBy('drawer')
                                           ->get();
        $result          = [];
        foreach ($storageAbsolute as $item) {
            $tmp          = (object)[];
            $tmp->rack    = $item->rack;
            $tmp->row     = $item->row;
            $tmp->column  = $item->drawer;
            $tmp->amount  = 0;
            $tmp->product = [];

            $storage_data = Storage::with(
                [
                    'storage_has_product',
                    'storage_has_product.product',
                ]
            )
                                   ->where('rack', '!=', "")
                                   ->where('row', '!=', "")
                                   ->where('column', '!=', "")
                                   ->whereHas(
                                       'storage_has_product', function ($q) use ($data) {
                                       if (isset($data['location_id'])) {
                                           return $q->where('location_ID', $data['location_id']);
                                       }
                                   }
                                   )
                                   ->when(
                                       array_get($data, 'rack'), function ($query, $rack) {
                                       $query->where('rack', $rack);
                                   }
                                   )
                                   ->where('column', $item->drawer)
                                   ->where('row', $item->row)
                                   ->groupBy('row')
                                   ->groupBy('column')
                                   ->get();
            foreach ($storage_data as $storage) {
                foreach ($storage->storage_has_product as $storage_has_product) {
                    if ($tmp->amount === 0) {
                        $tmp->amount = $storage_has_product->amount;
                    }
                    if ($tmp->amount != 0 && $tmp->amount < $storage_has_product->amount) {
                        $tmp->amount = $storage_has_product->amount;
                    }
                    if (isset($storage_has_product->product)) {
                        $storage_has_product->product->amount = $storage_has_product->amount;
                        $tmp->product[]                       = $storage_has_product->product;
                    }
                }
            }
            $result[] = $tmp;
        }

        return $result;
    }
}