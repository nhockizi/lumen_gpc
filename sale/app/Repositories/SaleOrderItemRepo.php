<?php

namespace App\Repositories;

use App\Libraries\Helpers;
use App\Models\Sales;
use App\Models\SalesOrder;
use App\Models\SalesOrderItem;

class SaleOrderItemRepo extends BaseRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        // TODO: Implement model() method.
        return SalesOrderItem::class;
    }

}