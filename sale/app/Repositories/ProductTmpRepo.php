<?php

namespace App\Repositories;

use App\Libraries\Helpers;
use App\Models\Brand;
use App\Models\ImageTmp;
use App\Models\Model;
use App\Models\Product;
use App\Models\ProductImages;
use App\Models\ProductTmp;
use App\Models\ProductType;
use Illuminate\Support\Facades\File;

class ProductTmpRepo extends BaseRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        // TODO: Implement model() method.
        return ProductTmp::class;
    }

    public function list($data)
    {
        $item_length = $data['length'] ?? ITEM_LENGHT;
        //        $fieldsSearch = [
        //            'name' => ['product_type', 'product_type_name', 'LIKE'],
        //            'code' => ['product_type', 'product_type_code', 'LIKE'],
        //        ];
        $query = ProductTmp::with(
            [
                'location_data',
            ]
        )
                           ->where('saved', INACTIVE);
        //        $query        = Helpers::searchFieldsMapping($query, $fieldsSearch);
        $result = $query->paginate($item_length);
        $result = Helpers::formatPagination($result);

        return $result;
    }
    public function _insert_data($data)
    {
        $user     = Helpers::getAuth();
        $data_row = Helpers::getRandomString(4)
                    .date('dm')
                    .$user->member_ID
                    .Helpers::getRandomString(4)
                    .date('His');
        if ($data['data_row'] == null) {
            $data['data_row'] = $data_row;
        }
        $productTmp = ProductTmp::where('data_row', $data['data_row'])->first();
        if (!$productTmp) {
            $productTmp = new ProductTmp();
        }
        $productTmp->data_row = $data['data_row'] ?? $data_row;;
        $productTmp->name            = $data['name'] ?? null;
        $productTmp->color           = $data['color'] ?? null;
        $productTmp->extra_field     = $data['extra_field'] ?? null;
        $productTmp->quality_label   = $data['quality_label'] ?? null;
        $productTmp->group_brand_id  = ($data['filter'] != null) ? json_encode($data['filter']) : null;
        $productTmp->product_type_ID = $data['product_type'] ?? null;
        $productTmp->brand_brand_ID  = $data['brand'] ?? null;
        $productTmp->model_parent_ID = $data['model'] ?? null;
        $productTmp->brand_parent    = $data['brand_parent'] ?? null;
        $productTmp->price           = $data['price_s'] ?? null;
        $productTmp->price_purchase  = $data['price_p'] ?? null;
        $productTmp->amount          = $data['amount'] ?? null;
        $productTmp->rack            = $data['rack'] ?? null;
        $productTmp->row             = $data['row'] ?? null;
        $productTmp->column          = $data['column'] ?? null;
        $productTmp->location        = $data['location'] ?? null;
        $productTmp->model_ID        = ($data['model_children'] != null) ? json_encode($data['model_children']) : null;
        $productTmp->saved           = INACTIVE;
        $productTmp->save();
        $data_row = $productTmp->data_row;
        if($data['images_old']){
            ImageTmp::whereNotIn('id',$data['images_old'])->where('data_row',$data_row)->delete();
        }
        if ($data['images']) {
            $url_image = 'product-tmp/'.$data_row;
            foreach ($data['images'] as $item) {
                File::makeDirectory($url_image, 0777, true, true);
                $storage_path = storage_path('app/public/product/tmp/'.$item['source']);
                $new_url      = public_path($url_image.'/'.$item['source']);
                File::move($storage_path, $new_url);

                $tmp           = new ImageTmp();
                $tmp->data_row = $data_row;
                $tmp->image_ID = $data_row;
                $tmp->name     = $item['source'];
                $tmp->path     = $new_url;
                $tmp->save();
            }
        }

        return $productTmp;
    }
    public function getDataByRow($data){
        $data = ProductTmp::with('image_tmp')->whereIn('data_row', $data)->get();

        $result = [];
        foreach ($data as $item) {
            $tmp = [
                'data_row' => $item->data_row,
                'sku' => $item->sku,
                'name' => $item->name,
                'color' => $item->color,
                'extra_field' => $item->extra_field,
                'quality_label' => $item->quality_label,
                'group_brand_id' => $item->group_brand_id,
                'product_type_ID' => $item->product_type_ID,
                'brand_brand_ID' => $item->brand_brand_ID,
                'model_parent_ID' => $item->model_parent_ID,
                'brand_parent' => $item->brand_parent,
                'price' => $item->price,
                'price_purchase' => $item->price_purchase,
                'amount' => $item->amount,
                'rack' => $item->rack,
                'row' => $item->row,
                'column' => $item->column,
                'location' => $item->location,
                'model_ID' => $item->model_ID,
                'images' => null
            ];
            if($item->image_tmp){
                $tmp['images'] = [];
                foreach ($item->image_tmp as $image_tmp){
                    $tmp['images'][] = [
                        'id' => $image_tmp->id,
                        'url' => env('APP_URL') . '/product-tmp/' . $image_tmp->image_ID . '/' . $image_tmp->name
                    ];
                }
            }
            $result[] = $tmp;
        }
        return $result;
    }
    public function save($data)
    {
        $productTmp = ProductTmp::whereIn('data_row', $data)->get();
        $_brand     = BrandRepo::getInstance();
        foreach ($productTmp as $item) {
            $_product  = ProductRepo::getInstance();
            $item->sku = $_brand->find($item->brand_brand_ID)->brand_CODE.$_product->getNewProduct();
            $this->_saveproducttmp($item);
            $item->saved = 1;
            $item->save();
        }
    }

    private function _saveproducttmp($row)
    {
        $product          = new Product();
        $product->sku     = $row->sku;
        $product->name    = $row->name;
        $product->name_en = $row->name;

        $product->description    = $this->_buildDescription($row, true);
        $product->description_en = $this->_buildDescription($row, false);

        $product->price           = $row->price;
        $product->price_purchase  = $row->price_purchase;
        $product->color           = $row->color;
        $product->extra_field     = $row->extra_field;
        $product->quality_label   = $row->quality_label;
        $product->group_brand_id  = $row->group_brand_id;
        $product->product_type_ID = $row->product_type_ID;
        $product->brand_brand_ID  = $row->brand_brand_ID;
        $product->model_parent_ID = $row->model_parent_ID;
        //        $product->brand_parent    = $row->brand_parent;

        $chilModel = json_decode($row->model_ID);
        if (is_array($chilModel) && count($chilModel) > 0) {
            $product->model_ID = $chilModel[0];
        }

        $product->extra_model = $row->model_ID;
        $product->save();
        $imageTmp  = ImageTmp::where('data_row', $row->data_row)->get();
        $url_image = 'product';
        $_product  = ProductRepo::getInstance();
        foreach ($imageTmp as $key => $img) {
            if (file_exists($img->path)) {
                $is_thumbnail = 0;
                if ($key == 0) {
                    $is_thumbnail = 1;
                }
                $image                     = new ProductImages();
                $image->product_product_ID = $product->product_ID;
                $image->image_url          = $img->name;
                $image->is_thumbnail       = $is_thumbnail;
                if ($image->save()) {
                    $new_url = public_path($url_image.'/'.$product->product_ID);
                    File::makeDirectory($new_url, 0777, true, true);
                    File::copy($img->path, $new_url.'/'.$img->name);
                }
            }
        }
        $storage['storage'][] = [
            'rack'     => $row->rack,
            'row'      => $row->row,
            'column'   => $row->column,
            'amount'   => $row->amount,
            'location' => $row->location,
        ];
        $_product->_saveStorageProduct($storage, $product->product_ID);

    }

    private function _buildDescription($row, $nl = true)
    {
        $brand          = Brand::where('brand_ID', $row->brand_brand_ID)->first();
        $brandName      = $brand->name ?? null;
        $productType    = ProductType::where('product_type_ID', $row->product_type_ID)->first();
        $productType_nl = $productType->description_nl ?? null;
        $productType_en = $productType->description_en ?? null;
        $extraModels    = json_decode($row->model_ID);
        $models         = Model::whereIn('model_ID', $extraModels)->get();
        $modelName      = [];
        foreach ($models as $model) {
            $modelName[] = $model->name;
        }
        $modelNameText   = implode("/", $modelName);
        $productTypeDesc = ($nl) ? $productType_nl : $productType_en;

        return $brandName.' '.$modelNameText.' '.$productTypeDesc;
    }

    public function rollback($data)
    {
    }

    public function delete($data)
    {
        ProductTmp::whereIn('data_row', $data)->delete();
        ImageTmp::whereIn('data_row', $data)->delete();
    }
}