<?php
namespace App\Repositories;

use App\Models\LogSync;
use App\Repositories\BaseRepository;

class LogSyncRepo extends BaseRepository
{

    public function model()
    {
        return LogSync::class;
    }

    static public $logSync = false;
}
