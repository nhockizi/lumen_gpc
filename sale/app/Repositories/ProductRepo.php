<?php

namespace App\Repositories;

use App\Libraries\Helpers;
use App\Models\Brand;
use App\Models\Product;
use App\Models\ProductImages;
use App\Models\ProductNotification;
use App\Models\StorageHasProduct;
use Illuminate\Support\Facades\File;

class ProductRepo extends BaseRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public $_brand_group
        = [
            [
                'id'   => 1,
                'name' => 'Spare parts',
            ],
            [
                'id'   => 2,
                'name' => 'Accessories',
            ],
            [
                'id'   => 3,
                'name' => 'Repair Tools',
            ],
        ];

    public function model()
    {
        // TODO: Implement model() method.
        return Product::class;
    }

    public function list($data, $isReady = true)
    {
        $item_length  = $data['length'] ?? ITEM_LENGHT;
        $fieldsSearch = [
            'product_name'    => [
                'product',
                'name',
                'LIKE',
            ],
            'brand_id'        => [
                'product',
                'brand_brand_ID',
                '=',
            ],
            'product_type_id' => [
                'product',
                'product_type_ID',
                '=',
            ],
            'model_parent_id' => [
                'product',
                'model_parent_ID',
                '=',
            ],
            'model_id'        => [
                'product',
                'model_ID',
                '=',
            ],
        ];
        $query        = Product::with(
            [
                'brand',
                'model',
                'model_parent',
                'product_type',
                'storage_has_product',
            ]
        )
                               ->where('is_ready', $isReady);
        if ($data['filter_search']) {
            $query = $query->where(
                function ($q) use ($data) {
                    $filter_search = explode(' ', $data['filter_search']);
                    foreach ($filter_search as $key => $filter) {
                        $q->where(
                            function ($q) use ($filter) {

                                $q->where('name', 'like', '%'.$filter.'%')
                                  ->orWhere('description', 'like', '%'.$filter.'%')
                                  ->orWhere('sku', 'like', '%'.$filter.'%');

                            }
                        );
                    }
                }
            );
        }
        if ($data['group_brand_id']) {
            if (is_array($data['group_brand_id'])) {
                $query = $query->where(
                    function ($q) use ($data) {
                        foreach ($data['group_brand_id'] as $key => $item) {
                            if ($key == 0) {
                                $q->where('group_brand_id', 'like', '%"'.$item.'"%');
                            } else {
                                $q->orWhere('group_brand_id', 'like', '%"'.$item.'"%');
                            }
                        }
                    }
                );
            }
        }
        $query      = Helpers::searchFieldsMapping($query, $fieldsSearch);
        $fieldsSort = [
            'id'    => [
                'product',
                'product_ID',
            ],
            'sku'   => [
                'product',
                'sku',
            ],
            'name'  => [
                'product',
                'name',
            ],
            'price' => [
                'product',
                'price',
            ],
        ];
        $query      = Helpers::sortFieldsMapping($query, $fieldsSort, $data['order'], $data['type']);
        $result     = $query->paginate($item_length);
        $result     = Helpers::formatPagination($result);
        $rows       = [];
        foreach ($result['rows'] as $key => $item) {
            $product_type    = [];
            $brand           = [];
            $model           = [];
            $model_parent    = [];
            $storage_product = [];
            try {
                $group_brand_id = json_decode($item->group_brand_id, true);
                $group_brand    = Brand::whereIn('brand_ID', $group_brand_id)->get();
            } catch (\Exception $e) {
                $group_brand = [];
            }

            if (isset($item->product_type)) {
                $product_type = [
                    'product_type_name' => $item->product_type->product_type_name ?? null,
                    'product_type_code' => $item->product_type->product_type_code ?? null,
                    'description_nl'    => $item->product_type->description_nl ?? null,
                    'description_en'    => $item->product_type->description_en ?? null,
                ];
            }
            if (isset($item->brand)) {
                $brand = [
                    'name'      => $item->brand->name ?? null,
                    'code'      => $item->brand->brand_CODE ?? null,
                    'group_id'  => $item->brand->group_id ?? null,
                    'parent_id' => $item->brand->parent_id ?? null,
                ];
            }
            if (isset($item->model)) {
                $model = [
                    'name'      => $item->model->name ?? null,
                    'brand_id'  => $item->model->brand_brand_ID ?? null,
                    'parent_id' => $item->model->parent_ID ?? null,
                ];
            }
            if (isset($item->model_parent)) {
                $model_parent = [
                    'name'      => $item->model_parent->name ?? null,
                    'brand_id'  => $item->model->brand_brand_ID ?? null,
                    'parent_id' => $item->model->parent_ID ?? null,
                ];
            }
            if (isset($item->storage_has_product)) {
                $storage_product = [];
                foreach ($item->storage_has_product as $storage_has_product) {
                    $storage_product[] = [
                        'id'          => $storage_has_product->id,
                        'amount'      => $storage_has_product->amount,
                        'rack'        => $storage_has_product->storage->rack,
                        'row'         => $storage_has_product->storage->row,
                        'column'      => $storage_has_product->storage->column,
                        'storage_id'  => $storage_has_product->storage_storage_ID,
                        'location_id' => $storage_has_product->location_ID,
                    ];
                }
            }
            $brand_group    = $this->_brand_group;
            $group_brand_id = json_decode($item->group_brand_id);
            if ($group_brand_id != null) {
                if (count($group_brand_id) > 0) {
                    foreach ($brand_group as $k => $v) {
                        if (in_array($v['id'], $group_brand_id)) {
                            $brand_group[$k]['is_check'] = (boolean)true;
                        }
                    }
                }
            } else {
                $group_brand_id = null;
            }
            $rows[] = [
                'id'                  => $item->product_ID,
                'sku'                 => $item->sku,
                'link'                => URL_MAIN.$item->sku,
                'factory_code'        => $item->factory_code,
                'name'                => $item->name,
                'name_en'             => $item->name_en,
                'description'         => $item->description,
                'description_en'      => $item->description_en,
                'image'               => $item->image,
                'price'               => $item->price,
                'price_wholesale'     => $item->price_wholesale,
                'price_purchase'      => $item->price_purchase,
                'sold'                => $item->sold,
                'color'               => $item->color,
                'brand_id'            => $item->brand_brand_ID,
                'product_type_id'     => $item->product_type_ID,
                'product_type'        => $product_type,
                'keyword'             => $item->keyword,
                'model_parent_id'     => $item->model_parent_ID,
                'model_id'            => $item->model_ID,
                'extra_field'         => $item->extra_field,
                'group_brand_id'      => $group_brand_id,
                'group_brand'         => $group_brand,
                'extra_model'         => $item->extra_model,
                'extra_product_code'  => $item->extra_product_code,
                'quality_label'       => $item->quality_label,
                'brand'               => $brand,
                'model'               => $model,
                'model_parent'        => $model_parent,
                'storage_has_product' => $storage_product,
                'brand_group'         => $brand_group,
                'quantity'            => $item->storage_has_product()->sum('amount'),
                'created_at'          => $item->created_at,
                'updated_at'          => $item->updated_at,
            ];
        }
        $result['rows'] = $rows;

        return $result;
    }
    public function getByListSku($sku){
        $result = Product::with(
            [
                'brand',
                'model',
                'model_parent',
                'product_type',
                'storage_has_product',
                'product_images',
            ]
        )
                         ->whereIn('sku', $sku)
                         ->get();
        foreach ($result as $item) {
            if ($item->group_brand_id != null) {
                $item->group_brand_id = json_decode($item->group_brand_id);
            }
            if ($item->extra_model != null) {
                $item->extra_model = json_decode($item->extra_model);
            }
            foreach ($item->storage_has_product as $storage_has_product) {
                $storage_has_product->storage;
            }
            foreach ($item->product_images as $image) {
                $image->image_url    = env('APP_URL') . '/product/' . $item->product_ID . '/' . $image->image_url;
                $image->is_thumbnail = (boolean)$image->is_thumbnail;
            }
        }
        return $result;
    }
    public function getByListId($ids){
        $result = Product::with(
            [
                'brand',
                'model',
                'model_parent',
                'product_type',
                'storage_has_product',
                'product_images',
            ]
        )
                         ->whereIn('product_ID', $ids)
                         ->get();
        foreach ($result as $item) {
            if ($item->group_brand_id != null) {
                $item->group_brand_id = json_decode($item->group_brand_id);
            }
            if ($item->extra_model != null) {
                $item->extra_model = json_decode($item->extra_model);
            }
            foreach ($item->storage_has_product as $storage_has_product) {
                $storage_has_product->storage;
            }
            foreach ($item->product_images as $image) {
                $image->image_url    = env('APP_URL') . '/product/' . $item->product_ID . '/' . $image->image_url;
                $image->is_thumbnail = (boolean)$image->is_thumbnail;
            }
        }
        return $result;
    }
    public function detail($id)
    {
        $result = Product::with(
            [
                'brand',
                'model',
                'model_parent',
                'product_type',
                'storage_has_product',
                'product_images',
            ]
        )
                         ->where('product_ID', $id)
                         ->first();
        if ($result->group_brand_id != null) {
            $result->group_brand_id = json_decode($result->group_brand_id);
        }
        if ($result->extra_model != null) {
            $result->extra_model = json_decode($result->extra_model);
        }
        foreach ($result->storage_has_product as $storage_has_product) {
            $storage_has_product->storage;
        }
        foreach ($result->product_images as $image) {
            $image->image_url    = env('APP_URL').'/product/'.$result->product_ID.'/'.$image->image_url;
            $image->is_thumbnail = (boolean)$image->is_thumbnail;
        }

        return $result;
    }

    public function getNewProduct()
    {
        return Product::max('product_ID') + 1;
    }

    public function _save($data, $type = 'create')
    {
        $data['sku']       = $this->generateSku($data['product_id'], $data['brand']);
        $attributesProduct = [
            'product_ID' => $data['product_id'],
        ];
        $valuesProduct     = [
            'sku'                => $this->generateSku($data['product_id'], $data['brand']),
            'name'               => (string)$data['product_name'],
            'name_en'            => (string)$data['product_name_en'],
            'description'        => (string)$data['description'],
            'description_en'     => (string)$data['description_en'],
            'price'              => (double)$data['sell_price'],
            'price_purchase'     => (double)$data['price_purchased'],
            'color'              => (string)$data['product_color'],
            'brand_brand_ID'     => (int)$data['brand'],
            'product_type_ID'    => (int)$data['product_type'],
            'model_parent_ID'    => (int)$data['model'],
            'model_ID'           => (int)$data['model_child'],
            'extra_field'        => (string)$data['extra_field'],
            'extra_product_code' => (string)$data['extra_product_code'],
            'quality_label'      => $data['quality'],
        ];
        if ($data['product_group'] != null) {
            $tmp = [];
            foreach ($data['product_group'] as $product_group) {
                $tmp[] = (int)$product_group;
            }
            $valuesProduct['group_brand_id'] = json_encode($tmp);
        }
        if ($data['extra_model_children'] != null) {
            $tmp = [];
            foreach ($data['extra_model_children'] as $extra_model_children) {
                $tmp[] = (int)$extra_model_children;
            }
            $valuesProduct['extra_model'] = json_encode($tmp);
        }
        $product = $this->updateOrCreate($attributesProduct, $valuesProduct);

        $this->_saveStorageProduct($data, $product->product_ID);
        if ($data['images'] != null) {
            $url_image     = 'product/'.$product->product_ID;
            $_productImage = ProductImagesRepo::getInstance();
            foreach ($data['images'] as $key => $item) {
                $deleted = $item['deleted'] ?? false;
                if ($deleted == false) {
                    $attributesProductImage = [
                        'image_ID' => $item['id'],
                    ];
                    if ($item['id'] == null) {
                        $valuesProductImage = [
                            'product_product_ID' => $product->product_ID,
                            'is_thumbnail'       => (boolean)$item['thumbnail'] ?? false,
                            'image_url'          => $item['image_url'],
                            'source'             => $item['source'],
                        ];
                        File::makeDirectory($url_image, 0777, true, true);
                        $storage_path = storage_path('app/public/product/tmp/'.$item['source']);
                        $new_url      = public_path($url_image.'/'.$item['image_url']);
                        File::move($storage_path, $new_url);
                    } else {
                        $valuesProductImage = [
                            'is_thumbnail' => (boolean)$item['thumbnail'] ?? false,
                        ];
                    }
                    $_productImage->updateOrCreate($attributesProductImage, $valuesProductImage);
                } else {
                    if ($item['id'] != null) {
                        ProductImages::where('image_ID', $item['id'])->delete();
                    }
                }
            }
        }

        return $product;
    }

    public function generateSku($product_id, $brand_id)
    {
        $brand = Brand::where('brand_ID', $brand_id)->first();
        $sku   = $brand->brand_CODE."".$product_id;

        return $sku;
    }

    public function _saveStorageProduct($data, $productId)
    {
        $storageHasProductIds = [];
        $_stockChange         = StockChangeRepo::getInstance();
        $_storageHasProduct   = StorageHasProductRepo::getInstance();
        $_storage             = StorageRepo::getInstance();
        if ($data['storage'] != null) {
            foreach ($data['storage'] as $key => $item) {
                $storageHasProductId = $item['id'] ?? null;
                $storage_ID          = null;
                $stockOld            = 0;
                if ($storageHasProductId != null) {
                    $storageHasProductData = $_storageHasProduct->find($storageHasProductId);
                    $storage_ID            = $storageHasProductData->storage_storage_ID;
                    $stockOld              = $storageHasProductData->amount ?? 0;
                }
                $attributesStorage           = [
                    'storage_ID' => $storage_ID,
                ];
                $valuesStorage               = [
                    'rack'    => $item['rack'] ?? null,
                    'row'     => $item['row'] ?? null,
                    'column'  => $item['column'] ?? null,
                ];
                $storage                     = $_storage->updateOrCreate($attributesStorage, $valuesStorage);
                $attributesStorageHasProduct = [
                    'id' => $storageHasProductId,
                ];
                if (isset($item['location'])) {
                    $location = $item['location'];
                } else {
                    $location = $item['location_id'];
                }
                $valuesStorageHasProduct = [
                    'amount'             => $item['amount'],
                    'location_ID'        => $location,
                    'product_product_ID' => $productId,
                    'storage_storage_ID' => $storage->storage_ID,
                ];
                $storageHasProduct       = $_storageHasProduct->updateOrCreate(
                    $attributesStorageHasProduct,
                    $valuesStorageHasProduct
                );
                $_stockChange->changeStock(
                    [
                        'amount_old' => $stockOld,
                        'amount_new' => $storageHasProduct->amount,
                        'product_id' => $productId,
                        'storage_id' => $storageHasProduct->storage_storage_ID,
                    ]
                );
                $storageHasProductIds[] = $storageHasProduct->id;
            }
        }

        $StorageHasProductDelete = StorageHasProduct::whereNotIn('id', $storageHasProductIds)
                                                    ->where('product_product_ID', $productId)
                                                    ->get();
        foreach ($StorageHasProductDelete as $item) {
            $_stockChange->changeStock(
                [
                    'amount_old' => $item->amount,
                    'amount_new' => 0,
                    'product_id' => $productId,
                    'storage_id' => $item->storage_storage_ID,
                ]
            );
            $item->delete();
        }
    }

    public function _saveNotificationQuantity($data)
    {
        $authUser   = Helpers::getAuth();
        $authUserId = $authUser->member_ID;

        $result     = ProductNotification::where('user_id', $authUserId)
                                         ->where('product_id', $data['id'])
                                         ->first();
        $attributes = [
            'id' => null,
        ];
        if ($result) {
            $attributes['id'] = $result->id;
        }
        $values               = [
            'user_id'    => $authUserId,
            'product_id' => $data['id'],
            'qty'        => $data['quantity'],
        ];
        $_productNotification = ProductNotificationRepo::getInstance();
        $_productNotification->updateOrCreate($attributes, $values);
    }

    public function getQuantityNotification($id)
    {
        $authUser   = Helpers::getAuth();
        $authUserId = $authUser->member_ID;
        $result     = ProductNotification::where('user_id', $authUserId)
                                         ->where('product_id', $id)
                                         ->first();

        return $result->qty ?? null;
    }

    public function getAllOutOfStock($minAmount = 0)
    {
        $result = Product::with('storage_has_product')
                         ->whereHas(
                             'storage_has_product', function ($query) use ($minAmount) {
                             $query->groupBy('product_product_ID')
                                   ->havingRaw("sum(amount) <= ".$minAmount);
                         }
                         )
                         ->get();

        return $result;
    }

    public function exportPriceList()
    {
        $result = Product::all()->map(
            function ($item) {
                $item->group = null;
                if ($item->group_brand_id != null) {
                    $groupBrandId = json_decode($item->group_brand_id, true);
                    if (!empty($groupBrandId)) {
                        $group = [];
                        foreach ($groupBrandId as $id) {
                            if (isset($this->_brand_group[$id]['name'])) {
                                $group[] = $this->_brand_group[$id]['name'];
                            }
                        }
                        $item->group = implode(", ", $group);
                    }
                }

                return $item;
            }
        );

        return $result;
    }

    public function getStockChange($id)
    {
        $product      = $this->find($id);
        $result       = [
            'id'            => $product->product_ID,
            'sku'           => $product->sku,
            'name'          => $product->name,
            'current_stock' => (int)$product->storage_has_product()->sum('amount'),
            'stock_change'  => null,
        ];
        $stock_change = $product->stock_change()
                                ->groupBy('random_code')
                                ->orderBy('time', 'ASC')
                                ->selectRaw('*,sum(amount_old) as sum_amount_old,sum(amount_new) as sum_amount_new')
                                ->get();
        foreach ($stock_change as $item) {
            $old = $item->sum_amount_old;
            $new = $item->sum_amount_new;
            if ($new > $old) {
                $check = $new - $old;
            } else {
                if ($old > $new) {
                    $check = $old - $new;
                } else {
                    $check = 0;
                }
            }
            if (!$check) {
                continue;
            }
            $message = null;
            if ($new > $old) {
                $isAdded = true;
                if ($item->difference == 'refund') {
                    if ($item->bon_id) {
                        $message = "On "
                                   .date('d-m-Y', strtotime($item->time))
                                   ." a total of "
                                   .$item->sum_amount_new
                                   ." pcs was refunded to stock for invoice <a target='_blank' href='#'>#"
                                   .$item->bon_id
                                   ."</a>";
                    } else {
                        $message = "On "
                                   .date('d-m-Y', strtotime($item->time))
                                   ." a total of "
                                   .$item->sum_amount_new
                                   ." pcs was refunded to stock";
                    }
                } else {
                    if ($item->difference == 'delete') {
                        $message = "On "
                                   .date('d-m-Y', strtotime($item->time))
                                   ." a total of "
                                   .$item->sum_amount_new
                                   ." pcs was refunded to stock when deleted invoice #"
                                   .$item->bon_id;
                    } else {
                        $message = "On "
                                   .date('d-m-Y', strtotime($item->time))
                                   ." a total of "
                                   .$item->sum_amount_new
                                   ." pcs was added to stock";
                    }
                }
            } else {
                $isAdded = false;
                if ($item->bon_id) {
                    $message = "On "
                               .date('d-m-Y', strtotime($item->time))
                               ." a total of "
                               .$item->sum_amount_new
                               ." pcs was substracted to stock for invoice <a target='_blank' href='#'>#"
                               .$item->bon_id
                               ."</a>";
                } else {
                    $message = "On "
                               .date('d-m-Y', strtotime($item->time))
                               ." a total of "
                               .$item->sum_amount_new
                               ." pcs was substracted to stock";
                }
            }
            $result['stock_change'][] = [
                'id'         => $item->stock_change_ID,
                'time'       => $item->time,
                'amount_old' => $item->sum_amount_old,
                'amount_new' => $item->sum_amount_new,
                'isAdded'    => $isAdded,
                'message'    => $message,
            ];
        }

        return $result;
    }

    public function synchronize($data)
    {
        $product = $this->findWhereIn('product_ID', $data['id']);
        $list    = [
            [
                'sku',
                'categories',
                'price',
                'name',
                'product_type',
                'inventory_product_id',
                'multi_filter_type',
                'status',
                'qty',
                'is_in_stock',
                'manage_stock',
                'color',
                'description',
                'short_description',
                'store',
                'quality_label',
                'model_children',
            ],
        ];
        foreach ($product as $item) {
            for ($i = 0; $i < 3; $i++) {

                if ($i == 0) {
                    $name = $item->name;
                } else {
                    $name = $item->name_en ?? $item->name;
                }
                $qty = $product->storage_has_product->sum('amount');
                if ($i == 0) {
                    $description       = $item->description;
                    $short_description = $item->description;
                    $store             = 'default';
                } else {
                    if ($i == 1) {
                        $description       = $item->description_en;
                        $short_description = $item->description_en;
                        $store             = 'en,es,de,fr,it';
                    } else {
                        if ($i == 2) {
                            $description       = $item->description_en;
                            $short_description = $item->description_en;
                            $store             = '';
                        }
                    }
                }
                $modelChild = null;
                $list[]     = [
                    'sku'                  => $item->sku,
                    'nane'                 => $name,
                    'categories'           => null,
                    'price'                => $item->price,
                    'product_type'         => $item->product_type->product_type_name,
                    'inventory_product_id' => $item->product_ID,
                    'status'               => ACTIVE,
                    'qty'                  => $qty,
                    'is_in_stock'          => ($qty > 0) ? 1 : 0,
                    'manage_stock'         => ACTIVE,
                    'color'                => $item->color,
                    'description'          => $description,
                    'short_description'    => $short_description,
                    'store'                => $store,
                    'quality_label'        => $item->quality_label,
                    'model_children'       => $modelChild,
                ];
            }
            if ($item->product_images) {
                foreach ($item->product_images as $product_images) {
                    $img
                        = public_path(
                        'product/'
                        .$item->product_ID
                        .'/'
                        .$product_images->image_url
                    );
                    if (file_exists($img) && is_file($img)) {
                        copy($img, PATH_EXPORT_IMAGE."/".$product_images->image_url);
                    }
                }
            }
        }
        $path = PATH_EXPORT_CSV;
        if (!is_dir($path)) {
            mkdir($path, 0777, true);
        }

        // backup sync file
        $name_file = 'product_csv_'.date('Ymd-His').".csv";
        $fp        = fopen($path.$name_file, 'w');

        // real sync csv
        $_sync_file = PATH_IMPORT_CSV.SYNC_CSV_FILE;

        if (file_exists($_sync_file)) {
            unlink($_sync_file);
        }
        $fp_sync = fopen($_sync_file, 'w');

        foreach ($list as $fields) {
            fputcsv($fp, $fields);
            fputcsv($fp_sync, $fields);
        }

        fclose($fp);
        fclose($fp_sync);

        /*
         * run sh sync product
         */
        $_sh_path = PATH_SHELL_FILE.'product/cli_sync_product.sh';
        exec('sh '.$_sh_path);

        // save log
        $result = [
            'data'    => $product,
            'status'  => true,
            'message' => 'Synchronize Successfully',
        ];
        Helpers::log(__FUNCTION__, $result);
    }

    public function synchronizeImages($id)
    {
        $product       = $this->find($id);
        $list          = [
            [
                'sku',
                'image',
                'small_image',
                'thumbnail',
                'media_gallery',
            ],
        ];
        $image         = null;
        $media_gallery = null;
        if (isset($product->product_images_thumbnail)) {
            $image         = '+/'.$product->product_images_thumbnail->image_url;
            $media_gallery = $product->product_images_thumbnail->image_url;
        }
        $list[] = [
            'sku'           => $product->sku,
            'image'         => $image,
            'small_image'   => $image,
            'thumbnail'     => $image,
            'media_gallery' => $media_gallery,
        ];
        foreach ($product->product_images as $product_images) {
            $img = public_path(
                'product/'
                .$product->product_ID
                .'/'
                .$product->product_images_thumbnail->image_url
            );
            if (file_exists($img) && is_file($img)) {
                copy($img, PATH_EXPORT_IMAGE."/".$product->product_images_thumbnail->image_url);
            }
        }
        $path = env('PATH_EXPORT_CSV');

        // backup sync file
        $name_file = 'product_images_csv_'.date('Ymd-His').".csv";
        $fp        = fopen($path.$name_file, 'w');

        // real sync csv
        $_sync_file = env('PATH_IMPORT_CSV').SYNC_IMAGE_CSV_FILE;
        if (file_exists($_sync_file)) {
            unlink($_sync_file);
        }
        $fp = fopen($_sync_file, 'w');
        foreach ($list as $fields) {
            fputcsv($fp, $fields);
        }
        fclose($fp);

        // run sh sync product
        $_sh_path = env('PATH_SHELL_FILE').'product/cli_sync_image_product.sh';
        exec('sh '.$_sh_path);

        // save log
        $result = [
            'data'    => $product,
            'status'  => true,
            'message' => 'Synchronize Images Successfully',
        ];
        Helpers::log(__FUNCTION__, $result);
    }

    public function synchronizeUpdateProduct($sku)
    {
        $product = $this->findByField('sku', $sku);
        $list    = [
            [
                'sku',
                'categories',
                'price',
                'name',
                'product_type',
                'inventory_product_id',
                'multi_filter_type',
                'status',
                'qty',
                'is_in_stock',
                'manage_stock',
                'color',
                'description',
                'short_description',
                'store',
                'quality_label',
                'model_children',
            ],
        ];
        $_model  = ModelRepo::getInstance();
        for ($i = 0; $i < 3; $i++) {
            $categories = null;
            if (!empty($product->extra_model)) {
                $model_childrens = json_decode($product->extra_model, true);
                foreach ($model_childrens as $key => $child) {
                    $model = $_model->find($child);
                    if ($model->parent) {
                        $categories .= $model->brand->name ?? null;
                        $categories .= ">";
                        $categories .= $model->parent->name ?? null;
                        $categories .= ">";
                        $categories .= $model->name ?? null;
                        $categories .= ";;";
                    }
                }
            } else {
                $categories .= $product->brand->name ?? null;
                $categories .= ">";
                $categories .= $product->model_parent->name ?? null;
                $categories .= ">";
            }
            $categories   = rtrim($categories, ';;');
            $_filter_type = null;
            $qty          = $product->storage_has_product->sum('amount');

            if ($i == 0) {
                $description       = $product->description;
                $short_description = $product->description;
                $store             = 'default';
            } else {
                if ($i == 1) {
                    $description       = $product->description_en;
                    $short_description = $product->description_en;
                    $store             = 'en,es,de,fr,it';
                } else {
                    if ($i == 2) {
                        $description       = $product->description_en;
                        $short_description = $product->description_en;
                        $store             = '';
                    }
                }
            }
            $list[] = [
                'sku'                  => $product->sku,
                'categories'           => $categories,
                'price'                => $product->price,
                'name'                 => $product->name,
                'product_type'         => $product->product_type->product_type_name,
                'inventory_product_id' => $product->product_ID,
                'multi_filter_type'    => $_filter_type,
                'status'               => ACTIVE,
                'qty'                  => $qty,
                'is_in_stock'          => ($qty > 0) ? 1 : 0,
                'manage_stock'         => ACTIVE,
                'color'                => $product->color,
                'description'          => $description,
                'short_description'    => $short_description,
                'store'                => $store,
                'quality_label'        => $product->quality_label,
                'model_children'       => $product->model->name ?? null,
            ];

            $path = PATH_EXPORT_CSV;

            // backup sync file
            $name_file = 'product_csv_'.date('Ymd-His').".csv";
            $fp        = fopen($path.$name_file, 'w');

            // real sync csv
            $_sync_file = PATH_IMPORT_CSV.SYNC_CSV_FILE;

            if (file_exists($_sync_file)) {
                unlink($_sync_file);
            }
            $fp_sync = fopen($_sync_file, 'w');

            foreach ($list as $fields) {
                fputcsv($fp, $fields);
                fputcsv($fp_sync, $fields);
            }

            fclose($fp);
            fclose($fp_sync);

            /*
             * run sh sync product
             */
            $_sh_path = PATH_SHELL_FILE.'product/cli_sync_product.sh';
            exec('sh '.$_sh_path);

            // save log
            $result = [
                'data'    => $product,
                'status'  => true,
                'message' => 'Synchronize Update Product Successfully',
            ];
            Helpers::log(__FUNCTION__, $result);
        }
    }

    public function getPriceBySku($data)
    {
        $result = [];
        if (isset($data['sku'])) {
            $product = Product::select('sku', 'price')->whereIn('sku', $data['sku'])->groupBy('sku')->get();
            $tmp     = array_group_by($product->toArray(), 'sku');
            foreach ($data['sku'] as $sku) {
                $result[$sku] = $tmp[$sku][0]['price'] ?? 0;
            }
        }

        return $result;
    }

    public function getStockAmountBySku($data)
    {
        $result = [];
        if (isset($data['sku'])) {
            foreach ($data['sku'] as $sku) {
                $result[$sku] = StorageHasProduct::where('location_ID', $data['location'])
                                                 ->whereHas(
                                                     'product', function ($q) use ($sku) {
                                                     $q->where('sku', $sku);
                                                 }
                                                 )
                                                 ->sum('amount');
            }
        }

        return $result;
    }

    public function listDataTable($data)
    {
        $item_length = $data['length'] ?? ITEM_LENGHT;
        $query       = Product::with(
            [
                'brand',
                'model',
                'model_parent',
                'product_type',
                'storage_has_product',
            ]
        );

        $query = $query->leftJoin('storage_has_product', 'product_product_ID', 'product_ID')
                       ->selectRaw('product.*,sum(storage_has_product.amount) as quantity,count(*) as total')
                       ->groupBy('product.product_ID');
        if ($data['search']['value']) {
            //            $query = $query->Where('product.sku', 'LIKE', "%" . $data['search']['value'] . "%")
            //                           ->orWhere('product.color', 'LIKE', "%" . $data['search']['value'] . "%")
            //                           ->orWhere('product.price', 'LIKE', "%" . $data['search']['value'] . "%");
            $value = explode(' ', $data['search']['value']);
            foreach ($value as $v) {
                $query = $query->Where('product.name', 'LIKE', "%".$v."%");
            }
        }
        switch ($data['order'][0]['column']) {
            case 0:
                $query = $query->orderBy('product.sku', $data['order'][0]['dir']);
                break;
            case 1:
                $query = $query->orderBy('product.name', $data['order'][0]['dir']);
                break;
            case 2:
                $query = $query->orderBy('product.quality_label', $data['order'][0]['dir']);
                break;
            case 3:
                $query = $query->orderBy('product.price', $data['order'][0]['dir']);
                break;
            case 4:
                $query = $query->orderBy('quantity', $data['order'][0]['dir']);
                break;
        }
        $count       = $query->toBase()->getCountForPagination();
        $query       = $query->offset($data['start'])->limit($item_length);
        $result_data = $query->get();
        foreach ($result_data as $key => $item) {
            $product_type    = [];
            $brand           = [];
            $model           = [];
            $model_parent    = [];
            $storage_product = [];
            try {
                $group_brand_id = json_decode($item->group_brand_id, true);
                $group_brand    = Brand::whereIn('brand_ID', $group_brand_id)->get();
            } catch (\Exception $e) {
                $group_brand = [];
            }

            if (isset($item->product_type)) {
                $product_type = [
                    'product_type_name' => $item->product_type->product_type_name ?? null,
                    'product_type_code' => $item->product_type->product_type_code ?? null,
                    'description_nl'    => $item->product_type->description_nl ?? null,
                    'description_en'    => $item->product_type->description_en ?? null,
                ];
            }
            if (isset($item->brand)) {
                $brand = [
                    'name'      => $item->brand->name ?? null,
                    'code'      => $item->brand->brand_CODE ?? null,
                    'group_id'  => $item->brand->group_id ?? null,
                    'parent_id' => $item->brand->parent_id ?? null,
                ];
            }
            if (isset($item->model_data)) {
                $model = [
                    'name'      => $item->model_data->name ?? null,
                    'brand_id'  => $item->model_data->brand_brand_ID ?? null,
                    'parent_id' => $item->model_data->parent_ID ?? null,
                ];
            }
            if (isset($item->model_parent)) {
                $model_parent = [
                    'name'      => $item->model_parent->name ?? null,
                    'brand_id'  => $item->model_parent->brand_brand_ID ?? null,
                    'parent_id' => $item->model_parent->parent_ID ?? null,
                ];
            }
            if (isset($item->storage_has_product)) {
                $storage_product = [];
                foreach ($item->storage_has_product as $storage_has_product) {
                    $storage_product[] = [
                        'id'          => $storage_has_product->id,
                        'amount'      => $storage_has_product->amount,
                        'rack'        => $storage_has_product->storage->rack,
                        'row'         => $storage_has_product->storage->row,
                        'column'      => $storage_has_product->storage->column,
                        'storage_id'  => $storage_has_product->storage_storage_ID,
                        'location_id' => $storage_has_product->location_ID,
                    ];
                }
            }
            $brand_group    = $this->_brand_group;
            $group_brand_id = json_decode($item->group_brand_id);
            if ($group_brand_id != null) {
                if (count($group_brand_id) > 0) {
                    foreach ($brand_group as $k => $v) {
                        if (in_array($v['id'], $group_brand_id)) {
                            $brand_group[$k]['is_check'] = (boolean)true;
                        }
                    }
                }
            } else {
                $group_brand_id = null;
            }
            $quantity = $item->storage_has_product()->sum('amount');
            if ($quantity == 0) {
                $quantity = 0;
            } elseif ($quantity < 10) {
                $quantity = "<10";
            } elseif ($quantity < 21) {
                $quantity = "10-20";
            } else {
                $quantity = "20+";
            }
            $rows[] = [
                'id'                  => $item->product_ID,
                'sku'                 => $item->sku,
                'link'                => URL_MAIN.$item->sku,
                'factory_code'        => $item->factory_code,
                'name'                => $item->name,
                'name_en'             => $item->name_en,
                'description'         => $item->description,
                'description_en'      => $item->description_en,
                'image'               => $item->image,
                'price'               => $item->price ?? null,
                'price_wholesale'     => $item->price_wholesale,
                'price_purchase'      => $item->price_purchase,
                'sold'                => $item->sold,
                'color'               => $item->color,
                'brand_id'            => $item->brand_brand_ID,
                'product_type_id'     => $item->product_type_ID,
                'product_type'        => $product_type,
                'keyword'             => $item->keyword,
                'model_parent_id'     => $item->model_parent_ID,
                'model_id'            => $item->model_ID,
                'extra_field'         => $item->extra_field,
                'group_brand_id'      => $group_brand_id,
                'group_brand'         => $group_brand,
                'extra_model'         => $item->extra_model,
                'extra_product_code'  => $item->extra_product_code ?? null,
                'quality_label'       => $item->quality_label,
                'brand'               => $brand,
                'model'               => $model,
                'model_parent'        => $model_parent,
                'storage_has_product' => $storage_product,
                'brand_group'         => $brand_group,
                'quantity'            => $quantity,
            ];
        }
        $result['data']            = $rows ?? [];
        $result['draw']            = $data['draw'];
        $result['recordsFiltered'] = $count;
        $result['recordsTotal']    = $count;

        return $result;
    }

    public function listSingle($data)
    {
        $item_length = $data['length'] ?? ITEM_LENGHT;
        $query       = Product::with(
            [
                'brand',
                'model',
                'model_parent',
                'product_type',
                'storage_has_product',
            ]
        );
        $query       = $query->leftJoin('storage_has_product', 'product_product_ID', 'product_ID')
                             ->selectRaw('product.*,sum(storage_has_product.amount) as quantity,count(*) as total')
                             ->groupBy('product.product_ID');
        if (isset($data['name'])) {
            $value = explode(' ', $data['name']);
            foreach ($value as $v) {
                $query = $query->where('name', 'LIKE', "%".$v."%");
            }
        }
        switch ($data['order']) {
            case 'sku':
                $query = $query->orderBy('product.sku', $data['type']);
                break;
            case 'name':
                $query = $query->orderBy('product.name', $data['type']);
                break;
            case 'price':
                $query = $query->orderBy('product.price', $data['type']);
                break;
            case 'quantity':
                $query = $query->orderBy('quantity', $data['type']);
                break;
        }
        $result = $query->paginate($item_length);
        $result = Helpers::formatPagination($result);
        $rows   = [];
        foreach ($result['rows'] as $key => $item) {
            $product_type    = [];
            $brand           = [];
            $model           = [];
            $model_parent    = [];
            $storage_product = [];
            try {
                $group_brand_id = json_decode($item->group_brand_id, true);
                $group_brand    = Brand::whereIn('brand_ID', $group_brand_id)->get();
            } catch (\Exception $e) {
                $group_brand = [];
            }

            if (isset($item->product_type)) {
                $product_type = [
                    'product_type_name' => $item->product_type->product_type_name ?? null,
                    'product_type_code' => $item->product_type->product_type_code ?? null,
                    'description_nl'    => $item->product_type->description_nl ?? null,
                    'description_en'    => $item->product_type->description_en ?? null,
                ];
            }
            if (isset($item->brand)) {
                $brand = [
                    'name'      => $item->brand->name ?? null,
                    'code'      => $item->brand->brand_CODE ?? null,
                    'group_id'  => $item->brand->group_id ?? null,
                    'parent_id' => $item->brand->parent_id ?? null,
                ];
            }
            if (isset($item->model)) {
                $model = [
                    'name'      => $item->model->name ?? null,
                    'brand_id'  => $item->model->brand_brand_ID ?? null,
                    'parent_id' => $item->model->parent_ID ?? null,
                ];
            }
            if (isset($item->model_parent)) {
                $model_parent = [
                    'name'      => $item->model_parent->name ?? null,
                    'brand_id'  => $item->model->brand_brand_ID ?? null,
                    'parent_id' => $item->model->parent_ID ?? null,
                ];
            }
            if (isset($item->storage_has_product)) {
                $storage_product = [];
                foreach ($item->storage_has_product as $storage_has_product) {
                    $storage_product[] = [
                        'id'          => $storage_has_product->id,
                        'amount'      => $storage_has_product->amount,
                        'rack'        => $storage_has_product->storage->rack,
                        'row'         => $storage_has_product->storage->row,
                        'column'      => $storage_has_product->storage->column,
                        'storage_id'  => $storage_has_product->storage_storage_ID,
                        'location_id' => $storage_has_product->location_ID,
                    ];
                }
            }
            $brand_group    = $this->_brand_group;
            $group_brand_id = json_decode($item->group_brand_id);
            if ($group_brand_id != null) {
                if (count($group_brand_id) > 0) {
                    foreach ($brand_group as $k => $v) {
                        if (in_array($v['id'], $group_brand_id)) {
                            $brand_group[$k]['is_check'] = (boolean)true;
                        }
                    }
                }
            } else {
                $group_brand_id = null;
            }
            $quantity       = $item->storage_has_product()->sum('amount');
            $total_quantity = (int)$quantity;
            if ($quantity == 0) {
                $quantity = 0;
            } elseif ($quantity < 10) {
                $quantity = "<10";
            } elseif ($quantity < 21) {
                $quantity = "10-20";
            } else {
                $quantity = "20+";
            }
            $rows[] = [
                'id'                  => $item->product_ID,
                'sku'                 => $item->sku,
                'link'                => URL_MAIN.$item->sku,
                'factory_code'        => $item->factory_code,
                'name'                => $item->name,
                'name_en'             => $item->name_en,
                'description'         => $item->description,
                'description_en'      => $item->description_en,
                'image'               => $item->image,
                'price'               => $item->price,
                'price_wholesale'     => $item->price_wholesale,
                'price_purchase'      => $item->price_purchase,
                'sold'                => $item->sold,
                'color'               => $item->color,
                'brand_id'            => $item->brand_brand_ID,
                'product_type_id'     => $item->product_type_ID,
                'product_type'        => $product_type,
                'keyword'             => $item->keyword,
                'model_parent_id'     => $item->model_parent_ID,
                'model_id'            => $item->model_ID,
                'extra_field'         => $item->extra_field,
                'group_brand_id'      => $group_brand_id,
                'group_brand'         => $group_brand,
                'extra_model'         => $item->extra_model,
                'extra_product_code'  => $item->extra_product_code,
                'quality_label'       => $item->quality_label,
                'brand'               => $brand,
                'model'               => $model,
                'model_parent'        => $model_parent,
                'storage_has_product' => $storage_product,
                'brand_group'         => $brand_group,
                'quantity'            => $quantity,
                'total_quantity'      => $total_quantity,
            ];
        }
        $result['rows'] = $rows;

        return $result;
    }
}