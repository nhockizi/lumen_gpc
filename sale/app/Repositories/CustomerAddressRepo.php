<?php

namespace App\Repositories;

use App\Libraries\Helpers;
use App\Models\Brand;
use App\Models\Customer;
use App\Models\CustomerAddress;
use App\Models\Product;

class CustomerAddressRepo extends BaseRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        // TODO: Implement model() method.
        return CustomerAddress::class;
    }

    public function list()
    {
        $request      = app('request')->all();
        $item_length  = $request['length'] ?? ITEM_LENGHT;
        $fieldsSearch = [
            'customer_id' => [
                'customer_address',
                'customer_id',
                '='
            ],
            //            'name'     => ['brand', 'name', 'LIKE'],
        ];
        $query        = CustomerAddress::with(['customer']);
        $query        = Helpers::searchFieldsMapping($query, $fieldsSearch);
        $result       = $query->paginate($item_length);
        $result       = Helpers::formatPagination($result);
        $rows         = [];
        foreach ($result['rows'] as $key => $item) {
            $tmp               = $item->toArray();
            $tmp['fixCountry'] = Helpers::_fixCountry($tmp['country']);
            $tmp['address']    = $item->street . ', ' . $item->city . ', ' . $item->country;
            $rows[]            = $tmp;
        }
        $result['rows'] = $rows;

        return $result;
    }

    public function _save($data)
    {
        $attributes    = [
            'id' => $data['id'],
        ];
        $values = [
            'customer_id'                 => $data['customer_id'] ?? null,
            'customer_name'               => $data['customer_name'] ?? null,
            'company_name'                => $data['company_name'] ?? null,
            'street'                      => $data['street'] ?? null,
            'country'                     => $data['country'] ?? null,
            'city'                        => $data['city'] ?? null,
            'telephone'                   => $data['telephone'] ?? null,
            'postal_code'                 => $data['postal_code'] ?? null,
            'address_type'                => $data['address_type'] ?? null,
            'billing_default'             => $data['billing_default'] ?? null,
            'shipping_default'            => $data['shipping_default'] ?? null,
            'magento_shipping_address_id' => $data['magento_shipping_address_id'] ?? null,
        ];
        $result = $this->updateOrCreate($attributes, $values);
        return $result;
    }

}