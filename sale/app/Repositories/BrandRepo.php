<?php

namespace App\Repositories;

use App\Libraries\Helpers;
use App\Models\Brand;
use App\Models\Product;

class BrandRepo extends BaseRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        // TODO: Implement model() method.
        return Brand::class;
    }

    public function list($data)
    {
        $item_length  = $data['length'] ?? ITEM_LENGHT;
        $fieldsSearch = [
            'brand_id' => ['brand', 'brand_ID', '='],
            'name'     => ['brand', 'name', 'LIKE'],
        ];
        $query        = Brand::with(
            [
                'child',
                'model' => function ($query) {
                    $query->where('parent_ID', 0);
                },
                'model.child',
            ]
        )
                             ->whereNull('group_id');
        $query        = Helpers::searchFieldsMapping($query, $fieldsSearch);
        $fieldsSort   = [
            'brand_ID'   => [
                'brand',
                'brand_ID',
            ],
            'name'       => [
                'brand',
                'name',
            ],
            'brand_CODE' => [
                'brand',
                'brand_CODE',
            ],
        ];
        $query        = Helpers::sortFieldsMapping($query, $fieldsSort, $data['order'], $data['type']);
        $result       = $query->paginate($item_length);
        $result       = Helpers::formatPagination($result);

        return $result;
    }

    public function listWithModel($product_id)
    {
        $product      = Product::where('product_ID', $product_id)->first();
        $fieldsSearch = [
            'brand_id' => ['brand', 'brand_ID', '='],
            'name'     => ['brand', 'name', 'LIKE'],
        ];
        $query        = Brand::with(
            [
                'child',
                'model_data' => function ($query) {
                    $query->where('parent_ID', 0);
                },
                'model_data.model_data',
            ]
        )
                             ->whereHas(
                                 'model', function ($query) {
                                 $query->where('model_ID', '!=', '0');
                             }
                             )
                             ->whereNull('group_id');
        $query        = Helpers::searchFieldsMapping($query, $fieldsSearch);
        $data         = $query->get();
        $result       = [];
        foreach ($data as $key_result => $item) {
            $tmp_1 = [];
            foreach ($item->model_data as $key_model => $model) {
                $model->id = $model->model_ID;
                $tmp_2     = [];
                foreach ($model->model_data as $key_child => $child) {
                    $tmp_2[] = [
                        'id'      => $child->model_ID,
                        'name'    => $child->name,
                        'tree_id' => 'model_'.$child->model_ID,
                        'level'   => 2,
                    ];
                }
                $tmp_1[] = [
                    'id'       => $model->model_ID,
                    'name'     => $model->name,
                    'children' => $tmp_2,
                    'tree_id'  => 'model_'.$model->model_ID,
                    'level'    => 1,
                ];
            }
            $result[] = [
                'id'       => $item->brand_ID,
                'name'     => $item->name,
                'children' => $tmp_1,
                'isRoot'   => true,
                'tree_id'  => 'brand_'.$item->brand_ID,
                'level'    => 0,
            ];
        }

        return $result;
    }

    public function detail($id)
    {
        $result = Brand::with(
            [
                'child',
            ]
        )
                       ->where('brand_ID', $id)
                       ->first();

        return $result;
    }

    public function _save($data)
    {
        $attributes = [
            'brand_ID' => $data['id'],
        ];
        $values     = [
            'name'       => (string)$data['name'],
            'brand_CODE' => (string)$data['code'],
        ];
        $result     = $this->updateOrCreate($attributes, $values);

        return $result;
    }
}