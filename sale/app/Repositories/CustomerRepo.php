<?php

namespace App\Repositories;

use App\Libraries\Helpers;
use App\Models\Customer;

class CustomerRepo extends BaseRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        // TODO: Implement model() method.
        return Customer::class;
    }

    public function list()
    {
        $request      = app('request')->all();
        $item_length  = $request['length'] ?? ITEM_LENGHT;
        $fieldsSearch = [
            'customer_id'   => [
                'customer',
                'customer_ID',
                '=',
            ],
            'customer_name' => [
                'customer',
                'name',
                'LIKE',
            ],
        ];
        $query        = Customer::select('*');
        $query        = Helpers::searchFieldsMapping($query, $fieldsSearch);
        $result       = $query->paginate($item_length);
        $result       = Helpers::formatPagination($result);

        return $result;
    }

    public function detail($id)
    {
        $result        = $this->with(
            [
                'customer_address',
                'invoice',
                'invoice.customer_address',
                'invoice.customer'
            ]
        )
                              ->find($id);
        foreach ($result->invoice as $invoice){
            $invoice->pdf      = env('APP_URL') .
                              '/pdf/' .
                              $invoice->bon_file;
        }
        $result->country = Helpers::_fixCountry($result->country);
        $result->total = Helpers::formatNumber($result->invoice->sum('price_total'));

        return $result;
    }
}