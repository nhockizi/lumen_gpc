<?php

namespace App\Repositories;

use App\Libraries\Helpers;
use App\Models\Sales;
use App\Models\SalesOrder;

class SaleOrderRepo extends BaseRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        // TODO: Implement model() method.
        return SalesOrder::class;
    }

    public function general()
    {
        $result = [
            'status' => Helpers::_map(SALSE_ORDER_STATUS_DATA),
        ];

        return $result;
    }

    public function list($data)
    {
        $sale        = Helpers::getAuth();
        $item_length = $data['length'] ?? ITEM_LENGHT;
        $query       = SalesOrder::with(
            [
                'sale',
            ]
        )
                                 ->where('sales_id', $sale->id);
        $result      = $query->paginate($item_length);
        $result      = Helpers::formatPagination($result);
        $rows        = [];
        foreach ($result['rows'] as $item) {
            $rows[] = [
                'id'          => $item->id,
                'sale'        => $item->sale,
                'total'       => $item->total,
                'status'      => $item->status,
                'status_name' => SALSE_ORDER_STATUS_DATA[$item->status] ?? null,
            ];
        }
        $result['rows'] = $rows;

        return $result;
    }

    public function _save($data)
    {
        $sale       = Helpers::getAuth();
        $items      = $data['items'];
        $attributes = [
            'id' => $data['id'] ?? null,
        ];
        $values     = [
            'sales_id' => $sale->id,
            'status'   => $data['status'],
        ];
        $result     = $this->updateOrCreate($attributes, $values);

        $_sale_order_item = SaleOrderItemRepo::getInstance();
        $_product         = ProductRepo::getInstance();

        foreach ($items as $item) {
            $attributes = [
                'id' => $item['id'] ?? null,
            ];
            $product    = $_product->find($item['product_id']);
            $values     = [
                'sales_order_id' => (int)$result->id,
                'product_id'     => (int)$item['product_id'],
                'price'          => (double)$product->price,
                'quantity'       => (int)$item['quantity'],
                'total'          => Helpers::mul_value($product->price, $item['quantity']),
            ];
            $_sale_order_item->updateOrCreate($attributes, $values);
            $result->total = Helpers::sum_value($result->total, $values['total']);
        }

        $result->save();

        return $result;
    }

    public function detail($id)
    {
        $sale   = Helpers::getAuth();
        $result = SalesOrder::with(
            [
                'sales_order_item',
            ]
        )
                       ->where('id', $id)
                       ->where('sales_id', $sale->id)
                       ->first();

        return $result;
    }
}