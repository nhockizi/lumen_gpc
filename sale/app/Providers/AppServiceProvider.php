<?php

namespace App\Providers;

use GuzzleHttp\Client;
use GuzzleHttp\RequestOptions;
use Illuminate\Support\ServiceProvider;
use Laravel\Lumen\Http\Redirector;
use Laravel\Lumen\Http\ResponseFactory;
use Laravel\Lumen\Routing\UrlGenerator;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(IOC_RESTFUL, function (){
            return $this->getBindingGuzzle();
        });

        $this->app->bind(IOC_RESTFUL_REQUEST, function ($app, $params){
            return $this->getBindingGuzzleRequest($params[0], $params[1]);
        });
        $this->app->singleton('Illuminate\Contracts\Routing\ResponseFactory', function ($app) {
            return new ResponseFactory(
                $app['Illuminate\Contracts\View\Factory'],
                $app[Redirector::class]
            );
        });
    }

    /**
     * @return Client
     */
    private function getBindingGuzzle(){
        $client = new Client([
            RequestOptions::HTTP_ERRORS => false
        ]);
        return $client;
    }

    /**
     * @param string $method
     * @param string $url
     * @return \GuzzleHttp\Psr7\Request
     */
    private function getBindingGuzzleRequest($method, $url){
        $request = new \GuzzleHttp\Psr7\Request($method, $url);
        return $request;
    }
    public function boot(UrlGenerator $url)
    {
        if(env('REDIRECT_HTTPS'))
        {
            $url->forceSchema('https');
        }
    }
}
