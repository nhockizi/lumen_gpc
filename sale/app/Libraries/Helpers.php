<?php

namespace App\Libraries;

use App\Models\LogActive;
use App\Models\Member;
use App\Models\RolesPermission;
use App\Models\Sales;
use App\Models\User;
use App\Services\BCMath\BCMathFacade;
use App\Services\UserLoggedIn\UserLoggedInFacade;
use Carbon\Carbon;
use GuzzleHttp\Client;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Validation\Validator;
use Intervention\Image\Facades\Image;
use SwaggerLume\Exceptions\SwaggerLumeException;

class Helpers
{
    public static function getClassStatusCode($code)
    {
        $classCode  = (int)($code / 100);
        $classArray = [
            'informational',
            'success',
            'redirection',
            'client_error',
            'server_error',
        ];

        return $classArray[$classCode - 1];
    }

    public static function getTimeZone()
    {
        $request  = app(Request::class);
        $timezone = $request->get('timezone');

        return $timezone;
    }

    public static function formatPagination(LengthAwarePaginator $data)
    {
        $length      = $data->perPage();
        $totalRecord = $data->total();
        $result      = [
            'page'            => $data->currentPage(),
            'length'          => $length,
            'recordsTotal'    => $totalRecord,
            'recordsFiltered' => $totalRecord,
            'total_page'      => ceil($totalRecord / $length),
            'rows'            => $data->items(),
        ];

        return $result;
    }

    public static function getToken()
    {
        $tokenRaw = app('request')->header('Authorization');
        preg_match("/Bearer (.*)/", $tokenRaw, $result);

        return $result[1];
    }

    /**
     * @param $validator
     * @param $string
     *
     * @return array
     */
    public static function getErrorMessage($validator, $string = null)
    {
        $result = $tmp = [];
        $errors = $validator->getMessageBag()->toArray();
        foreach ($errors as $field => $error) {
            $tmp[$field] = implode(',', $error);
        }
        if ($string) {
            $result[$string] = $tmp;
        } else {
            $result = $tmp;
        }

        return $result;
    }

    public static function changeTimeZone($dateString, $timeZoneSource = null, $timeZoneTarget = null)
    {
        if (empty($timeZoneSource)) {
            $timeZoneSource = date_default_timezone_get();
        }
        if (empty($timeZoneTarget)) {
            $timeZoneTarget = date_default_timezone_get();
        }
        $dt = new \DateTime($dateString, new \DateTimeZone($timeZoneSource));
        $dt->setTimezone(new \DateTimeZone($timeZoneTarget));

        return $dt;
    }

    public static function convertDateToDefaultTimeZone(string $dateTime, \DateTimeZone $timeZone)
    :\DateTime {
        return self::convertDateTimeZone($dateTime, $timeZone, new \DateTimeZone(date_default_timezone_get()));
    }

    public static function convertDateTimeZone(string $dateTime, \DateTimeZone $from, \DateTimeZone $to)
    :\DateTime {
        $date = new \DateTime($dateTime, $from);
        $date->setTimezone($to);

        return $date;
    }

    public static function getItemPerPage()
    {
        $request     = self::getRequestInstance();
        $itemPerPage = $request->get(
            config('repository.pagination.params.limit'),
            config('repository.pagination.limit')
        );

        return $itemPerPage;
    }

    /**
     * @return Request
     */
    public static function getRequestInstance()
    {
        return app('request');
    }

    public static function cropImg($imgPath, $width, $height = null, $prefix = 'resize')
    {
        $info   = pathinfo($imgPath);
        $newImg = dirname($imgPath).'/'.$info['filename'].'_'.$prefix.'.'.$info['extension'];
        $img    = Image::make($imgPath);
        $img->resize(
            $width,
            $height,
            function ($constraint) {
                $constraint->aspectRatio();
            }
        )
            ->save($newImg, 60);

        return $newImg;
    }

    /**
     * map search field
     *
     * @param Builder $query
     * @param array   $fieldsSearch , format [KEY => [TABLE_NAME,FIELD,CONDITION],...],
     *                              with KEY : key using for search request,
     *                              with TABLE_NAME : table name,
     *                              with FIELD : field name,
     *                              with CONDITION : search condition, acept 2 values "=" and "LIKE", default "=",
     *
     * @return Builder
     */
    public static function searchFieldsMapping($query, array $fieldsSearch, $request = [])
    {
        $request = $request ? $request : app('request')->all();
        if (!empty($fieldsSearch)) {
            foreach ($fieldsSearch as $searchKey => $fieldSearch) {
                if (array_get($request, $searchKey, null) != null) {
                    $searchValue = array_get($request, $searchKey);
                    $condition   = null;
                    $alias       = null;
                    $field       = null;
                    $countSearch = count($fieldSearch);
                    if (
                    !in_array(
                        $countSearch, [
                        2,
                        3,
                    ]
                    )
                    ) {
                        continue;
                    }
                    if ($countSearch == 3) {
                        $condition = $fieldSearch[2];
                        $field     = $fieldSearch[1];
                        $alias     = $fieldSearch[0];
                    } else {
                        $condition = '=';
                        $field     = $fieldSearch[1];
                        $alias     = $fieldSearch[0];
                    }
                    $condition = strtoupper($condition);
                    if (
                    !in_array(
                        strtoupper($condition),
                        [
                            '=',
                            'LIKE',
                            'IN',
                            '<',
                            '>',
                            '<=',
                            '=>',
                            '>=',
                            '=<',
                            '!=',
                            '<>',
                        ]
                    )
                    ) {
                        continue;
                    }
                    if ($condition == 'IN') {
                        $searchValue = explode(',', $searchValue);
                        $query->whereIn($alias.'.'.$field, $searchValue);
                    } else {
                        if ($condition == 'LIKE') {
                            $searchValue = '%'.trim($searchValue).'%';
                        }
                        $query->where($alias.'.'.$field, $condition, $searchValue);
                    }
                }
            }
        }

        return $query;
    }

    /**
     * map filter field
     *
     * @param Builder $query
     * @param array   $fieldsSearch , format [KEY => [TABLE_NAME,FIELD],...],
     *                              with KEY : key using for search request,
     *                              with TABLE_NAME : table name
     *                              with FIELD : field name
     *
     * @return Builder
     */
    public static function sortFieldsMapping($query, array $fieldsSort, $order = null, $orderType = null)
    {
        $request = app('request');
        if ($order == null) {
            $order = $request->get('order', null);
        }
        if (empty($order)) {
            return $query;
        }
        if ($orderType == null) {
            $orderType = $request->get('sort', 'DESC');
        }
        $orderType = strtoupper($orderType);
        if (
        !in_array(
            $orderType, [
            'ASC',
            'DESC',
        ]
        )
        ) {
            $orderType = 'ASC';
        }
        $orderField = $order;
        if (!empty($fieldsSort)) {
            foreach ($fieldsSort as $sortKey => $fieldSort) {
                if ($sortKey == $orderField) {
                    $alias = null;
                    $field = null;
                    $field = $fieldSort[1];
                    $alias = $fieldSort[0];
                    if (!empty($alias)) {
                        $query->orderBy($alias.'.'.$field, $orderType);
                    } else {
                        $query->orderBy($field, $orderType);
                    }
                    break;
                }
            }
        }

        return $query;
    }

    /**
     * @param $query
     * @param $search
     * @param $fieldSearch
     *
     * @return mixed
     */
    static function addConditionToQuery($query, $search, $fieldSearch)
    {
        if (!$search) {
            return $query;
        }
        foreach ($search as $key => $val) {
            if (!isset($fieldSearch[$key])) {
                continue;
            }
            $field = $fieldSearch[$key]['field'];
            switch ($fieldSearch[$key]['type']) {
                case 'string':
                    $compare = $fieldSearch[$key]['compare'] ?? 'like';
                    if (strtolower(trim($compare)) == 'like') {
                        $val = '%'.$val.'%';
                    }
                    $query->where(DB::raw($fieldSearch[$key]['field']), $compare, $val);
                    break;
                case 'date':
                    $val        = str_replace('"', '', $val);
                    $dateFormat = date('Y-m-d', strtotime($val));
                    $query->where(
                        DB::raw('DATE('.$field.')'),
                        $fieldSearch[$key]['compare'],
                        $dateFormat
                    );
                    break;
                case 'array':
                    $delimiter = $fieldSearch[$key]['delimiter'] ?? ",";
                    $val       = explode($delimiter, $val);
                    $query->whereIn($field, $val);
                    break;
                default:
                    $query->where($fieldSearch[$key]['field'], $fieldSearch[$key]['compare'], $val);
                    break;
            }
        }

        return $query;
    }

    /**
     * @param Validator $validator
     *
     * @return null|string
     */
    public static function formatErrorsMessage($validator)
    {
        $result = [];
        $errors = $validator->getMessageBag()->toArray();
        foreach ($errors as $key => $error) {
            $result = array_merge($result, $error);
        }
        $result = array_values(array_unique($result));
        if (!$result) {
            return null;
        }

        return implode(',<br>', $result);
    }

    /**
     * Get real IP from client
     *
     * @return string
     */
    public static function getRealClientIP()
    {
        $headers = [
            'HTTP_X_FORWARDED_FOR',
            'HTTP_X_FORWARDED',
            'HTTP_FORWARDED_FOR',
            'HTTP_FORWARDED',
            'HTTP_VIA',
            'HTTP_X_COMING_FROM',
            'HTTP_COMING_FROM',
            'HTTP_CLIENT_IP',
        ];
        foreach ($headers as $header) {
            if (isset ($_SERVER [$header])) {
                //Check server
                if (($pos = strpos($_SERVER [$header], ',')) != false) {
                    $ip = substr($_SERVER [$header], 0, $pos);//True
                } else {
                    $ip = $_SERVER [$header]; //False
                }
                $ipnumber = ip2long($ip);
                if ($ipnumber !== -1 && $ipnumber !== false && (long2ip($ipnumber) === $ip)) {
                    if (
                        ($ipnumber - 184549375)
                        && // Not in 10.0.0.0/8
                        ($ipnumber - 1407188993)
                        && // Not in 172.16.0.0/12
                        ($ipnumber - 1062666241)
                    ) // Not in 192.168.0.0/16
                    {
                        if (($pos = strpos($_SERVER [$header], ',')) != false) {
                            $ip = substr($_SERVER [$header], 0, $pos);
                        } else {
                            $ip = $_SERVER [$header];
                        }
                    }

                    return $ip;
                }
            }
        }

        return $_SERVER ['REMOTE_ADDR'];
    }

    public static function runningInConsole()
    :bool
    {
        return App::runningInConsole();
    }

    public static function logFile($data, $fileName = 'log_test', $option = [])
    {
        $timeNow    = self::getDateTimeNow(null, env('TIME_ZONE_TEST', 'Asia/Ho_Chi_Minh'));
        $timeDetail = (count($timeNow) > 0) ? '['.
                                              $timeNow['mday'].'/'.
                                              $timeNow['mon'].'/'.
                                              $timeNow['year'].' '.
                                              $timeNow['hours'].':'.
                                              $timeNow['minutes'].':'.
                                              $timeNow['seconds'].']' : date("Y-m-d H:i:s");
        $logName    = $fileName.'_'.date("Y_m_d").'.txt';
        $data       = is_string($data) ? $data : json_encode($data);
        $exception  = self::get($option, 'exception');
        if (!is_null($exception)) {
            file_put_contents(
                storage_path('logs/'.$logName),
                $timeDetail.' : '.$exception->getFile()."\n",
                FILE_APPEND
            );
            file_put_contents(
                storage_path('logs/'.$logName),
                $timeDetail.' : '.$exception->getLine()."\n",
                FILE_APPEND
            );
        }
        file_put_contents(storage_path('logs/'.$logName), $timeDetail.' : '.$data."\n", FILE_APPEND);
    }

    public static function getDateTimeNow($time = null, $timeZone = null)
    {
        if (!is_null($timeZone)) {
            date_default_timezone_set($timeZone);
        }

        return is_null($time) ? getdate() : getdate($time);
    }

    public static function trimArray($input)
    {
        return array_map('trim', $input);
    }

    public static function isAbsolutePath($file)
    {
        return strspn($file, '/\\', 0, 1)
               || (strlen($file) > 3 && ctype_alpha($file[0])
                   && substr($file, 1, 1) === ':'
                   && strspn($file, '/\\', 2, 1)
               )
               || null !== parse_url($file, PHP_URL_SCHEME);
    }

    public static function makePath($path)
    {
        $dir = pathinfo($path, PATHINFO_DIRNAME);
        if (is_dir($dir)) {
            echo 1;
            die;

            return true;
        } else {
            if (self::makePath($dir)) {
                if (mkdir($dir)) {
                    chmod($dir, 0777);

                    return true;
                }
            }
        }

        return false;
    }

    public static function replaceExtensionPath($filename, $new_extension)
    {
        $info = pathinfo($filename);

        return ($info['dirname'] ? $info['dirname'].DIRECTORY_SEPARATOR : '')
               .$info['filename']
               .'.'
               .$new_extension;
    }

    /**
     * @return Client
     */
    public static function createRestful()
    {
        return app(IOC_RESTFUL);
    }

    public static function getRandomString($length = 8, $type = 'all')
    {
        if ($type == 'all') {
            $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        } elseif ($type == 'alphabet') {
            $characters = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        } elseif ($type == 'uppercase') {
            $characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        } elseif ($type == 'lowercase') {
            $characters = 'abcdefghijklmnopqrstuvwxyz';
        } elseif ($type == 'number') {
            $characters = '0123456789';
        }

        $string = '';

        for ($i = 0; $i < $length; $i++) {
            $string .= $characters[mt_rand(0, strlen($characters) - 1)];
        }

        return $string;
    }

    public static function log($function, $content = [], $exception = null)
    {
        if ($exception) {
            $content['exception'] = $exception->getMessage().' in '.$exception->getFile().' at '.$exception->getLine();
        }
        $content = "[Sync ".$function."] ".json_encode($content);
        if (empty($content['status'])) {
            Log::info($content);
        } else {
            Log::error($content);
        }
    }

    public static function _map($arr)
    {
        return array_map(
            function ($value, $key) {
                return [
                    'key'   => $key,
                    'value' => $value,
                ];
            }, $arr, array_keys($arr)
        );
    }

    public static function _fixCountry($_country)
    {
        if (strlen($_country) > 2) {
            if ($_country == "België" || $_country == "Belgium") {
                $_country = "BE";
            } elseif (
                $_country == "Netherlands" || $_country == "Netherland" || $_country == "Nederland"
                || $_country
                   == "Nedre"
                || $_country == "Nederlan"
            ) {
                $_country = "NL";
            } elseif ($_country == "Netherlands" || $_country == "Netherland" || $_country == "Nederland") {
                $_country = "NL";
            } elseif ($_country == "Denmark") {
                $_country = "DK";
            } elseif ($_country == "Germany") {
                $_country = "DE";
            } elseif ($_country == "France") {
                $_country = "FR";
            } else {
                $_country = "NL";
            }
        }

        return $_country;
    }

    public static function formatDate($value, $format = null)
    {
        return self::_formatDateTime($value, $format, FORMAT_DATE);
    }

    public static function _formatDateTime($value, $format, $defaultFormat)
    {
        if (empty($value) || $value == '0000-00-00') {
            return null;
        }
        if (empty($format)) {
            $format = $defaultFormat;
        }
        try {
            $date = Carbon::parse($value);
            if (strlen($value) == 10) {
                return $date->format($format);
            } else {
                if (strlen($value) == 19) {
                    return $date->format($format);
                } else {
                    return null;
                }
            }
        } catch (\Exception $e) {
            return null;
        }
    }

    public static function formatDateTime($value, $format = null)
    {
        return self::_formatDateTime($value, $format, FORMAT_DATE_TIME);
    }

    public static function sub_value($left_operand, $right_operand, $scale = BC_DEFAULT_SCALE)
    {
        $result = BCMathFacade::sub($left_operand, $right_operand);

        return self::formatNumber($result);
    }

    public static function formatNumber($number, $scale = BC_DEFAULT_SCALE)
    {
        return round($number, $scale, PHP_ROUND_HALF_UP);
    }

    public static function sum_value($left_operand, $right_operand, $scale = BC_DEFAULT_SCALE)
    {
        $result = BCMathFacade::add($left_operand, $right_operand);

        return self::formatNumber($result);
    }

    public static function percent_value($operand, $percent, $scale = BC_DEFAULT_SCALE)
    {
        $result = BCMathFacade::div(BCMathFacade::mul($operand, $percent), 100);

        return self::formatNumber($result);
    }

    public static function mul_value($left_operand, $right_operand, $scale = BC_DEFAULT_SCALE)
    {
        $result = BCMathFacade::mul($left_operand, $right_operand);

        return self::formatNumber($result);
    }

    public static function listEU()
    {
        $eu_countrycodes = [
            'AL',
            'AD',
            'AT',
            'BY',
            'BE',
            'BA',
            'BG',
            'HR',
            'CY',
            'CZ',
            'DK',
            'EE',
            'FO',
            'FI',
            'FR',
            'DE',
            'GI',
            'GR',
            'HU',
            'IS',
            'IE',
            'IM',
            'IT',
            'RS',
            'LV',
            'LI',
            'LT',
            'LU',
            'MK',
            'MT',
            'MD',
            'MC',
            'ME',
            'NL',
            'PL',
            'PT',
            'RO',
            'RU',
            'SM',
            'RS',
            'SK',
            'SI',
            'ES',
            'SE',
            'UA',
            'GB',
            'VA',
            'RS',
        ];

        return $eu_countrycodes;
    }

    public static function saveLog($action, $msg, $user_id, $update = false)
    {
        $ip = isset($_SERVER['HTTP_X_REAL_IP']) ? $_SERVER['HTTP_X_REAL_IP'] : $_SERVER['REMOTE_ADDR'];
        if ($update == true) {
            Member::where('member_ID', $user_id)
                  ->update(
                      [
                          'lastip'     => $ip,
                          'lastactive' => date('Y-m-d H:i:s'),
                      ]
                  );
        }
        $model               = new LogActive();
        $model->user_id      = $user_id;
        $model->action       = $action;
        $model->message      = $msg;
        $model->ip           = $ip;
        $model->created_date = date('Y-m-d H:i:s');
        $model->location     = self::getLocation($model->ip);
        $model->save();
    }

    public static function getLocation($ip)
    {
        if ($ip) {
            $query = @unserialize(file_get_contents(IP_API_URL.$ip));
            if ($query && $query['status'] == 'success') {
                return $query['city'];
            }
        }

        return null;
    }

    public static function canSeePrice()
    {
        $auth = self::getAuth();
        if (!$auth) {
            throw new \Exception(trans('permission_denied'));
        }
        $result = RolesPermission::whereHas(
            'roles_controller', function ($q) {
            $q->where('name', 'BonController');
        }
        )
                                 ->whereHas(
                                     'roles_function', function ($q) {
                                     $q->where('name', 'DocumentWithPrice');
                                 }
                                 )
                                 ->exists();

        return $result;
    }

    public static function getAuth()
    {
        $key = self::getApiKey();
        $sale = Sales::where('api_key', $key)->first();
        return $sale;
    }

    function swagger_lume_asset($asset)
    {
        $file = swagger_ui_dist_path($asset);

        if (!file_exists($file)) {
            throw new SwaggerLumeException(sprintf('Requested L5 Swagger asset file (%s) does not exists', $asset));
        }

        return route(
            'swagger-lume.asset',
            [
                'asset' => $asset,
                'v'     => md5($file),
            ],
            env('REDIRECT_HTTPS')
        );
    }
    public static function getApiKey(){
        $request  = app(Request::class);
        $api_key = $request->header()['x-api-key'][0] ?? null;
        return $api_key;
    }
}
