<?php

namespace App\Http\Middleware;

use App\Models\Member;
use App\Models\Sales;
use App\Services\Response\ResponseFacade;
use App\User;
use Closure;
use Exception;
use Firebase\JWT\ExpiredException;
use Firebase\JWT\JWT;

class SaleAuthenticate
{
    public function handle($request, Closure $next, $guard = null)
    {
        $api_key = $request->header()['x-api-key'][0] ?? null;
        if ($api_key === null) {
            $msg = 'Your request was made with invalid api key.';

            return ResponseFacade::send($msg, HTTP_UNAUTHORIZED);
        }
        $sale = Sales::where('api_key', $api_key)->first();
        if (!$sale) {
            $msg = 'Your request was made with invalid api key.';

            return ResponseFacade::send($msg, HTTP_UNAUTHORIZED);
        }
        return $next($request);
    }
}