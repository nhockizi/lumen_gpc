<?php
namespace App\Http\Middleware;

use App\Libraries\Helpers;
use App\Services\Response\ResponseFacade;
use Closure;

class PermissionMiddleware
{

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @param $permission
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        list($controllerName, $actionName) = explode('@', $request->route()[1]['uses']);
        $controllerName = str_replace('App\Http\Controllers\\',"",$controllerName);
        $auth = Helpers::getAuth();
        $check = false;
        $auth->roles->roles_permission->map(function($data) use ($controllerName,$actionName,&$check){
            if(strtolower($controllerName) == strtolower($data->roles_controller->name)){
                if(strtolower($actionName) == strtolower($data->roles_function->name)){
                    $check = true;
                }
            }
        });
        if($check === false){
            return ResponseFacade::send(trans('messages.permission_denied'), HTTP_FORBIDDEN);
        }

        return $next($request);
    }
}