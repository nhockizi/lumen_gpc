<?php

namespace App\Http\Controllers;

use App\Http\Controllers\TraitController\ErrorTrait;
use App\Libraries\Helpers;
use App\Repositories\SaleRepo;
use Illuminate\Http\Request;
use Validator;

class AuthController extends Controller
{
    protected $_sale;
    protected $_request;
    use ErrorTrait;

    public function __construct(
        SaleRepo $saleRepo,
        Request $request
    ) {
        $this->_sale    = $saleRepo;
        $this->_request = $request;
    }

    public function resetKey()
    {
        $sale          = Helpers::getAuth();
        $sale->api_key = $this->_sale->generalApiKey();
        $sale->save();

        return $sale;
    }
}