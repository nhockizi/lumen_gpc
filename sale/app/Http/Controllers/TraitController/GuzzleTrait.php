<?php
    namespace App\Http\Controllers\TraitController;
    use App\Libraries\Helpers;
    use GuzzleHttp\RequestOptions;
    use Illuminate\Http\Request;
    use Symfony\Component\HttpFoundation\ParameterBag;
    use Validator;
    use \Illuminate\Support\Facades\Route;

    trait GuzzleTrait
    {
        private function _guzzle_connect($url, $method = 'post', $data = [])
        {
            $res    = Helpers::createRestful()
                             ->$method(
                                 $url,
                                 [
                                     RequestOptions::HEADERS => [
                                         'Authorization' => 'Bearer '.Helpers::getToken()
                                     ],
                                     RequestOptions::JSON    => $data
                                 ]
                             );
            $result = [
                'statusCode' => $res->getStatusCode(),
                'content'    => json_decode($res->getBody()->getContents())
            ];
            return $result;
        }
        private function _internal_request($url, $method, $data = [])
        {
            $request = Request::create($url, $method);
            $request->headers->add(
                [
                    'Authorization' => 'Bearer '.Helpers::getToken(),
                    'Content-type'  => 'application/json',
                ]
            );
            $request->setJson(new ParameterBag($data));
            $response = app()->handle($request);
            $result = [
                'statusCode' => $response->getStatusCode(),
                'content'    => json_decode($response->getContent())
            ];
            return $result;
        }
    }