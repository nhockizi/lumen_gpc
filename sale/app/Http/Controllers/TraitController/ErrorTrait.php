<?php

namespace App\Http\Controllers\TraitController;

use Validator;

trait ErrorTrait
{
    private $_message_error_trait
        = [
            'required'       => 'The :attribute field is required.',
            'integer'        => 'The :attribute field is integer.',
            'numeric'        => 'The :attribute field is numeric.',
            'array'          => 'The :attribute field is array.',
            'exists'         => 'The :attribute field is not exists.',
            'unique'         => 'The :attribute field is unique.',
            'same'           => 'The :attribute and :other must match.',
            'size'           => 'The :attribute must be exactly :size.',
            'between'        => 'The :attribute value :input is not between :min - :max.',
            'in'             => 'The :attribute must be one of the following types: :values',
            'date_format'    => 'The :attribute field is wrong format :format',
            'after'          => 'The :attribute must be less than :date',
            'after_or_equal' => 'The :attribute field must be less than or equal values :date field',
            'email'          => 'The :attribute field must be format email',
        ];

    private function _validate_error($data, $rules, &$messages, $message_error = [])
    {
        $message_error_trait = array_merge($this->_message_error_trait, $message_error);
        $validator           = Validator::make($data, $rules, $message_error_trait);
        if ($validator->fails()) {
            if ($messages != '') {
                $messages .= '<br>';
            }
            $messages .= $this->_show_error($validator->errors());
        }
    }

    private function _show_error($errors)
    {
        $errors      = $errors->toArray();
        $error       = '';
        $check_error = 0;
        foreach ($errors as $err) {
            foreach ($err as $key => $value) {
                if ($check_error > 0) {
                    $error .= '<br>';
                }
                $error .= $value;
                $check_error++;
            }
        }

        return $error;
    }
}