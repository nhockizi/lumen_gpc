<?php

namespace App\Http\Controllers;

use App\Http\Controllers\TraitController\ErrorTrait;
use App\Repositories\SaleOrderRepo;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Validator;

class SaleOrderController extends Controller
{
    protected $_sale_order;
    protected $_request;
    use ErrorTrait;

    public function __construct(
        SaleOrderRepo $saleOrderRepo,
        Request $request
    ) {
        $this->_sale_order = $saleOrderRepo;
        $this->_request    = $request;
    }

    public function list(Request $request)
    {
        try {
            $request_all = $request->all();
            $data        = [
                'length' => array_get($request_all, 'length', null),
                //                'filter_search' => array_get($request_all, 'filter_search', null),
                //                'order'         => array_get($request_all, 'order', null),
                //                'type'          => array_get($request_all, 'type', null),
            ];
            $result      = $this->_sale_order->list($data);

            return $this->response($result);
        } catch (\Exception $e) {

            return $this->response($e->getMessage(), Response::HTTP_BAD_REQUEST);
        }
    }

    public function create()
    {
        $data = $this->_data();
        $this->_validate($data, $message);
        if ($message != '') {
            return $this->response(
                $message,
                Response::HTTP_INTERNAL_SERVER_ERROR
            );
        }
        DB::beginTransaction();
        try {
            $result = $this->_sale_order->_save($data);
            DB::commit();
            $message = trans('messages.create_success');

            return $this->response($result, Response::HTTP_OK, $message);
        } catch (\Exception $e) {
            DB::rollBack();

            return $this->response($e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    private function _data($id = null)
    {
        $request = $this->_request->all();
        $result  = [
            'id'      => $id,
            'sale_id' => array_get($request, 'sale_id', null),
            'status'  => array_get($request, 'status', SALSE_ORDER_STATUS_NEW),
            'items'   => array_get($request, 'items', null),
        ];

        return $result;
    }

    private function _validate($data, &$message)
    {
        $rules = [
            'status' => [
                'required',
                'in:'.SALSE_ORDER_STATUS_NEW.','.SALSE_ORDER_STATUS_APPROVAL.','.SALSE_ORDER_STATUS_CANCEL,
            ],
            'items'  => [
                'required',
                'array',
            ],
        ];
        $this->_validate_error($data, $rules, $message);
        if (is_array($data['items'])) {
            foreach ($data['items'] as $items) {
                $rules = [
                    'product_id' => [
                        'required',
                        'exists:product,product_ID',
                    ],
                    'quantity'   => [
                        'required',
                        'integer',
                    ],
                ];
                if (isset($items['id'])) {
                    $rules['id'] = [
                        'required',
                        'exists:sales_order_item,id',
                    ];
                }
                $this->_validate_error($items, $rules, $message);
            }
        }

        return $message;
    }

    public function get($id)
    {
        try {
            $result = $this->_sale_order->detail($id);
            if (!$result) {
                throw new \Exception(trans('messages.data_not_found'));
            }

            return $this->response($result);
        } catch (\Exception $e) {

            return $this->response($e->getMessage(), Response::HTTP_BAD_REQUEST);
        }
    }

    public function update($id)
    {
        $result = $this->_sale_order->detail($id);
        if (!$result) {
            throw new \Exception(trans('messages.data_not_found'));
        }
        $data = $this->_data($id);
        $this->_validate($data, $message);
        if ($message != '') {
            return $this->response(
                $message,
                Response::HTTP_INTERNAL_SERVER_ERROR
            );
        }
        DB::beginTransaction();
        try {
            $result = $this->_sale_order->_save($data);
            DB::commit();
            $message = trans('messages.update_success');

            return $this->response($result, Response::HTTP_OK, $message);
        } catch (\Exception $e) {
            DB::rollBack();

            return $this->response($e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function delete($id)
    {
        DB::beginTransaction();
        try {
            $result = $this->_sale_order->detail($id);
            if (!$result) {
                throw new \Exception(trans('messages.data_not_found'));
            }
            $result->delete();
            DB::commit();
            $message = trans('messages.delete_success');

            return $this->response($message);
        } catch (\Exception $e) {
            DB::rollBack();

            return $this->response($e->getMessage(), Response::HTTP_BAD_REQUEST);
        }
    }
}