<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 7/10/2018
 * Time: 10:51 AM
 */

namespace App\Observers\Company;


use App\Jobs\AuthorizeCreatePaymentProfileJob;
use App\Models\Company\CompanyCreditCard;
use App\Services\AuthorizeNet\Src\AuthorizeNetHelper;

class CompanyCreditCardObserver
{
    public function created(CompanyCreditCard $creditCard)
    {
        dispatch(new AuthorizeCreatePaymentProfileJob($creditCard->id));
    }

    public function updating(CompanyCreditCard $creditCard)
    {
        if($creditCard->isDirty([
            'company_id',
            'verify',
            'no',
            'expiration_year',
            'expiration_month',
        ])){
            dispatch(new AuthorizeCreatePaymentProfileJob($creditCard->id));
        }
    }
}