<?php

namespace App\Jobs;

use Illuminate\Support\Facades\Mail;

class SentMailJob extends Job
{
    protected $template, $data;

    public function __construct($data, $template = null)
    {
        $this->data     = $data;
        $this->template = $template;
    }

    /**
     * Execute the job.
     *
     * @return boolean
     */
    public function handle()
    {
        if($this->template != null) {
            $data = $this->data;
            Mail::send($this->template, $data, function ($m) use ($data) {
                $m->to($data['to'])
                  ->subject($data['subject'])
                ;
                if ($data['file']) {
                    $m->attach($data['file']);
                }
                if ($data['cc_email']) {
                    $cc_email = explode(';', $data['cc_email']);
                    foreach ($cc_email as $cc) {
                        $m->cc($cc);
                    }
                }
            });
        }
    }
}
