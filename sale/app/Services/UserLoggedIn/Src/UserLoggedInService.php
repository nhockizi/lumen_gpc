<?php

namespace App\Services\UserLoggedIn\Src;

use App\Libraries\Helpers;
use App\Models\User;
use App\Repositories\UserRepo;
use GuzzleHttp\RequestOptions;
use Tymon\JWTAuth\Facades\JWTAuth;
use Tymon\JWTAuth\JWT;
use Tymon\JWTAuth\Token;

class UserLoggedInService
{
    protected $_user;
    protected $_jwt;
    private static $_isLatest = false;
    private $_userRepository;
    private $_userId;

    public function __construct()
    {
    }
    /**
     * @return null|User
     */
    public function getUser()
    {
        return JWTAuth::user();
    }

    /**
     * @return string
     */
    public function getToken() {
        $tokenRaw =  app('request')->header('Authorization');
        preg_match("/Bearer (.*)/", $tokenRaw, $result);
        return $result[1];
    }

}