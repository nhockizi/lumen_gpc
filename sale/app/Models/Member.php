<?php

namespace App\Models;

use App\Models\BaseModel;
use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Support\Facades\Hash;
use Tymon\JWTAuth\Contracts\JWTSubject;

class Member extends BaseModel implements AuthenticatableContract, AuthorizableContract, JWTSubject
{
    use Authenticatable;
    public    $timestamps = false;
    /**
     * Determine if the entity has a given ability.
     *
     * @param  string $ability
     * @param  array|mixed $arguments
     * @return bool
     */
    public function can($ability, $arguments = [])
    {
        // TODO: Implement can() method.
    }
    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'password_alternative',
    ];
    protected $primaryKey = 'member_ID';
    protected $fillable = [
        'member_ID',
        'email',
        'password',
        'username',
        'role',
        'using_api',
        'active'
    ];

    /**
     * The table name
     *
     * @var string
     */
    protected $table = 'member';

    public function getAuthPassword() {
        return $this->password;
    }

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

    function setPasswordAttribute($raw){
        $this->attributes['password'] = Hash::make($raw);
    }

    function setPasswordAlternativeAttribute($raw){
        if($raw != null) {
            $this->attributes['password_alternative'] = Hash::make($raw);
        }
    }

    public function roles(){
        return $this->hasOne(Roles::class, 'id', 'role');
    }

}
