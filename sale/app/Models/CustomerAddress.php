<?php

namespace App\Models;

class CustomerAddress extends BaseModel
{
    protected $table      = 'customer_address';
    protected $primaryKey = 'id';
    public    $timestamps = false;

    protected $fillable   = [
        'customer_id',
        'customer_name',
        'company_name',
        'street',
        'country',
        'city',
        'telephone',
        'postal_code',
        'address_type',
        'billing_default',
        'shipping_default',
        'magento_shipping_address_id',
    ];

    public function customer()
    {
        return $this->hasOne(Customer::class, 'customer_ID', 'customer_id');
    }
}
