<?php

namespace App\Models;

class Brand extends BaseModel
{
    public    $timestamps = false;
    protected $table    = 'brand';
    protected $primaryKey = 'brand_ID';
    protected $fillable = [
        'name',
        'brand_CODE',
        'group_id',
        'parent_id'
    ];

    public function group()
    {
        return $this->hasOne(Brand::class, 'brand_ID', 'group_id');
    }

    public function child()
    {
        return $this->hasOne(Brand::class, 'group_id', 'brand_ID');
    }

    public function model_data()
    {
        return $this->hasMany(Model::class, 'brand_brand_ID', 'brand_ID');
    }

    public function model()
    {
        return $this->hasMany(Model::class, 'brand_brand_ID', 'brand_ID');
    }
}
