<?php

namespace App\Models;

class RolesController extends BaseModel
{
    protected $table = 'roles_controller';
    protected $primaryKey = 'id';
    public $timestamps = false;

    protected $fillable = [
        'name',
    ];

    public function roles_controller_function(){
        return $this->hasMany(RolesControllerFunction::class, 'roles_controller_id', 'id');
    }

    public function roles_permission(){
        return $this->hasMany(RolesPermission::class, 'roles_controller_id', 'id');
    }
}