<?php

namespace App\Models;

use App\Models\BaseModel;

class ProductNotification extends BaseModel
{
    protected $table = 'product_notification';
    protected $primaryKey = 'id';
    public $timestamps = false;

    protected $fillable = [
        'user_id',
        'product_id',
        'qty'
    ];

}
