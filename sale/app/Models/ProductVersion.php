<?php

namespace App\Models;

class ProductVersion extends BaseModel
{
    public    $timestamps = false;
    protected $table      = 'product_version';
    protected $fillable   = [
        'product_ID',
        'history_product_id',
    ];

    public function product()
    {
        return $this->hasOne(Product::class, 'product_ID', 'product_ID');
    }

    public function history_product()
    {
        return $this->hasOne(HistoryProduct::class, 'id', 'history_product_id');
    }

    public function history_product_compare()
    {
        return $this->hasMany(HistoryProductCompare::class, 'history_product_id', 'history_product_id');
    }
}
