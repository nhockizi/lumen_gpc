<?php

namespace App\Models;

class RolesFunction extends BaseModel
{
    protected $table = 'roles_function';
    protected $primaryKey = 'id';
    public $timestamps = false;

    protected $fillable = [
        'name',
    ];

    public function roles_controller_function(){
        return $this->hasMany(RolesControllerFunction::class,'roles_function_id', 'id');
    }

    public function roles_permission(){
        return $this->hasMany(RolesPermission::class,'roles_function_id', 'id');
    }
}