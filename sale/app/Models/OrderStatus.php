<?php

namespace App\Models;

class OrderStatus extends BaseModel
{
    public    $timestamps = false;
    protected $table      = 'order_status';
    protected $primaryKey = 'id';
    protected $fillable   = [
        'order_id',
        'status'
    ];

}
