<?php

namespace App\Models;

class Customer extends BaseModel
{
    public    $timestamps = false;
    protected $table      = 'customer';
    protected $primaryKey = 'customer_ID';
    protected $fillable   = [
            'name',
            'VAT',
            'contact_name',
            'country',
            'city',
            'postalcode',
            'street',
            'email',
            'phone',
            'created_date',
            'province',
            'customer_name',
            'kzsystem_id',
            'type_tax',
            'billing_email',
        ];

    public function countries()
    {
        return $this->hasOne(Country::class, 'country_code', 'country');
    }

    public function invoice()
    {
        return $this->hasMany(Bon::class, 'customer_customer_ID', 'customer_ID');
    }

    public function customer_address()
    {
        return $this->hasMany(CustomerAddress::class, 'customer_id', 'customer_ID');
    }
}
