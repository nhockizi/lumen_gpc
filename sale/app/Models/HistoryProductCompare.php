<?php

namespace App\Models;

class HistoryProductCompare extends BaseModel
{
    public    $timestamps   = false;
    public    $incrementing = false;
    protected $table        = 'history_product_compare';
    protected $primaryKey   = null;
    protected $fillable     = [
            'history_product_id',
            'field',
            'data_old',
            'data_new',
        ];

    public function history_product()
    {
        return $this->hasOne(HistoryProduct::class, 'id', 'history_product_id');
    }
}
