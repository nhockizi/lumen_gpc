<?php

namespace App\Models;

class ProductTmp extends BaseModel
{
    public    $timestamps = false;
    protected $table      = 'product_tmp';
    protected $fillable   = [
            'data_row',
            'sku',
            'name',
            'color',
            'extra_field',
            'quality_label',
            'group_brand_id',
            'product_type_ID',
            'brand_brand_ID',
            'model_parent_ID',
            'brand_parent',
            'price',
            'price_purchase',
            'amount',
            'rack',
            'row',
            'column',
            'location',
            'session',
            'model_ID',
            'saved'
        ];

    public function location_data()
    {
        return $this->hasOne(Location::class, 'location_ID', 'location');
    }

    public function image_tmp()
    {
        return $this->hasMany(ImageTmp::class, 'data_row', 'data_row');
    }
}
