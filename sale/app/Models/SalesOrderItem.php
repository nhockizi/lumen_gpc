<?php

namespace App\Models;

class SalesOrderItem extends BaseModel
{
    protected $table = 'sales_order_item';
    protected $primaryKey = 'id';
    public $timestamps = false;
    protected $fillable = [
        'sales_order_id',
        'product_id',
        'price',
        'quantity',
        'total',
    ];

}
