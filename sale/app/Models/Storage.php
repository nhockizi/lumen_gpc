<?php

namespace App\Models;

class Storage extends BaseModel
{
    public    $timestamps = false;
    protected $table      = 'storage';
    protected $primaryKey = 'storage_ID';
    protected $fillable   = [
            'storage_ID',
            'rack',
            'row',
            'column',
        ];

    public function storage_has_product()
    {
        return $this->hasMany(StorageHasProduct::class,'storage_storage_ID');
    }

    public function storage_absolute()
    {
        return $this->hasOne(StorageAbsolute::class,'storage_ID');
    }
}
