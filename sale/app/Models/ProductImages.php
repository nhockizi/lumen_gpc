<?php

namespace App\Models;

class ProductImages extends BaseModel
{
    public    $timestamps = false;
    protected $table      = 'product_images';
    protected $primaryKey = 'image_ID';
    protected $fillable
                          = [
            'image_ID',
            'product_product_ID',
            'is_thumbnail',
            'image_url',
            'source',
            'deleted',
        ];

}
