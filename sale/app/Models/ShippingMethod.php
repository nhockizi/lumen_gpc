<?php

namespace App\Models;

use App\Models\BaseModel;

class ShippingMethod extends BaseModel
{
    protected $table = 'shipping_method';
    protected $primaryKey = 'shipping_method_ID';
    public $timestamps = false;
    protected $fillable = [];

}
