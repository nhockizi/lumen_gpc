<?php

namespace App\Models;

use App\Models\BaseModel;

class ProductType extends BaseModel
{
    public    $timestamps = false;
    protected $table = 'product_type';
    protected $primaryKey = 'product_type_ID';
    protected $fillable = [
        'product_type_name',
        'product_type_code',
        'description_nl',
        'description_en',
    ];

}
