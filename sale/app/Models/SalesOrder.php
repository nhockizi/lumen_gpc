<?php

namespace App\Models;

class SalesOrder extends BaseModel
{
    protected $table = 'sales_order';
    protected $primaryKey = 'id';
    protected $fillable = [
        'sales_id',
        'total',
        'status',
    ];

    public function sale(){
        return $this->hasOne(Sales::class, 'id', 'sales_id');
    }

    public function sales_order_item(){
        return $this->hasMany(SalesOrderItem::class, 'sales_order_id', 'id');
    }
}
