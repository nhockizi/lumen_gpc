<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get(
    '/',
    function () use ($router) {
        return $router->app->version();
    }
);

if (class_exists(Melihovv\LaravelLogViewer\LaravelLogViewerServiceProvider::class)) {
    $router->group(
        ['namespace' => '\Melihovv\LaravelLogViewer'],
        function () use ($router) {
            $router->get('logs', 'LaravelLogViewerController@index');
        }
    );
}

$prefix = env('APP_PREFIX', 'api').'/'.env('APP_VERSION', 'v1');

$router->group(
    [
        'prefix' => $prefix,
    ],
    function ($router) {
        require_once 'auth.php';
        require_once 'saleOrder.php';
    }
);