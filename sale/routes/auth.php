<?php
$router->group(
    [
        'prefix'     => 'auth',
        'middleware' => 'sale',
    ],
    function ($router) {
        $router->get(
            'reset-key',
            [
                'uses' => 'AuthController@resetKey',
            ]
        );
    }
);