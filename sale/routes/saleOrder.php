<?php
$router->group(
    [
        'prefix'     => 'sale-order',
        'middleware' => 'sale',
    ],
    function ($router) {
        $router->get(
            'list',
            [
                'uses' => 'SaleOrderController@list',
            ]
        );
        $router->post(
            'create',
            [
                'uses' => 'SaleOrderController@create',
            ]
        );
        $router->post(
            'update/{id:[0-9]+}',
            [
                'uses' => 'SaleOrderController@update',
            ]
        );
        $router->get(
            'detail/{id:[0-9]+}',
            'SaleOrderController@get'
        );
        $router->get(
            'delete/{id:[0-9]+}',
            [
                'uses' => 'SaleOrderController@delete',
            ]
        );
    }
);