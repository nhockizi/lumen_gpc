<?php
$router->group(
    [
        'prefix' => 'bon',
    ],
    function ($router) {
        $router->get(
            'test',
            'BonController@test'
        );
    }
);
$router->group(
    [
        'prefix'     => 'bon',
        'middleware' => 'jwt',
    ],
    function ($router) {
        $router->get(
            'general-post-order-today',
            'BonController@generalPostOrderToday'
        );
        $router->get(
            'list-by-customer/{id:[0-9]+}',
            'BonController@listByCustomer'
        );
        $router->get(
            'list-post-todo',
            'BonController@listPostTodo'
        );
        $router->get(
            'list-post-doing',
            'BonController@listPostDoing'
        );
        $router->get(
            'list-post-review',
            'BonController@listPostReview'
        );
        $router->get(
            'list-post-done',
            'BonController@listPostDone'
        );
        $router->get(
            'list-verkoop-orders',
            'BonController@listVerkoopOrders'
        );
        $router->get(
            'list-post-orders',
            'BonController@listPostOrders'
        );
        $router->get(
            'list-invoice-magento',
            'BonController@listInvoiceMagento'
        );
        $router->get(
            'general',
            'BonController@general'
        );
        $router->get(
            'list-transaction-invoice',
            'BonController@listTransactionInvoice'
        );
        $router->get(
            'list-transaction-invoice/general',
            'BonController@masterData'
        );
        $router->post(
            'general-pdf',
            'BonController@generatePDF'
        );
        $router->post(
            'download-pdf',
            'BonController@downloadPDF'
        );
        $router->post(
            'create',
            [
                'middleware' => 'permission',
                'uses'       => 'BonController@create',
            ]
        );
        $router->get(
            'detail/{id:[0-9]+}',
            'BonController@get'
        );
        $router->post(
            'update/{id:[0-9]+}',
            [
                'middleware' => 'permission',
                'uses'       => 'BonController@update',
            ]
        );
        $router->get(
            'terms',
            'BonController@terms'
        );
        $router->get(
            'email-content',
            'BonController@emailContent'
        );
        $router->post(
            'generate-proforma-pdf',
            'BonController@generateProformaPDF'
        );
        $router->post(
            'send-email',
            'BonController@sendEmail'
        );
        $router->get(
            'delete/{id:[0-9]+}',
            [
                'middleware' => 'permission',
                'uses'       => 'BonController@delete',
            ]
        );
        $router->post(
            'confirm',
            'BonController@confirm'
        );
        $router->post(
            'confirm-shipments',
            'BonController@confirmShipments'
        );
        $router->get(
            'revert/{id:[0-9]+}',
            'BonController@revert'
        );
    }
);