<?php
$router->group(
    [
        'prefix' => 'shipping-method',
        'middleware' => 'jwt',
    ],
    function ($router) {
        $router->get(
            'list',
            'ShippingMethodController@list'
        );
    }
);