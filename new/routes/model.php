<?php
$router->group(
    [
        'prefix' => 'model',
        'middleware' => 'jwt',
    ],
    function ($router) {
        $router->get(
            'list',
            [
//                'middleware' => 'permission',
                'uses' => 'ModelController@list',
            ]
        );
        $router->get(
            'delete/{id:[0-9]+}',
            [
                'middleware' => 'permission',
                'uses' => 'ModelController@delete',
            ]
        );
        $router->post(
            'create',
            [
                'middleware' => 'permission',
                'uses' => 'ModelController@create',
            ]
        );
        $router->post(
            'update/{id:[0-9]+}',
            [
                'middleware' => 'permission',
                'uses' => 'ModelController@update',
            ]
        );
    }
);