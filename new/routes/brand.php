<?php
$router->group(
    [
        'prefix' => 'brand',
        'middleware' => 'jwt',
    ],
    function ($router) {
        $router->get(
            'list',
            [
//                'middleware' => 'permission',
                'uses' => 'BrandController@list',
            ]
        );
        $router->get(
            'list-with-model',
            'BrandController@listWithModel'
        );
        $router->get(
            'list-with-model/{product_id:[0-9]+}',
            'BrandController@listWithModel'
        );
        $router->get(
            'detail/{id:[0-9]+}',
            'BrandController@get'
        );
        $router->get(
            'delete/{id:[0-9]+}',
            [
                'middleware' => 'permission',
                'uses' => 'BrandController@delete',
            ]
        );
        $router->post(
            'create',
            [
                'middleware' => 'permission',
                'uses' => 'BrandController@create',
            ]
        );
        $router->post(
            'update/{id:[0-9]+}',
            [
                'middleware' => 'permission',
                'uses' => 'BrandController@update',
            ]
        );
    }
);