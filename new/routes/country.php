<?php
$router->group(
    [
        'prefix'     => 'country',
    ],
    function ($router) {
        $router->get(
            'list',
            'CountryController@list'
        );
    }
);