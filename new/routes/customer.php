<?php
$router->group(
    [
        'prefix'     => 'customer',
        'middleware' => 'jwt',
    ],
    function ($router) {
        $router->get(
            'list',
            [
//                'middleware' => 'permission',
                'uses' => 'CustomerController@list',
            ]
        );
        $router->post(
            'check-email',
            'CustomerController@checkEmail'
        );
        $router->post(
            'check-post-code',
            'CustomerController@checkPostCode'
        );
        $router->get(
            'detail/{id:[0-9]+}',
            'CustomerController@get'
        );
        $router->post(
            'update/{id:[0-9]+}',
            [
                'middleware' => 'permission',
                'uses' => 'CustomerController@update',
            ]
        );
        $router->get(
            'delete/{id:[0-9]+}',
            [
                'middleware' => 'permission',
                'uses' => 'CustomerController@delete',
            ]
        );
    }
);