<?php
$router->group(
    [
        'prefix' => 'product-tmp',
        'middleware' => 'jwt',
    ],
    function ($router) {
        $router->get(
            'list',
            [
                'uses' => 'ProductTmpController@list',
            ]
        );
        $router->get(
            'get-data-by-row',
            [
                'uses' => 'ProductTmpController@getDataByRow',
            ]
        );
        $router->post(
            'create',
            [
                'uses' => 'ProductTmpController@create',
            ]
        );
        $router->post(
            'save',
            [
                'uses' => 'ProductTmpController@save',
            ]
        );
        $router->post(
            'rollback',
            [
                'uses' => 'ProductTmpController@rollback',
            ]
        );
        $router->post(
            'delete',
            [
                'uses' => 'ProductTmpController@delete',
            ]
        );
    }
);