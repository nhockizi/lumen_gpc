<?php
$router->group(
    [
        'prefix'     => 'customer-address',
        'middleware' => 'jwt',
    ],
    function ($router) {
        $router->get(
            'list',
            [
//                'middleware' => 'permission',
                'uses' => 'CustomerAddressController@list',
            ]
        );
        $router->post(
            'create',
            [
//                'middleware' => 'permission',
                'uses' => 'CustomerAddressController@create',
            ]
        );
        $router->post(
            'update/{id:[0-9]+}',
            [
//                'middleware' => 'permission',
                'uses' => 'CustomerAddressController@update',
            ]
        );
        $router->get(
            'delete/{id:[0-9]+}',
            [
//                'middleware' => 'permission',
                'uses' => 'CustomerAddressController@delete',
            ]
        );
    }
);