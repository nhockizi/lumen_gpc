<?php
$router->group(
    [
        'prefix' => 'product',
    ]
    ,
    function ($router) {
        $router->get(
            'max',
            'ProductController@maxId'
        );
    }
);
$router->group(
    [
        'prefix'     => 'product',
        'middleware' => 'jwt',
    ],
    function ($router) {
        $router->get(
            'list-datatable',
            'ProductController@listDataTable'
        );
        $router->get(
            'list',
            [
//                'middleware' => 'permission',
                'uses' => 'ProductController@list',
            ]
        );
        $router->get(
            'list-single',
            'ProductController@listSingle'
        );
        $router->get(
            'get-quantity-notification/{id:[0-9]+}',
            'ProductController@getQuantityNotification'
        );
        $router->get(
            'detail-by-list-sku',
            'ProductController@getByListSku'
        );
        $router->get(
            'detail-by-list-id',
            'ProductController@getByListId'
        );
        $router->get(
            'detail-by-sku/{sku}',
            'ProductController@getBySku'
        );
        $router->get(
            'detail/{id:[0-9]+}',
            'ProductController@get'
        );
        $router->post(
            'update-quantity-notification',
            'ProductController@updateQuantityNotification'
        );
        $router->post(
            'create',
            [
                'middleware' => 'permission',
                'uses' => 'ProductController@create',
            ]
        );
        $router->post(
            'update/{id:[0-9]+}',
            [
                'middleware' => 'permission',
                'uses' => 'ProductController@update',
            ]
        );
        $router->post(
            'update-row/{id:[0-9]+}',
            'ProductController@updateRow'
        );
        $router->get(
            'export-list-almost-out-of-stock',
            'ProductController@exportListAlmostOutOfStock'
        );
        $router->get(
            'export-price-list',
            'ProductController@exportPriceList'
        );
        $router->post(
            'synchronize',
            'ProductController@synchronize'
        );
        $router->get(
            'synchronize-by-sku/{sku}',
            'ProductController@synchronizeUpdateProduct'
        );
        $router->get(
            'synchronize-images',
            'ProductController@synchronizeImages'
        );
        $router->get(
            'synchronize-images/{id:[0-9]+}',
            'ProductController@synchronizeImages'
        );
        $router->get(
            'get-stock-change/{id:[0-9]+}',
            'ProductController@getStockChange'
        );
        $router->get(
            'delete/{id:[0-9]+}',
            [
                'middleware' => 'permission',
                'uses' => 'ProductController@delete',
            ]
        );
        $router->post(
            'get-price-by-sku',
            'ProductController@getPriceBySku'
        );
        $router->post(
            'get-stock-amount-by-sku',
            'ProductController@getStockAmountBySku'
        );
    }
);

//Antom
$router->group(
    [
        'prefix'     => 'product',
        'middleware' => 'jwt',
    ],
    function ($router) {
        $router->get(
            'list-not-ready',
            'ProductController@listTmp'
        );
        $router->post(
            'create-not-ready',
            'ProductController@createTmp'
        );
        $router->post(
            'update-not-ready/{id:[0-9]+}',
            'ProductController@updateTmp'
        );
    }
);