<?php
$router->group(
    [
        'prefix' => 'sale',
        'middleware' => 'jwt',
    ],
    function ($router) {
        $router->get(
            'general',
            [
                'uses' => 'SaleController@general',
            ]
        );
        $router->get(
            'list',
            [
                'middleware' => 'permission',
                'uses' => 'SaleController@list',
            ]
        );
        $router->post(
            'create',
            [
                'middleware' => 'permission',
                'uses' => 'SaleController@create',
            ]
        );
        $router->post(
            'update/{id:[0-9]+}',
            [
                'middleware' => 'permission',
                'uses' => 'SaleController@update',
            ]
        );
        $router->get(
            'detail/{id:[0-9]+}',
            'SaleController@get'
        );
        $router->get(
            'delete/{id:[0-9]+}',
            [
                'middleware' => 'permission',
                'uses' => 'SaleController@delete',
            ]
        );
    }
);