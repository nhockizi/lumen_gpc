<?php
$router->group(
    [
        'prefix' => 'shipment',
        'middleware' => 'jwt',
    ],
    function ($router) {
        $router->get(
            'list',
            [
//                'middleware' => 'permission',
                'uses' => 'ShipmentController@list',
            ]
        );
        $router->get(
            'detail/{id:[0-9]+}',
            'ShipmentController@get'
        );
        $router->get(
            'delete/{id:[0-9]+}',
            [
                'middleware' => 'permission',
                'uses' => 'ShipmentController@delete',
            ]
        );
    }
);