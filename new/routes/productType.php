<?php
$router->group(
    [
        'prefix' => 'product-type',
        'middleware' => 'jwt',
    ],
    function ($router) {
        $router->get(
            'list',
            [
//                'middleware' => 'permission',
                'uses' => 'ProductTypeController@list',
            ]
        );
        $router->post(
            'create',
            [
                'middleware' => 'permission',
                'uses' => 'ProductTypeController@create',
            ]
        );
        $router->post(
            'update/{id:[0-9]+}',
            [
                'middleware' => 'permission',
                'uses' => 'ProductTypeController@update',
            ]
        );
        $router->get(
            'delete/{id:[0-9]+}',
            [
                'middleware' => 'permission',
                'uses' => 'ProductTypeController@delete',
            ]
        );
    }
);