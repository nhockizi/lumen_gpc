<?php
$router->group(
    [
        'prefix' => 'user',
        'middleware' => 'jwt',
    ],
    function ($router) {
        $router->get(
            'list',
            [
//                'middleware' => 'permission',
                'uses' => 'UserController@list',
            ]
        );
        $router->post(
            'create',
            [
                'middleware' => 'permission',
                'uses' => 'UserController@create',
            ]
        );
        $router->post(
            'update/{id:[0-9]+}',
            [
                'middleware' => 'permission',
                'uses' => 'UserController@update',
            ]
        );
        $router->get(
            'detail/{id:[0-9]+}',
            'UserController@get'
        );
    }
);