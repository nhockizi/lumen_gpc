<?php
$router->group(
    [
        'prefix' => 'history-product',
        'middleware' => 'jwt',
    ],
    function ($router) {
        $router->get(
            'list',
            [
                //                'middleware' => 'permission',
                'uses' => 'HistoryProductController@list',
            ]
        );
        $router->get(
            'get-by-product/{product_id:[0-9]+}',
            'HistoryProductController@getByProduct'
        );
        $router->post(
            'rollback',
            'HistoryProductController@rollback'
        );
    }
);