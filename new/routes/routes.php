<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get(
    '/',
    function () use ($router) {
        return $router->app->version();
    }
);

if (class_exists(Melihovv\LaravelLogViewer\LaravelLogViewerServiceProvider::class)) {
    $router->group(
        ['namespace' => '\Melihovv\LaravelLogViewer'],
        function () use ($router) {
            $router->get('logs', 'LaravelLogViewerController@index');
        }
    );
}

$prefix = env('APP_PREFIX', 'api').'/'.env('APP_VERSION', 'v1');

$router->group(
    [
        'prefix' => $prefix,
    ],
    function ($router) {
        require_once 'auth.php';
        require_once 'user.php';
        require_once 'product.php';
        require_once 'brand.php';
        require_once 'model.php';
        require_once 'location.php';
        require_once 'productType.php';
        require_once 'stockChange.php';
        require_once 'bon.php';
        require_once 'customer.php';
        require_once 'customerAddress.php';
        require_once 'country.php';
        require_once 'shippingMethod.php';
        require_once 'shipment.php';
        require_once 'role.php';
        require_once 'storage.php';
        require_once 'historyProduct.php';
        require_once 'productTmp.php';
        require_once 'sale.php';
        require_once 'saleOrder.php';
    }
);