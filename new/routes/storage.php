<?php
$router->group(
    [
        'prefix' => 'storage',
        'middleware' => 'jwt',
    ],
    function ($router){
        $router->get(
            'list-rack/{locationID:[0-9]+}',
            [
                //                'middleware' => 'permission',
                'uses' => 'StorageController@listRack',
            ]
        );
        $router->get(
            'list',
            [
                //                'middleware' => 'permission',
                'uses' => 'StorageController@list',
            ]
        );
        $router->get(
            'export-excel',
            [
                //                'middleware' => 'permission',
                'uses' => 'StorageController@exportExcel',
            ]
        );
    }
);