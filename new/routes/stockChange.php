<?php
$router->group(
    [
        'prefix' => 'stock-change',
        'middleware' => 'jwt',
    ],
    function ($router) {
        $router->get(
            'get-by-product/{id:[0-9]+}',
            'StockChangeController@getByProduct'
        );
    }
);