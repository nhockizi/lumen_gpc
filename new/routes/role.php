<?php
$router->group(
    [
        'prefix' => 'role',
        'middleware' => 'jwt',
    ],
    function ($router) {
        $router->get(
            'list',
            [
                'middleware' => 'permission',
                'uses' => 'RolesController@list',
            ]
        );
        $router->get(
            'list-permission',
            'RolesController@listPermission'
        );
        $router->post(
            'create',
            [
                'middleware' => 'permission',
                'uses' => 'RolesController@create',
            ]
        );
        $router->post(
            'update/{id:[0-9]+}',
            [
                'middleware' => 'permission',
                'uses' => 'RolesController@update',
            ]
        );
        $router->get(
            'detail/{id:[0-9]+}',
            'RolesController@get'
        );
    }
);

$router->group(
    [
        'prefix' => 'role-controller',
        'middleware' => 'jwt',
    ],
    function ($router) {
        $router->get(
            'list',
            [
//                'middleware' => 'permission',
                'uses' => 'RolesControllerController@list',
            ]
        );
        $router->get(
            'list-roles/{id:[0-9]+}',
            'RolesControllerController@listRoles'
        );
        $router->post(
            'create',
            [
                'middleware' => 'permission',
                'uses' => 'RolesControllerController@create',
            ]
        );
        $router->post(
            'update/{id:[0-9]+}',
            [
                'middleware' => 'permission',
                'uses' => 'RolesControllerController@update',
            ]
        );
        $router->get(
            'detail/{id:[0-9]+}',
            'RolesControllerController@get'
        );
    }
);