<?php
$router->group(
    ['prefix' => 'auth'],
    function ($router) {
        $router->post('login', 'AuthController@login');
        $router->delete('logout', 'AuthController@logout');
    }
);

$router->group(
    [
        'prefix'     => 'auth',
        'middleware' => 'jwt',
    ],
    function ($router) {
        $router->get(
            'user',
            'AuthController@user'
        );
        $router->get(
            'permission',
            'AuthController@permission'
        );
    }
);