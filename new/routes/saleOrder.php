<?php
$router->group(
    [
        'prefix'     => 'sale-order',
        'middleware' => 'jwt',
    ],
    function ($router) {
        $router->get(
            'general',
            [
                'uses' => 'SaleOrderController@general',
            ]
        );
        $router->get(
            'list',
            [
                'middleware' => 'permission',
                'uses'       => 'SaleOrderController@list',
            ]
        );
        $router->post(
            'create',
            [
                'middleware' => 'permission',
                'uses'       => 'SaleOrderController@create',
            ]
        );
        $router->post(
            'update/{id:[0-9]+}',
            [
                'middleware' => 'permission',
                'uses'       => 'SaleOrderController@update',
            ]
        );
        $router->get(
            'detail/{id:[0-9]+}',
            'SaleOrderController@get'
        );
        $router->get(
            'delete/{id:[0-9]+}',
            [
                'middleware' => 'permission',
                'uses'       => 'SaleOrderController@delete',
            ]
        );
    }
);