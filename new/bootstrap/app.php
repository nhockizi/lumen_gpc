<?php

require_once __DIR__.'/../vendor/autoload.php';

try {
    (new Dotenv\Dotenv(__DIR__.'/../'))->load();
} catch (Dotenv\Exception\InvalidPathException $e) {
    //
}

/*
|--------------------------------------------------------------------------
| Create The Application
|--------------------------------------------------------------------------
|
| Here we will load the environment and create the application instance
| that serves as the central piece of this framework. We'll use this
| application as an "IoC" container and router for this framework.
|
*/

$app = new Laravel\Lumen\Application(
    realpath(__DIR__.'/../')
);

$app->configure('app');
$app->configure('auth');
$app->configure('jwt');
$app->configure('filesystems');
$app->configure('repository');
$app->configure('mail');
$app->configure('queue');
$app->configure('variables');
$app->configure('swagger-lume');

$app->withFacades(true, [
    /**
     * Class alias here
     */
    App\Services\Response\ResponseFacade::class => 'RS',
    \Illuminate\Support\Facades\Crypt::class => 'Crypt',
    \Laravel\Lumen\Http\Redirector::class => 'redirect',
]);

$app->alias('mailer', \Illuminate\Contracts\Mail\Mailer::class);
$app->alias('Image', \Intervention\Image\Facades\Image::class);

$app->withEloquent();

/*
|--------------------------------------------------------------------------
| Register Container Bindings
|--------------------------------------------------------------------------
|
| Now we will register a few bindings in the service container. We will
| register the exception handler and the console kernel. You may add
| your own bindings here if you like or you can make another file.
|
*/

$app->singleton(
    Illuminate\Contracts\Debug\ExceptionHandler::class,
    App\Exceptions\Handler::class
);

$app->singleton(
    Illuminate\Contracts\Console\Kernel::class,
    App\Console\Kernel::class
);

/*
|--------------------------------------------------------------------------
| Register Middleware
|--------------------------------------------------------------------------
|
| Next, we will register the middleware with the application. These can
| be global middleware that run before and after each request into a
| route or middleware that'll be assigned to some specific routes.
|
*/

 $app->middleware([
     App\Http\Middleware\CorsMiddleware::class,
 ]);

$app->routeMiddleware([
    'auth' => App\Http\Middleware\Authenticate::class,
    'jwt' => App\Http\Middleware\JwtMiddleware::class,
    'permission' => App\Http\Middleware\PermissionMiddleware::class,
]);

$app->singleton(
    Illuminate\Contracts\Filesystem\Factory::class,
    function ($app) {
        return new Illuminate\Filesystem\FilesystemManager($app);
    }
);
/*
|--------------------------------------------------------------------------
| Register Service Providers
|--------------------------------------------------------------------------
|
| Here we will register all of the application's service providers which
| are used to bind services into the container. Service providers are
| totally optional, so you are not required to uncomment this line.
|
*/

$app->register(App\Providers\AppServiceProvider::class);
$app->register(App\Providers\AuthServiceProvider::class);
$app->register(App\Providers\EventServiceProvider::class);
$app->register(Prettus\Repository\Providers\RepositoryServiceProvider::class);
$app->register(\Illuminate\Filesystem\FilesystemServiceProvider::class);
$app->register(\App\Providers\LogServiceProvider::class);
$app->register(\Illuminate\Encryption\EncryptionServiceProvider::class);
$app->register(\Illuminate\Mail\MailServiceProvider::class);
$app->register(\VladimirYuldashev\LaravelQueueRabbitMQ\LaravelQueueRabbitMQServiceProvider::class);
$app->register(\Intervention\Image\ImageServiceProvider::class);
$app->register(Maatwebsite\Excel\ExcelServiceProvider::class);
if(class_exists(Melihovv\LaravelLogViewer\LaravelLogViewerServiceProvider::class)){
    $app->register(Melihovv\LaravelLogViewer\LaravelLogViewerServiceProvider::class);
}
$app->register(\SwaggerLume\ServiceProvider::class);
$app->register(App\Providers\MagentoServiceProvider::class);

/*
|--------------------------------------------------------------------------
| Load The Application Routes
|--------------------------------------------------------------------------
|
| Next we will include the routes file so that they can all be added to
| the application. This will provide all of the URLs the application
| can respond to, as well as the controllers that may handle them.
|
*/

$app->router->group([
    'namespace' => 'App\Http\Controllers',
], function ($router) {
    require __DIR__.'/../routes/routes.php';
});

return $app;
