<?php
define('ACTIVE', 1);
define('INACTIVE', 0);
define('BC_DEFAULT_SCALE', 2);
define('URL_MAIN', 'http://www.gsmpartscenter.com/catalog/product/redirect/sku/');

define('TIMEZONE_KEY', 'timezone');
define('FORMAT_DATE_TIME', 'd-m-Y H:i:s');
define('FORMAT_DATE_TIME_DB', 'Y-m-d H:i:s');
define('FORMAT_DATE', 'd-m-Y');
define('FORMAT_DATE_DB', 'Y-m-d');

define('MAX_FILE_LOG', 30);

define('ITEM_LENGHT', 15);
define('OUT_OF_STOCK', 5);

define('IOC_RESTFUL', 'restful');
define('IOC_RESTFUL_REQUEST', 'restful_request');
define('IOC_USER_LOGGED_IN_ID', 'user_logged_in_id');
define('IOC_USER_ROLES_IN_ID', 'user_roles_in_id');

define('VERKOOP_OP_REKENING', 1);
define('VERKOOP_BETAALD', 2);
define('POST_BETAALD', 3);
define('POST_OP_REKENING', 4);
define(
    'BON_TYPE_DATA',
    [
        VERKOOP_OP_REKENING => 'VERKOOP - Op rekening',
        VERKOOP_BETAALD     => 'VERKOOP - Betaald',
        POST_BETAALD        => 'POST - Betaald',
        POST_OP_REKENING    => 'POST - Op rekening',
    ]
);

define('PROCESS_TODO', 1);
define('PROCESS_DOING', 2);
define('PROCESS_REVIEW', 3);
define('PROCESS_DONE', 4);

define(
    'PAYMENT_METHOD',
    [
        'ideal'           => 'ideal',
        'paypal_express'  => 'paypal_express',
        'paypal_standard' => 'paypal_standard',
        'mrcash'          => 'mrcash',
        'ccsave'          => 'ccsave',
        'checkmo'         => 'checkmo',
        'banktransfer'    => 'banktransfer',
    ]
);
define(
    'PDF', [
    'AUTHOR'  => 'GPC',
    'TITLE'   => 'GPC Factuur',
    'SUBJECT' => 'Factuur',
    'KEY'     => 'GPC, Factuur',
]
);
define('API_POSTCODE_URL', 'https://api.postcode.nl/rest/addresses');
define('API_POSTCODE_USERNAME', 'tR0HN9R5exfekc8BKt5ekVk0hyQnfzyXdrtqmtKpGu2');
define('API_POSTCODE_PASSWORD', 'C5auzgWTYTV7ScdSkq8ZUe31rsoemOWMbpUEWXxWJYFBWGOU3r');

define('ORDER_STATUS_CANCElED', 'canceled');
define('ORDER_STATUS_PROCESSING', 'processing');
define('ORDER_STATUS_COMPLETE', 'complete');

define('KEY_BON_CONFIRM', '35mgKqcTTqfk20LgoibAfc26f');
define('URL_BON_CONFIRM', 'http://facturen.gpcgsm.nl/kz/api/customer.php');

define('INVOICE_TYPE_VERKOOP_ORDER', 1);
define('INVOICE_TYPE_POST_ORDER', 2);
define('INVOICE_TYPE_MAGENTO_ORDER', 3);
define('INVOICE_TYPE_ORDER_NOT_YET_CONFIRM', 4);
define('INVOICE_TYPE_TODO', 5);
define('INVOICE_TYPE_DOING', 6);
define('INVOICE_TYPE_REVIEW', 7);
define('INVOICE_TYPE_DONE', 8);

define('IP_API_URL', 'http://ip-api.com/php/');

define('SALSE_STATUS_PENDING', 1);
define('SALSE_STATUS_ENABLED', 2);
define('SALSE_STATUS_SUPPENDED', 3);
define(
    'SALSE_STATUS_DATA',
    [
        SALSE_STATUS_PENDING   => 'Pending Confirmation',
        SALSE_STATUS_ENABLED   => 'Enabled',
        SALSE_STATUS_SUPPENDED => 'Suspended',
    ]
);
define('PRIORITY_LOW', 1);
define('PRIORITY_MEDIUM', 2);
define('PRIORITY_HIGH', 3);
define(
    'PRIORITY_DATA',
    [
        PRIORITY_LOW   => 'Low',
        PRIORITY_MEDIUM   => 'Medium',
        PRIORITY_HIGH => 'High',
    ]
);

define('SALSE_ORDER_STATUS_NEW', 1);
define('SALSE_ORDER_STATUS_APPROVAL', 2);
define('SALSE_ORDER_STATUS_CANCEL', 3);
define(
    'SALSE_ORDER_STATUS_DATA',
    [
        SALSE_ORDER_STATUS_NEW   => 'New',
        SALSE_ORDER_STATUS_APPROVAL   => 'Approval',
        SALSE_ORDER_STATUS_CANCEL => 'Cancel',
    ]
);