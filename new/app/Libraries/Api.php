<?php
namespace App\Libraries;

use Illuminate\Http\Response;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Client;
use App\Repositories\LogSyncRepo;

class Api{

    const METHOD_GET = 'GET',
        METHOD_POST = 'POST';

    public static function getInstance($result = null, $statusCode = Response::HTTP_OK, $ops = null)
    {
        $type = Helpers::getClassStatusCode($statusCode);
        $textResult = ($type == 'success') ? 'results':'errors';
        $dataReturn = [
            '_type' => Helpers::getClassStatusCode($statusCode),
            '_time' => date('Y-m-d H:i:s')
        ];
        $logSync = LogSyncRepo::$logSync;
        if($logSync)
        {
            $logSync->type=$type;
            $logSync->sts=$statusCode;    
        }
        if ($ops && is_array($ops)) {
            foreach ($ops as $key => $item) {
                $dataReturn[$key] = $item;
            }
        }
        if (is_string($result)) {
            $dataReturn['message'] = $result;
            if($logSync) {
                $logSync->msg=$dataReturn['message'];
            }
        }
        if (!is_string($result) && $result !== null) {
            $dataReturn[$textResult] = $result;
            if($logSync) {
                $logSync->data_output=json_encode($result);
            }
        }
        if($logSync) {
            $logSync->update();
        }
        return response()->json($dataReturn, $statusCode);
    }

    public static function response($result = null, $statusCode = Response::HTTP_OK, $ops = null)
    {
        return self::getInstance($result, $statusCode, $ops);
    }
}