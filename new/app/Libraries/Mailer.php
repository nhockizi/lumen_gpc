<?php

namespace App\Libraries;

use Illuminate\Support\Facades\Facade;
use Illuminate\Support\Facades\Mail;

class Mailer extends Facade
{
    public function send($to, $subject, $template, $data, $cc_email = [], $filePath = null)
    {
        Mail::send($template, $data, function ($m) use ($to, $subject, $filePath) {
            $m->to($to)
              ->subject($subject)
            ;
            if ($filePath) {
                $m->attach($filePath);
            }
        });
    }
}