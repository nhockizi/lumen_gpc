<?php

namespace App\Models;

class Location extends BaseModel
{
    public    $timestamps = false;
    protected $table      = 'location';
    protected $primaryKey = 'location_ID';
    protected $fillable   = [
        'name',
        'country',
        'province',
        'postalcode',
        'street',
        'additional',
    ];
}
