<?php

namespace App\Models;

class HistoryProduct extends BaseModel
{
    public    $timestamps = false;
    protected $table      = 'history_product';
    protected $fillable   = [
        'product_ID',
        'data_old',
        'data_new',
        'created_by',
        'created_date',
    ];

    public function product()
    {
        return $this->hasOne(Product::class, 'product_ID', 'product_ID');
    }

    public function product_version()
    {
        return $this->hasOne(ProductVersion::class, 'product_ID', 'product_ID');
    }

    public function user()
    {
        return $this->hasOne(Member::class, 'member_ID', 'created_by');
    }

    public function history_product_compare()
    {
        return $this->hasMany(HistoryProductCompare::class, 'history_product_id', 'id');
    }
}
