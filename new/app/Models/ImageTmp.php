<?php

namespace App\Models;

class ImageTmp extends BaseModel
{
    public    $timestamps = false;
    protected $table      = 'image_tmp';
    protected $fillable   = [
            'data_row',
            'image_ID',
            'name',
            'path',
        ];

}
