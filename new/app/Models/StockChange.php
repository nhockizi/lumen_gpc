<?php

namespace App\Models;

class StockChange extends BaseModel
{
    protected $table    = 'stock_change';
    protected $primaryKey = 'stock_change_ID';
    public $timestamps = false;
    protected $fillable = [
        'time',
        'amount_old',
        'amount_new',
        'difference',
        'isAdded',
        'toAmsterdam',
        'product_product_ID',
        'storage_storage_ID',
        'bon_id',
        'random_code',
        'user_id'
    ];

}
