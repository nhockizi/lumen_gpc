<?php

namespace App\Models;

class StorageAbsolute extends BaseModel
{
    public    $timestamps = false;
    protected $table      = 'storage_absolute';
    protected $primaryKey = 'storage_ID';
    protected $fillable
                          = [
            'rack',
            'row',
            'drawer',
        ];
    public function storage()
    {
        return $this->hasOne(Storage::class,'storage_ID');
    }
}
