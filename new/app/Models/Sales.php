<?php

namespace App\Models;

class Sales extends BaseModel
{
    protected $table = 'sales';
    protected $primaryKey = 'id';
    protected $fillable = [
        'name',
        'email',
        'phone',
        'status',
        'priority',
        'api_key',
        'is_delete',
    ];

}
