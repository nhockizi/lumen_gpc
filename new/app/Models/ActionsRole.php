<?php

namespace App\Models;

class ActionsRole extends BaseModel
{
    protected $table = 'actions_role';
    protected $primaryKey = 'id';
    public $timestamps = false;

    protected $fillable = [
        'role_id',
        'controller',
        'action'
    ];
}