<?php

namespace App\Models;

class LogActive extends BaseModel
{
    public    $timestamps = false;
    protected $table      = 'log_active';
    protected $primaryKey = 'id';
    protected $fillable   = [
        'user_id',
        'action',
        'message',
        'ip',
        'location',
        'created_date'
    ];
}
