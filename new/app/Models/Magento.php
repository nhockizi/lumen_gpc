<?php

namespace App\Models;

class Magento
{
    public static function getAdminOrderUrl($_order_id)
    {
        if($_order_id != null) {
            app('Magento');

            return \Mage::helper('adminhtml')->getUrl('adminhtml/sales_order/view/order_id/', ['order_id' => (int)$_order_id]);
        }else{
            return null;
        }
    }
    public static function getPaymentName($method_id)
    {
        app('Magento');
        if ($method_id != "") {
            if (strpos($method_id, "paypal") !== false) {
                return "Paypal";
            }

            if (strpos($method_id, "checkmo") !== false) {
                return "Check / Money order";
            }

            $method = \Mage::helper('mpm/api')->getMethodByCode($method_id);
            if ($method && !empty($method)) {
                return $method['description'];
            }
        }

        return null;
    }

}