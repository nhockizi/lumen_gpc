<?php

namespace App\Models;

class Product extends BaseModel
{
    protected $table      = 'product';
    protected $primaryKey = 'product_ID';
    public    $timestamps = false;

    protected $fillable
        = [
            'product_ID',
            'sku',
            'factory_code',
            'name',
            'name_en',
            'description',
            'description_en',
            'image',
            'price',
            'price_wholesale',
            'price_purchase',
            'sold',
            'color',
            'brand_brand_ID',
            'product_type_ID',
            'keyword',
            'model_parent_ID',
            'model_ID',
            'product_type',
            'extra_field',
            'group_brand_id',
            'extra_model',
            'extra_product_code',
            'quality_label',
            'quality_label',
            'is_ready'
        ];

    public function brand()
    {
        return $this->hasOne(Brand::class, 'brand_ID', 'brand_brand_ID');
    }

    public function model()
    {
        return $this->hasOne(Model::class, 'model_ID', 'model_ID');
    }
    public function model_data()
    {
        return $this->hasOne(Model::class, 'model_ID', 'model_ID');
    }

    public function model_parent()
    {
        return $this->hasOne(Model::class, 'model_ID', 'model_parent_ID');
    }

    public function product_type()
    {
        return $this->hasOne(ProductType::class, 'product_type_ID', 'product_type_ID');
    }

    public function storage_has_product()
    {
        return $this->hasMany(StorageHasProduct::class, 'product_product_ID', 'product_ID');
    }

    public function product_images()
    {
        return $this->hasMany(ProductImages::class, 'product_product_ID', 'product_ID');
    }

    public function product_images_thumbnail()
    {
        return $this->hasOne(ProductImages::class, 'product_product_ID', 'product_ID')->where('is_thumbnail', ACTIVE);
    }

    public function stock_change()
    {
        return $this->hasMany(StockChange::class, 'product_product_ID', 'product_ID');
    }

    public function history_product()
    {
        return $this->hasOne(HistoryProduct::class, 'product_ID', 'product_ID');
    }
}
