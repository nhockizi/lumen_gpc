<?php

namespace App\Models;

class ProformaInvoiceEmail extends BaseModel
{
    protected $table      = 'proforma_invoice_email';
    protected $fillable   = [
        'subject',
        'content',
    ];

}
