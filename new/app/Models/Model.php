<?php

namespace App\Models;

class Model extends BaseModel
{
    public    $timestamps = false;
    protected $table      = 'model';
    protected $primaryKey = 'model_ID';
    protected $fillable   = [
            'name',
            'brand_brand_ID',
            'parent_ID',
        ];

    public function brand()
    {
        return $this->hasOne(Brand::class, 'brand_ID', 'brand_brand_ID');
    }

    public function parent()
    {
        return $this->hasOne(Model::class, 'model_ID', 'parent_ID');
    }

    public function child()
    {
        return $this->hasMany(Model::class, 'parent_ID', 'model_ID');
    }

    public function model()
    {
        return $this->hasMany(Model::class, 'parent_ID', 'model_ID');
    }

    public function model_data()
    {
        return $this->hasMany(Model::class, 'parent_ID', 'model_ID');
    }
}
