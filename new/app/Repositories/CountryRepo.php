<?php

namespace App\Repositories;

use App\Libraries\Helpers;
use App\Models\Brand;
use App\Models\Country;
use App\Models\Customer;
use App\Models\CustomerAddress;
use App\Models\Product;

class CountryRepo extends BaseRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        // TODO: Implement model() method.
        return Country::class;
    }

    public function list()
    {
        $request       = app('request')->all();
        $item_length = $request['length'] ?? ITEM_LENGHT;
        $fieldsSearch  = [
            'customer_id' => ['countries', 'name', 'LIKE'],
            //            'name'     => ['brand', 'name', 'LIKE'],
        ];
        $query         = Country::select('*');
        $query         = Helpers::searchFieldsMapping($query, $fieldsSearch);
        $result        = $query->paginate($item_length);
        $result        = Helpers::formatPagination($result);

        return $result;
    }

}