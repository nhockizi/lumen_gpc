<?php

namespace App\Repositories;

use App\Libraries\Helpers;
use App\Models\Bon;
use App\Models\BonHasProduct;
use App\Models\Invoice;
use App\Models\Magento;
use App\Models\ProformaInvoiceTerms;
use Illuminate\Support\Facades\Log;

class BonRepo extends BaseRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        // TODO: Implement model() method.
        return Bon::class;
    }

    public function list($type)
    {
        $result = Bon::with(
            [
                'customer',
                'shipping_method_data',
            ]
        )
                     ->where('invoice_status', INACTIVE);
        switch ($type) {
            case INVOICE_TYPE_VERKOOP_ORDER:
                $result = $result->whereIn(
                    'bon_status',
                    [
                        VERKOOP_OP_REKENING,
                        VERKOOP_BETAALD,
                    ]
                );
                break;
            case INVOICE_TYPE_POST_ORDER:
                $result = $result->whereNull('is_magento')
                                 ->whereIn(
                                     'bon_status',
                                     [
                                         POST_OP_REKENING,
                                         POST_BETAALD,
                                     ]
                                 );
                break;
            case INVOICE_TYPE_MAGENTO_ORDER:
                $result = $result->where('is_magento', ACTIVE);
                break;
            case INVOICE_TYPE_ORDER_NOT_YET_CONFIRM:
                break;
            case INVOICE_TYPE_TODO:
                $result = $result->whereIn(
                    'status_process',
                    [
                        INACTIVE,
                        PROCESS_TODO,
                    ]
                )
                                 ->whereIn(
                                     'bon_status',
                                     [
                                         POST_OP_REKENING,
                                         POST_BETAALD,
                                     ]
                                 );
                break;
            case INVOICE_TYPE_DOING:
                $result = $result->where('status_process', PROCESS_DOING)
                                 ->whereIn(
                                     'bon_status',
                                     [
                                         POST_OP_REKENING,
                                         POST_BETAALD,
                                     ]
                                 );
                break;
            case INVOICE_TYPE_REVIEW:
                $result = $result->where('status_process', PROCESS_REVIEW)
                                 ->whereIn(
                                     'bon_status',
                                     [
                                         POST_OP_REKENING,
                                         POST_BETAALD,
                                     ]
                                 );
                break;
            case INVOICE_TYPE_DONE:
                $result = $result->where('status_process', PROCESS_DONE)
                                 ->whereIn(
                                     'bon_status',
                                     [
                                         POST_OP_REKENING,
                                         POST_BETAALD,
                                     ]
                                 );
                break;
        }
        $result = $result->orderBy('bon_ID', 'DESC')
                         ->get();

        $array_check = [
            INVOICE_TYPE_TODO,
            INVOICE_TYPE_DOING,
            INVOICE_TYPE_REVIEW,
            INVOICE_TYPE_DONE,
        ];
        foreach ($result as $key => $item) {
            $item->bon_type = BON_TYPE_DATA[$item->bon_status] ?? null;
            $item->pdf        = env('APP_URL') . '/pdf/' . $item->bon_file;
            if (!Helpers::canSeePrice()) {
                $item->pdf = str_replace(".pdf", "_without_price.pdf", $item->pdf);
            }
            if ($item->shipping_method_data) {
                $item->shipping_method_data->name = $this->_convertShippingName($item->shipping_method);
            }
            if (in_array($type, $array_check)) {
                $item->count_left  = Bon::where('customer_customer_ID', $item->customer_customer_ID)
                                        ->where('address_id', $item->address_id)
                                        ->where('invoice_status', INACTIVE)
                                        ->whereIn(
                                            'bon_status', [
                                                            POST_BETAALD,
                                                            POST_OP_REKENING,
                                                        ]
                                        )
                                        ->count();
                $item->count_right = $item->count_left;
            }
        }

        return $result;
    }

    private function _convertShippingName($name)
    {
        $name = str_replace("_", " ", $name);
        if ($name == "standard") {
            $name = "GLS shipping";
        }

        return ucfirst($name);
    }

    public function masterData()
    {
        $result = [
            'payment_method' => Helpers::_map(PAYMENT_METHOD),
            'bon_type'       => Helpers::_map(BON_TYPE_DATA),
        ];

        return $result;
    }

    public function generalPostOrderToday()
    {
        $countGLS           = Bon::where('invoice_status', INACTIVE)
                                 ->whereIn(
                                     'bon_status', [
                                                     POST_OP_REKENING,
                                                     POST_BETAALD,
                                                 ]
                                 )
                                 ->whereIn(
                                     'shipping_method', [
                                                          'gls_standard',
                                                          'express_shipping_s7',
                                                          'express_shipping_s4',
                                                          'express_shipping_s1',
                                                      ]
                                 )
                                 ->count();
        $countUPS           = Bon::where('invoice_status', INACTIVE)
                                 ->whereIn(
                                     'bon_status', [
                                                     POST_OP_REKENING,
                                                     POST_BETAALD,
                                                 ]
                                 )
                                 ->whereIn(
                                     'shipping_method', [
                                                          'ups_shipping',
                                                          'ups_express_saver',
                                                      ]
                                 )
                                 ->count();
        $countInternational = Bon::where('invoice_status', INACTIVE)
                                 ->whereIn(
                                     'bon_status', [
                                                     POST_OP_REKENING,
                                                     POST_BETAALD,
                                                 ]
                                 )
                                 ->where(
                                     function ($q) {
                                         $q->whereHas(
                                             'customer', function ($q) {
                                             $q->whereNull('address_id')
                                               ->whereNotIn('country', Helpers::listEU());
                                         }
                                         )
                                           ->orWhere(
                                               function ($q) {
                                                   $q->whereHas(
                                                       'customer_address', function ($q) {
                                                       $q->whereNotIn('country', Helpers::listEU());
                                                   }
                                                   );
                                               }
                                           );
                                     }
                                 )
                                 ->count();
        $result             = [
            'countGLS'           => $countGLS,
            'countUPS'           => $countUPS,
            'countInternational' => $countInternational,
        ];

        return $result;
    }

    public function general()
    {
        $invoicePost    = Bon::where('invoice_status', INACTIVE)
                             ->whereIn(
                                 'bon_status',
                                 [
                                     POST_BETAALD,
                                     POST_OP_REKENING,
                                 ]
                             )
                             ->count();
        $invoiceVerkoop = Bon::where('invoice_status', INACTIVE)
                             ->whereIn(
                                 'bon_status',
                                 [
                                     VERKOOP_OP_REKENING,
                                     VERKOOP_BETAALD,
                                 ]
                             )
                             ->count();
        $result         = [
            'invoicePost'    => $invoicePost,
            'invoiceVerkoop' => $invoiceVerkoop,
        ];

        return $result;
    }

    public function listTransactionInvoice($data)
    {
        $item_length  = $data['length'] ?? ITEM_LENGHT;
        $fieldsSearch = [
            'customer_name'  => [
                'customer',
                'name',
                'LIKE',
            ],
            'date'           => [
                'bon',
                'bon_datum',
                '=',
            ],
            'invoice_id'     => [
                'bon',
                'bon_ID',
                'LIKE',
            ],
            'payment_method' => [
                'bon',
                'magento_method_payment',
                '=',
            ],
            'magento_id'     => [
                'bon',
                'magento_order_id',
                '=',
            ],
        ];
        $query        = Bon::with(['customer'])
                           ->leftJoin('customer', 'customer.customer_ID', '=', 'bon.customer_customer_ID')
                           ->where('invoice_status', ACTIVE);
        if ($data['product_name']) {
            $query = $query->whereHas(
                'bonHasProduct', function ($bonHasProduct) use ($data) {
                return $bonHasProduct->whereHas(
                    'product', function ($product) use ($data) {
                    return $product->where(
                        'name', 'like', '%' .
                              $data['product_name'] .
                              '%'
                    );
                }
                );
            }
            );
        }
        $query  = $query->orderBy('bon_ID', 'DESC');
        $query  = Helpers::searchFieldsMapping($query, $fieldsSearch);
        $result = $query->paginate($item_length);
        $result = Helpers::formatPagination($result);
        $rows   = [];
        foreach ($result['rows'] as $key => $item) {
            $rows[] = [
                'id'             => $item->bon_ID,
                'date'           => $item->bon_datum,
                'url_pdf'        => env('APP_URL') .
                    '/pdf/' .
                    $item->bon_file,
                'customer'       => $item->customer,
                'magento_url'    => Magento::getAdminOrderUrl($item->magento_order_id),
                'magento_id'     => $item->magento_order_id,
                'payment_method' => Magento::getPaymentName($item->magento_method_payment),
            ];
        }
        $result['rows'] = $rows;

        return $result;
    }

    public function updateTerms($data)
    {
        $check = ProformaInvoiceTerms::first();
        if ($check) {
            ProformaInvoiceTerms::where('content', '!=', null)
                                ->orWhereNull('content')
                                ->update(
                                    [
                                        'content' => $data['terms'],
                                    ]
                                );
        }
        else {
            $terms          = new ProformaInvoiceTerms();
            $terms->content = $data['terms'];
            $terms->save();
        }
    }

    public function generatePDF($data)
    {
        $_location = LocationRepo::getInstance();
        $location  = $_location->find($data['location']);

        //        $_country = CountryRepo::getInstance();
        //        $country  = $_country->findByField('country_code', $data['country']);
        //
        //        $_shipping = ShippingMethodRepo::getInstance();
        //        $shipping  = ShippingMethod::where('code', $data['shipping_type'])->first();
        $invoice = [];

        $_product     = ProductRepo::getInstance();
        $total_number = 0;
        $sub_total    = 0;
        foreach ($data['invoice'] as $item) {
            if ($item['is_product'] == true) {
                $product = $_product->find($item['product_id']);
                $name    = $product->name;
                if (
                    strpos($name, 'Apple') !== false
                ) {
                    $name = 'For ' . $name;
                }
            }
            else {
                $name = $item['product_name'];
            }
            $amount       = (double)Helpers::mul_value($item['price'], $item['number']);
            $invoice[]    = [
                'check'    => null,
                'name'     => $name,
                'location' => $item['storage'],
                'price'    => (double)$item['price'],
                'number'   => (int)$item['number'],
                'amount'   => $amount,
            ];
            $total_number = Helpers::sum_value($total_number, $item['number']);
            $sub_total    = (double)Helpers::sum_value($sub_total, $amount);
        }

        $tax       = 0;
        $sub_total = Helpers::formatNumber($sub_total);
        $shipping  = Helpers::formatNumber($data['shipping']);
        $_total    = Helpers::sum_value($sub_total, $shipping);
        if (
            $data['invoice_type'] ==
            21
        ) {
            $_tax = Helpers::mul_value($_total, 0.21);
            $_tax = Helpers::formatNumber($_tax);

            if (
                $sub_total !=
                $_tax
            ) {
                $tax = $_tax;
            }
        }
        $total  = Helpers::sum_value((double)$_total, (double)$data['payment']);
        $total  = Helpers::sum_value((double)$total, (double)$tax);
        $result = [
            'commercial'    => 'Pakbon',
            'customer_name' => $data['customer_name'] ?? null,
            'company_name'  => $data['company_name'] ?? null,
            'date'          => Helpers::formatDate($data['date']),
            'type'          => BON_TYPE_DATA[$data['type']] ?? null,
            'location'      => $location->name ?? null,
            'note'          => $data['note'] ?? null,
            'street'        => $data['street'] ?? null,
            'city'          => $data['city'] ?? null,
            'post_code'     => $data['post_code'] ?? null,
            'country'       => $data['country'] ?? null,
            'shipping_type' => $shipping->name ?? null,
            'invoice'       => $invoice,
            'view_price'    => $data['view_price'] ?? null,
            'total_number'  => (int)$total_number,
            'sub_total'     => (double)$sub_total,
            'tax'           => (double)$tax,
            'shipping'      => (double)$data['shipping'],
            'payment'       => (double)$data['payment'],
            'total'         => (double)$total,
        ];

        return $result;
    }

    public function _save($data, $type = 'create')
    {
        $authUser      = Helpers::getAuth();
        $authUserId    = $authUser->member_ID;
        $attributesBon = [
            'bon_ID' => $data['id'],
        ];
        $address_id    = null;
        if ($data['new_shipping_address'] == true) {
            $address_id = $data['shipping_address'];
        }
        $valuesBon = [
            'bon_datum'            => $data['date'],
            'price_shipping'       => $data['shipping'] ?? 0,
            'bon_status'           => $data['type'],
            'customer_customer_ID' => $data['customer_id'],
            'member_member_ID'     => $authUserId,
            'invoice_status'       => 0,
            'is_factuur'           => 0,
            'location_ID'          => $data['location'],
            'note'                 => $data['note'],
            'updated_date'         => date('Y-m-d'),
            'status_process'       => 0,
            'tax_amount'           => 0,
            'customer_email'       => $data['email'],
            'address_id'           => $address_id,
            'shipping_method'      => $data['shipping_type'],
            'price'                => $data['payment'] ?? 0,
        ];
        if (
        in_array(
            $data['type'],
            [
                3,
                4,
            ]
        )
        ) {
            $valuesBonType = [
                'country_id' => $data['country'],
            ];
            $valuesBon     = array_merge($valuesBon, $valuesBonType);
        }
        if ($type === 'create') {
            $valuesBon['created_date'] = date('Y-m-d');
        }
        $bon                = $this->updateOrCreate($attributesBon, $valuesBon);
        $payment            = $data['payment'] ?? 0;
        $sub_total          = 0;
        $_bonHasProduct     = BonHasProductRepo::getInstance();
        $_storageHasProduct = StorageHasProductRepo::getInstance();
        $_storage           = StorageRepo::getInstance();
        $_stockChange       = StockChangeRepo::getInstance();
        $ids                = [];
        foreach ($data['invoice'] as $item) {
            $_sub      = Helpers::mul_value($item['price'], $item['number']);
            $sub_total = Helpers::sum_value($sub_total, $_sub);

            $attributesBonHasProduct = [
                'id' => $item['id'] ?? null,
            ];

            if ($item['is_product'] == true) {
                $product_id = $item['product_id'];
            }
            else {
                $product_id = null;
            }
            $storage_id      = null;
            $storage_amount  = [];
            $before_stock    = 0;
            $invoice_storage = null;
            $amountProduct   = $item['number'];
            if (is_array($item['storage'])) {
                $random_code = $_stockChange->generateRandomCode();
                foreach ($item['storage'] as $storage) {
                    if ($amountProduct > 0) {
                        $storage_id[]      = $storage['id'];
                        $storageHasProduct = $_storageHasProduct->find($storage['id']);
                        $before_stock      = Helpers::sum_value($before_stock, $storageHasProduct->amount);
                        $invoice_storage   .= $storageHasProduct->storage->rack;
                        $invoice_storage   .= ' ';
                        $invoice_storage   .= $storageHasProduct->storage->row;
                        $invoice_storage   .= ' ';
                        $invoice_storage   .= $storageHasProduct->storage->column;

                        $amountOld = $storageHasProduct->amount;
                        if ($amountProduct >= $storageHasProduct->amount) {
                            $amountProduct             = Helpers::sub_value(
                                $amountProduct,
                                $storageHasProduct->amount
                            );
                            $storageHasProduct->amount = 0;
                        }
                        else {
                            $storageHasProduct->amount = Helpers::sub_value(
                                $storageHasProduct->amount,
                                $amountProduct
                            );
                            $amountProduct             = 0;
                        }
                        $storageHasProduct->save();
                        $amountNew                      = $storageHasProduct->amount;
                        $storage_amount[$storage['id']] = (int)Helpers::sub_value(
                            $amountOld,
                            $amountNew
                        );
                        $_stockChange->changeStock(
                            [
                                'amount_old' => $amountOld,
                                'amount_new' => $amountNew,
                                'product_id' => $item['product_id'],
                                'storage_id' => $storage['id'],
                            ],
                            'false',
                            'substracted',
                            $random_code
                        );
                    }
                }
                $storage_id     = json_encode($storage_id);
                $storage_amount = json_encode($storage_amount);
            }
            $after_stock         = Helpers::sub_value($before_stock, $item['number']);
            $valuesBonHasProduct = [
                'bon_bon_ID'         => $bon->bon_ID,
                'product_product_ID' => $product_id,
                'amount'             => $item['number'],
                'price'              => $item['price'],
                'storage_ID'         => $storage_id,
                'storage_amount'     => $storage_amount,
                'before_stock'       => $before_stock,
                'after_stock'        => $after_stock,
                'invoice_storage'    => $invoice_storage,
            ];
            $bonHasProduct       = $_bonHasProduct->updateOrCreate($attributesBonHasProduct, $valuesBonHasProduct);
            $ids[]               = $bonHasProduct->id;
        }

        BonHasProduct::where('bon_bon_ID', $bon->bon_ID)
                     ->whereNotIn('id', $ids)
                     ->delete();

        $sub_total = Helpers::formatNumber($sub_total);
        $shipping  = Helpers::formatNumber($valuesBon['price_shipping']);
        $_total    = Helpers::sum_value($sub_total, $shipping);
        if ($data['invoice_type'] == 21) {
            $_tax = Helpers::mul_value($_total, 0.21);
            $_tax = Helpers::formatNumber($_tax);

            if ($sub_total != $_tax) {
                $bon->tax_amount = $_tax;
            }
        }

        $total_price      = Helpers::sum_value($_total, $bon->tax_amount);
        $total_price      = Helpers::sub_value($total_price, $payment);
        $bon->sub_total   = $_total;
        $bon->price_total = $total_price;
        $bon->save();

        return $bon;
    }

    public function listByCustomer($customerID)
    {
        $result = Bon::whereHas(
            'customer',
            function ($q) use ($customerID) {
                $q->where('customer_ID', $customerID);
            }
        )
                     ->get();
        foreach ($result as $key => $item) {
            $item->pdf = env('APP_URL') .
                '/pdf/' .
                $item->bon_file;
        }
        return $result;
    }

    public function detail($id)
    {
        $result = $this->with(
            [
                'customer',
                'customer.countries',
                'bon_has_product',
                'bon_has_product.product',
                'bon_has_product.product.storage_has_product',
            ]
        )
                       ->find($id)
                       ->toArray();
        if ($result['bon_datum'] != null) {
            $result['bon_datum'] = Helpers::formatDate($result['bon_datum'], FORMAT_DATE_DB);
        }
        $result['total_item'] = 0;
        if ($result['bon_has_product']) {
            foreach ($result['bon_has_product'] as $key => $bon_has_product) {
                $result['total_item'] = Helpers::sum_value($result['total_item'], $bon_has_product['amount']);
                $product              = $bon_has_product['product'] ?? null;
                if ($product) {
                    $storage_product = [];
                    $product['id']   = $product['product_ID'] ?? null;
                    $storage_amount  = json_decode($bon_has_product['storage_amount'], true);
                    if (isset($product['storage_has_product'])) {
                        foreach ($product['storage_has_product'] as $storage_has_product) {
                            $is_checked = false;
                            if (isset($storage_amount[$storage_has_product['id']])) {
                                $is_checked = true;
                            }
                            $storage_product[] = [
                                'id'          => $storage_has_product['id'],
                                'amount'      => $storage_has_product['amount'],
                                'rack'        => $storage_has_product->storage->rack ?? null,
                                'row'         => $storage_has_product->storage->row ?? null,
                                'column'      => $storage_has_product->storage->column ?? null,
                                'storage_id'  => $storage_has_product['storage_storage_ID'],
                                'location_id' => $storage_has_product['location_ID'],
                                'is_checked'  => $is_checked,
                            ];
                        }
                    }
                    $product['storage_has_product']             = $storage_product;
                    $result['bon_has_product'][$key]['product'] = $product;
                }
            }
        }
        $result['magento_url']     = Magento::getAdminOrderUrl($id);
        $result['bon_status_name'] = BON_TYPE_DATA[$result['bon_status']] ?? null;
        $result['factuur']         = null;
        if ($result['is_factuur']) {
            $result['factuur'] = 'Factuur';
        }

        return $result;
    }

    public function refundQty($data)
    {
        $_storageHasProduct = StorageHasProductRepo::getInstance();
        $_stockChange       = StockChangeRepo::getInstance();
        $storage_ID         = json_decode($data->storage_ID, true);
        $storage_amount     = json_decode($data->storage_amount, true);
        $random_code        = $_stockChange->generateRandomCode();
        if ($storage_ID) {
            foreach ($storage_ID as $item) {
                $storageHasProduct = $_storageHasProduct->find($item);

                $amount_product            = $storage_amount[$item] ?? 0;
                $amount_old                = $storageHasProduct->amount;
                $storageHasProduct->amount = Helpers::sum_value($amount_old, $amount_product);

                $storageHasProduct->save();

                $_stockChange->changeStock(
                    [
                        'amount_old'  => $amount_old,
                        'amount_new'  => $storageHasProduct->amount,
                        'product_id'  => $storageHasProduct->product_product_ID,
                        'storage_id'  => $storageHasProduct->id,
                        'bon_id'      => $data->bon_bon_ID,
                        'toAmsterdam' => 'false',
                    ],
                    'false',
                    'delete',
                    $random_code
                );
            }
        }
        $storage = $_storageHasProduct->findWhere(['product_product_ID' => $data->product_product_ID]);
        if ($storage) {
            foreach ($storage as $item) {
                if (!strpos($data->storage_amount, '"' . $item->id . '"')) {
                    $_stockChange->changeStock(
                        [
                            'amount_old'  => $item->amount,
                            'amount_new'  => $item->amount,
                            'product_id'  => $item->product_product_ID,
                            'storage_id'  => $item->storage_storage_ID,
                            'bon_id'      => $data->bon_bon_ID,
                            'toAmsterdam' => 'false',
                        ],
                        'false',
                        'delete',
                        $random_code
                    );
                }
            }
        }
    }

    public function confirmShipments($ids)
    {

    }

    public function confirm($ids)
    {
        $result = [
            'count_confirmed_with_f' => 0,
            'count_confirmed'        => 0,
            'count_error'            => 0,
            'sum_amount'             => 0,
            'total_sum_F'            => 0,
            'total_sum'              => 0,
        ];
        foreach ($ids as $id) {
            $invoice = $this->find($id);
            if (!$invoice) {
                $result['count_error']++;
                $result['error_id'][] = $id;
                continue;
            }
            $result['total_sum'] = Helpers::sum_value($result['total_sum'], $invoice->price_total);
            if ($invoice->invoice_status != ACTIVE) {
                $count = $this->process_pakbon($invoice);
                foreach ($count as $key => $value) {
                    $result[$key] = $result[$key] + $value;
                }
                $invoice->invoice_status = ACTIVE;
                $invoice->save();
            }
        }

        return $result;
    }

    private function process_pakbon($invoice)
    {
        $result          = [
            'count_confirmed_with_f' => 0,
            'count_confirmed'        => 0,
            'count_error'            => 0,
            'sum_amount'             => 0,
            'total_sum_F'            => 0,
            'total_sum'              => 0,
        ];
        $checkKZ         = (object)[];
        $checkKZ->status = true;
        $checkKZ         = $this->KZsavecustomer($invoice->customer_customer_ID);
        if ($checkKZ->status == 'ERROR' || $checkKZ->status == false) {
            Log::error(
                'Cannot save customer ID ' .
                $invoice->customer_customer_ID .
                ' to KZ!'
            );
            exit;
        }
        if ($invoice->bon_file) {
            $fileName = env('APP_URL') .
                '/pdf/' .
                $invoice->bon_file;
            $fileSize = filesize($fileName);
            $finfo    = finfo_open(FILEINFO_MIME_TYPE);
            $finfo    = finfo_file($finfo, $fileName);
            $cFile    = new CURLFile($fileName, $finfo, basename($fileName));

            $status_kz = $invoice->bon_status;

            // status is paid
            if (in_array($status_kz, Invoice::statusKzPaid())) {
                $status_kz = 2;
            }
            else {
                if (in_array($status_kz, Invoice::statusKzNotPaid())) {
                    $status_kz = 1;
                }
            }

            if (!empty($invoice->is_factuur)) {
                $status_kz = 3;
            }

            $data = [
                "filedata"   => $cFile,
                "filename"   => $cFile->postname,
                "klant"      => $invoice->rCustomer->name,
                "bon_datum"  => $invoice->bon_datum,
                "bon_total"  => $invoice->price_total,
                "bon_status" => $status_kz,
                "key"        => KEY_BON_CONFIRM,
            ];

            $cURL = curl_init();
            curl_setopt($cURL, CURLOPT_URL, LINK_API);
            curl_setopt($cURL, CURLOPT_POST, true);
            curl_setopt($cURL, CURLOPT_POSTFIELDS, $data);
            curl_setopt($cURL, CURLOPT_INFILESIZE, $fileSize);

            curl_exec($cURL);
            if ($invoice->is_factuur) {
                $result['count_confirmed_with_f'] = $result['count_confirmed_with_f'] +
                    1;
                $result['total_sum_F']            = $result['total_sum_F'] +
                    $invoice->price_total;
            }
            else {
                $result['count_confirmed'] = $result['count_confirmed'] +
                    1;
                $result['sum_amount']      = $result['sum_amount'] +
                    $invoice->price_total;
            }
            curl_close($cURL);
        }
        else {
            Log::error(
                'file pdf ' .
                env('APP_URL') .
                '/pdf/' .
                $invoice->bon_file .
                ' not found'
            );
            $result['count_error'] = $result['count_error'] +
                1;
        }

        return $result;
    }

    private function KZsavecustomer($customer_id)
    {
        $_customer = CustomerRepo::getInstance();
        $customer  = $_customer->find($customer_id);
        if (!$customer) {
            return false;
        }
        $data = [
            'action'     => 'add',
            'customer'   => $customer->name,
            'email'      => $customer->email,
            'street'     => $customer->street,
            'postalcode' => $customer->postalcode,
            'city'       => $customer->city,
            'country'    => $customer->country,
            'vat'        => $customer->VAT,
        ];

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, URL_BON_CONFIRM);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $result = curl_exec($ch);
        curl_close($ch);

        return json_decode($result);
    }
}