<?php

namespace App\Repositories;

use App\Libraries\Helpers;
use App\Models\Sales;

class SaleRepo extends BaseRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        // TODO: Implement model() method.
        return Sales::class;
    }

    public function general()
    {
        $result = [
            'status'   => Helpers::_map(SALSE_STATUS_DATA),
            'priority' => Helpers::_map(PRIORITY_DATA),
        ];

        return $result;
    }

    public function list($data)
    {
        $item_length = $data['length'] ?? ITEM_LENGHT;
        $query       = Sales::select('*');
        $result      = $query->paginate($item_length);
        $result      = Helpers::formatPagination($result);

        return $result;
    }
    public function generalApiKey(){
        return md5(Helpers::getRandomString(50));
    }
    public function _save($data)
    {
        $attributes = [
            'id' => $data['id'],
        ];
        $values     = [
            'name'     => (string)$data['name'],
            'phone'    => (string)$data['phone'],
            'status'   => (int)$data['status'],
            'priority' => (int)$data['priority'],
        ];
        if ($data['id'] === null) {
            $values['api_key'] = $this->generalApiKey();
            $values['email'] = (string)$data['email'];
        }
        $result = $this->updateOrCreate($attributes, $values);

        return $result;
    }

}