<?php

namespace App\Repositories;

use App\Libraries\Helpers;
use App\Models\Brand;
use App\Models\Location;
use App\Models\ProductNotification;
use App\Models\ProductType;

class ProductNotificationRepo extends BaseRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        // TODO: Implement model() method.
        return ProductNotification::class;
    }

}