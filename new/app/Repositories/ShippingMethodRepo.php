<?php

namespace App\Repositories;

use App\Libraries\Helpers;
use App\Models\Model;
use App\Models\ShippingMethod;

class ShippingMethodRepo extends BaseRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        // TODO: Implement model() method.
        return ShippingMethod::class;
    }

    public function list()
    {
        $request       = app('request')->all();
        $item_length = $request['length'] ?? ITEM_LENGHT;
        $fieldsSearch  = [
            'name' => ['shipping_method', 'name', 'Like'],
            //            'parent_id' => ['model', 'parent_ID', '='],
        ];
        $query         = ShippingMethod::select('*');
        $query         = Helpers::searchFieldsMapping($query, $fieldsSearch);
        $result        = $query->paginate($item_length);
        $result        = Helpers::formatPagination($result);

        return $result;
    }
}