<?php

namespace App\Repositories;

use App\Libraries\Helpers;
use App\Models\HistoryProduct;
use App\Models\Model;
use App\Models\Product;
use App\Models\ProductImages;
use App\Models\ProductVersion;
use App\Models\StorageHasProduct;

class HistoryProductRepo extends BaseRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        // TODO: Implement model() method.
        return HistoryProduct::class;
    }

    public function list($data)
    {
        $item_length = $data['length'] ?? ITEM_LENGHT;
        $query       = ProductVersion::with(
            [
                'history_product',
                'history_product.user',
                'product',
            ]
        );
        $query->whereHas(
            'product', function ($q) use ($data) {
            if (isset($data['filter_search'])) {
                $filter_search = explode(' ', $data['filter_search']);
                foreach ($filter_search as $key => $filter) {
                    $q->where(
                        function ($q) use ($filter) {

                            $q->where('name', 'like', '%'.$filter.'%')
                              ->orWhere('description', 'like', '%'.$filter.'%')
                              ->orWhere('sku', 'like', '%'.$filter.'%');

                        }
                    );
                }
            }
        }
        );
        $result = $query->paginate($item_length);
        $result = Helpers::formatPagination($result);

        return $result;
    }

    public function getByProduct($productID)
    {
        $data   = HistoryProduct::with(
            [
                'history_product_compare',
                'product',
                'user',
                'product_version',
            ]
        )
                                ->where('product_ID', $productID)
                                ->orderBy('id', 'DESC')
                                ->get();
        $result = [];

        $_product  = ProductRepo::getInstance();
        $_brand    = BrandRepo::getInstance();
        $_storage  = StorageRepo::getInstance();
        $_location = LocationRepo::getInstance();
        foreach ($data as $item) {
            $history_product_compare = [];
            foreach ($item->history_product_compare as $value) {
                switch ($value->field) {
                    case 'storage':
                        $data_old   = [];
                        $data_new   = [];
                        $data_check = json_decode($value->data_old);
                        foreach ($data_check as $storage) {
                            $storage_data = $_storage->find($storage->storage_storage_ID);
                            $location     = $_location->find($storage->location_ID);
                            $data_old[]   = [
                                'amount'   => $storage->amount,
                                'rack'     => $storage_data->rack ?? null,
                                'row'      => $storage_data->row ?? null,
                                'column'   => $storage_data->column ?? null,
                                'location' => $location->name ?? null,
                            ];
                        }
                        $data_check = json_decode($value->data_new);
                        foreach ($data_check as $storage) {
                            $storage_data = $_storage->find($storage->storage_storage_ID);
                            $location     = $_location->find($storage->location_ID);
                            $data_new[]   = [
                                'amount'   => $storage->amount,
                                'rack'     => $storage_data->rack ?? null,
                                'row'      => $storage_data->row ?? null,
                                'column'   => $storage_data->column ?? null,
                                'location' => $location->name ?? null,
                            ];
                        }
                        break;
                    case 'images':
                        $data_old   = [];
                        $data_new   = [];
                        $data_check = json_decode($value->data_old);
                        foreach ($data_check as $image) {
                            $data_old[] = env('APP_URL').'/product/'.$item->product_ID.'/'.$image->image_url;
                        }
                        $data_check = json_decode($value->data_new);
                        foreach ($data_check as $image) {
                            $data_new[] = env('APP_URL').'/product/'.$item->product_ID.'/'.$image->image_url;
                        }
                        break;
                    case 'extra_model':
                        $data_old   = [];
                        $data_new   = [];
                        $data_check = json_decode($value->data_old);
                        $model      = Model::whereIn('model_ID', $data_check)->get();
                        foreach ($model as $m) {
                            $data_old[] = $m->name;
                        }
                        $data_check = json_decode($value->data_new);
                        $model      = Model::whereIn('model_ID', $data_check)->get();
                        foreach ($model as $m) {
                            $data_new[] = $m->name;
                        }
                        break;
                    case 'group_brand_id':
                        $data_old   = [];
                        $data_new   = [];
                        $data_check = json_decode($value->data_old);
                        foreach ($_product->_brand_group as $_brand_group) {
                            if (in_array($_brand_group['id'], $data_check)) {
                                $data_old[] = $_brand_group['name'];
                            }
                        }
                        $data_check = json_decode($value->data_new);
                        foreach ($_product->_brand_group as $_brand_group) {
                            if (in_array($_brand_group['id'], $data_check)) {
                                $data_new[] = $_brand_group['name'];
                            }
                        }
                        break;
                    default :
                        $data_old = $value->data_old;
                        $data_new = $value->data_new;
                        break;
                }

                $history_product_compare[] = [
                    'field'    => $value->field,
                    'data_old' => $data_old,
                    'data_new' => $data_new,
                ];
            }
            $tmp      = [
                'id'                      => $item->id,
                'product'                 => $item->product,
                'history_product_compare' => $history_product_compare,
                'user'                    => $item->user,
                'product_version'         => ($item->product_version->history_product_id === $item->id) ? true : false,
                'created_date'            => $item->created_date,
            ];
            $result[] = $tmp;
        }

        return $result;
    }

    public function rollback($data)
    {
        $history = $this->find($data['id']);

        $version                     = ProductVersion::where('product_ID', $history->product_ID)->first();
        $version->history_product_id = $data['id'];
        $version->save();

        $product = Product::find($history->product_ID);
        if (!$product) {
            $product = new Product();
        }
        $product->product_ID = $history->product_ID;
        if ($data['type'] == false) {
            $result = json_decode($history->data_old);
        } else {
            $result = json_decode($history->data_new);
        }
        foreach ($result as $key => $value) {
            if (in_array($key, ['images', 'storage'])) {
                switch ($key) {
                    case 'images':
                        ProductImages::where('product_product_ID', $history->product_ID)->delete();
                        if ($value != '') {
                            foreach ($value as $img_id => $img) {
                                $product_image = ProductImages::find($img_id);
                                if (!$product_image) {
                                    $product_image = new ProductImages();
                                }
                                $product_image->image_ID           = $img_id;
                                $product_image->product_product_ID = $history->product_ID;
                                $product_image->is_thumbnail       = $img->is_thumbnail;
                                $product_image->image_url          = $img->image_url;
                                $product_image->source             = $img->source;
                                $product_image->save();
                            }
                        }
                        break;
                    case 'storage':
                        StorageHasProduct::where('product_product_ID', $history->product_ID)->delete();
                        if ($value != '') {
                            foreach ($value as $storageID => $storage_data) {
                                $storage_has_product = StorageHasProduct::find($storageID);
                                if (!$storage_has_product) {
                                    $storage_has_product = new StorageHasProduct();
                                }
                                $storage_has_product->id                 = $storageID;
                                $storage_has_product->storage_storage_ID = $storage_data->storage_storage_ID;
                                $storage_has_product->product_product_ID = $history->product_ID;
                                $storage_has_product->amount             = $storage_data->amount;
                                $storage_has_product->location_ID        = $storage_data->location_ID;
                                $storage_has_product->save();
                            }
                        }
                        break;
                }
            } else {
                $product->$key = $value;
            }
        }
        $product->save();
    }
}