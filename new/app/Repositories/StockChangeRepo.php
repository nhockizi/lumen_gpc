<?php

namespace App\Repositories;

use App\Libraries\Helpers;
use App\Models\StockChange;

class StockChangeRepo extends BaseRepository
{
    public static function checkRandomCode($code)
    {
        return StockChange::where('random_code', $code)->first();
    }

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        // TODO: Implement model() method.
        return StockChange::class;
    }
    public function changeStock ($data, $is_add = 'true', $difference = null , $random_code = null){
        $data = (array) $data;
        if(!$random_code){
            $random_code = $this->generateRandomCode();
        }
        $amount_old = $data['amount_old'] ?? 0;
        $amount_new = $data['amount_new'] ?? 0;

        if (!$difference) {
            if ($amount_old < $amount_new) {
                $difference = 'add';
            } else {
                $difference = 'substracted';
            }
        }
        $attributes           = [
            'stock_change_ID' => $data['id'] ?? null,
        ];
        $values = [
            'time' => date('Y-m-d H:i:s'),
            'amount_old' => $amount_old,
            'amount_new' => $amount_new,
            'difference' => $difference,
            'isAdded' => $is_add,
            'random_code' => $random_code,
        ];
        if(isset($data['product_id'])){
            $values['product_product_ID'] = $data['product_id'];
        }
        if(isset($data['storage_id'])){
            $values['storage_storage_ID'] = $data['storage_id'];
        }
        if(isset($data['bon_id'])){
            $values['bon_id'] = $data['bon_id'];
        }
        if(isset($data['toAmsterdam'])){
            $values['toAmsterdam'] = $data['toAmsterdam'];
        }
        $this->updateOrCreate($attributes,$values);
    }
//    public function changeStock($stockOld, $stockNew, $storageId, $productId, $is_add = 'true', $difference = null , $random_code = null)
//    {
//        $stock = new StockChange();
//        $stock->time = date('Y-m-d H:i:s');
//        $stock->amount_old = $stockOld;
//        $stock->amount_new = $stockNew;
//        if (!$difference) {
//            if ($stockOld < $stockNew) {
//                $stock->difference = 'add';
//            } else {
//                $stock->difference = 'substracted';
//            }
//        } else {
//            $stock->difference = $difference;
//        }
//        $stock->isAdded = $is_add;
//        $stock->product_product_ID = $productId;
//        $stock->storage_storage_ID = $storageId;
//        if(!$random_code){
//            $random_code = $this->generateRandomCode();
//        }
//        $stock->random_code = $random_code;
//        $stock->save();
//    }
    public function generateRandomCode()
    {
        $code = Helpers::getRandomString();
        if (StockChange::where('random_code', $code)->exists()) {
            $code = $this->generateRandomCode();
        }

        return $code;
    }
}