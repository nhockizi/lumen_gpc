<?php

namespace App\Repositories;

use App\Libraries\Helpers;
use App\Models\BonHasProduct;
use App\Models\Brand;
use App\Models\Product;

class BonHasProductRepo extends BaseRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        // TODO: Implement model() method.
        return BonHasProduct::class;
    }
}