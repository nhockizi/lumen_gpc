<?php
namespace App\Providers;
use Illuminate\Support\ServiceProvider;

class MagentoServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('Magento', function () {
            require_once
                MAGENTO_PATH.'app/Mage.php';
            \Mage::app('default');
            \Mage::app()->setCurrentStore(0);
        });
    }
}
