<?php

namespace App\Http\Middleware;

use App\Models\Member;
use App\Services\Response\ResponseFacade;
use App\User;
use Closure;
use Exception;
use Firebase\JWT\ExpiredException;
use Firebase\JWT\JWT;

class JwtMiddleware
{
    public function handle($request, Closure $next, $guard = null)
    {
        $token = $request->bearerToken();
        if (!$token) {
            $msg = 'Your request was made with invalid credentials.';
            return ResponseFacade::send($msg, HTTP_UNAUTHORIZED);
        }
        try {
            $credentials = JWT::decode($token, env('JWT_SECRET'), ['HS256']);
        } catch (ExpiredException $e) {
            $msg = 'Credentials is expired.';
            return ResponseFacade::send($msg, HTTP_UNAUTHORIZED);
        } catch (Exception $e) {
            $msg = 'An error while credentials.';
            return ResponseFacade::send($msg, HTTP_BAD_REQUEST);
        }

        $user = Member::select(
            [
                'member_ID',
                'username',
                'email',
            ]
        )
                      ->where('member_ID', $credentials->sub)
                      ->where('using_api', ACTIVE)
                      ->where('active', ACTIVE)
                      ->first();
        if (!$user) {
            $msg = 'Your request was made with invalid credentials.';
            return ResponseFacade::send($msg, HTTP_UNAUTHORIZED);
        }
        $request->auth = $user;

        return $next($request);
    }
}