<?php

namespace App\Http\Controllers;

use App\Http\Controllers\TraitController\ErrorTrait;
use App\Repositories\ProductTmpRepo;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Validator;

class ProductTmpController extends Controller
{
    protected $_productTmp;
    protected $_request;
    use ErrorTrait;

    public function __construct(
        ProductTmpRepo $productTmpRepo,
        Request $request
    ) {
        $this->_productTmp = $productTmpRepo;
        $this->_request    = $request->all();
    }

    public function getDataByRow()
    {
        $data  = [
            'row' => array_get($this->_request, 'row', null),
        ];
        $rules = [
            'row' => [
                'required',
                'array',
            ],
        ];
        $this->_validate_error($data, $rules, $message);
        if ($message != '') {
            return $this->response(
                $message,
                HTTP_INTERNAL_SERVER_ERROR
            );
        }
        DB::beginTransaction();
        try {
            $result = $this->_productTmp->getDataByRow($data['row']);
            DB::commit();

            return $this->response($result);
        } catch (\Exception $e) {
            DB::rollBack();

            return $this->response($e->getMessage().' in '.$e->getFile().' at '.$e->getLine(), Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * @OA\Get(
     *     path="/product-tmp/list",
     *     summary="List all Product Tmp",
     *     tags={"Product Type"},
     *     operationId="product_tmp_list",
     *     security={{"Bearer":{}}},
     *     @OA\Parameter(
     *         description="page",
     *         in="query",
     *         name="page",
     *         @OA\Schema(
     *           type="integer",
     *           format="int32"
     *         )
     *     ),
     *     @OA\Parameter(
     *         description="number item length",
     *         in="query",
     *         name="length",
     *         @OA\Schema(
     *           type="integer",
     *           format="int32"
     *         )
     *     ),
     *     @OA\Response(
     *          response="200",
     *          description="{
    'status': true,
    'message': null,
    'data': object
    }",
     *     ),
     *     @OA\Response(
     *          response="401",
     *          description="{
    'status': false,
    'message': 'Invalid token',
    'data': []
    }",
     *     ),
     * )
     */
    public function list(Request $request)
    {
        try {
            $data   = [
                'length' => array_get($this->_request, 'length', null),
                'order'  => array_get($this->_request, 'order', null),
                'type'   => array_get($this->_request, 'type', null),
            ];
            $result = $this->_productTmp->list($data);

            return $this->response($result);
        } catch (\Exception $e) {

            return $this->response($e->getMessage(), Response::HTTP_BAD_REQUEST);
        }
    }

    public function create(Request $request)
    {
        $data = [
            'data_row'       => array_get($this->_request, 'data_row', null),
            'name'           => array_get($this->_request, 'name', null),
            'product_type'   => array_get($this->_request, 'product_type', null),
            'brand_parent'   => array_get($this->_request, 'brand_parent', null),
            'brand'          => array_get($this->_request, 'brand', null),
            'model'          => array_get($this->_request, 'model', null),
            'model_children' => array_get($this->_request, 'model_children', null),
            'color'          => array_get($this->_request, 'color', null),
            'extra_field'    => array_get($this->_request, 'extra_field', null),
            'quality_label'  => array_get($this->_request, 'quality_label', null),
            'filter'         => array_get($this->_request, 'filter', null),
            'price_p'        => array_get($this->_request, 'price_p', null),
            'price_s'        => array_get($this->_request, 'price_s', null),
            'amount'         => array_get($this->_request, 'amount', null),
            'rack'           => array_get($this->_request, 'rack', null),
            'row'            => array_get($this->_request, 'row', null),
            'column'         => array_get($this->_request, 'column', null),
            'location'       => array_get($this->_request, 'location', null),
            'images'         => null,
            'images_file'    => array_get($this->_request, 'images_file', null),
            'images_old'     => array_get($this->_request, 'images_old', null),
        ];
        try {
            if ($request->hasFile('images_file')) {
                $images_file = $request->file('images_file');
                foreach ($images_file as $key => $file) {
                    Storage::disk('public')->put('product/tmp', $file);
                    if ($data['images'] == null) {
                        $data['images'] = [];
                    }
                    $data['images'][$key]['image_url'] = $file->getClientOriginalName();
                    $data['images'][$key]['source']    = $file->hashName();
                }
            }
            $result = $this->_productTmp->_insert_data($data);

            return $this->response($result);
        } catch (\Exception $e) {

            return $this->response($e->getMessage(), Response::HTTP_BAD_REQUEST);
        }
    }

    public function save()
    {
        $data  = [
            'row' => array_get($this->_request, 'row', null),
        ];
        $rules = [
            'row' => [
                'required',
                'array',
            ],
        ];
        $this->_validate_error($data, $rules, $message);
        if ($message != '') {
            return $this->response(
                $message,
                HTTP_INTERNAL_SERVER_ERROR
            );
        }
        DB::beginTransaction();
        try {
            $result = $this->_productTmp->save($data['row']);
            DB::commit();

            return $this->response($result);
        } catch (\Exception $e) {
            DB::rollBack();

            return $this->response($e->getMessage().' in '.$e->getFile().' at '.$e->getLine(), Response::HTTP_BAD_REQUEST);
        }
    }

    public function rollback()
    {
        $data  = [
            'row' => array_get($this->_request, 'row', null),
        ];
        $rules = [
            'row' => [
                'required',
                'array',
            ],
        ];
        $this->_validate_error($data, $rules, $message);
        if ($message != '') {
            return $this->response(
                $message,
                HTTP_INTERNAL_SERVER_ERROR
            );
        }
        DB::beginTransaction();
        try {
            $result = $this->_productTmp->rollback($data);
            DB::commit();

            return $this->response($result);
        } catch (\Exception $e) {
            DB::rollBack();

            return $this->response($e->getMessage().' in '.$e->getFile().' at '.$e->getLine(), Response::HTTP_BAD_REQUEST);
        }
    }

    public function delete()
    {
        $data  = [
            'row' => array_get($this->_request, 'row', null),
        ];
        $rules = [
            'row' => [
                'required',
                'array',
            ],
        ];
        $this->_validate_error($data, $rules, $message);
        if ($message != '') {
            return $this->response(
                $message,
                HTTP_INTERNAL_SERVER_ERROR
            );
        }
        DB::beginTransaction();
        try {
            $result = $this->_productTmp->delete($data['row']);
            DB::commit();

            return $this->response($result);
        } catch (\Exception $e) {
            DB::rollBack();

            return $this->response($e->getMessage().' in '.$e->getFile().' at '.$e->getLine(), Response::HTTP_BAD_REQUEST);
        }
    }
}