<?php

namespace App\Http\Controllers;

use App\Http\Controllers\TraitController\ErrorTrait;
use App\Repositories\SaleRepo;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Validator;

class SaleController extends Controller
{
    protected $_sale;
    protected $_request;
    use ErrorTrait;

    public function __construct(
        SaleRepo $saleRepo,
        Request $request
    ) {
        $this->_sale    = $saleRepo;
        $this->_request = $request;
    }

    public function general()
    {
        try {
            $result = $this->_sale->general();

            return $this->response($result);
        } catch (\Exception $e) {

            return $this->response($e->getMessage(), Response::HTTP_BAD_REQUEST);
        }
    }

    public function list(Request $request)
    {
        try {
            $request_all = $request->all();
            $data        = [
                'length' => array_get($request_all, 'length', null),
                //                'filter_search' => array_get($request_all, 'filter_search', null),
                //                'order'         => array_get($request_all, 'order', null),
                //                'type'          => array_get($request_all, 'type', null),
            ];
            $result      = $this->_sale->list($data);

            return $this->response($result);
        } catch (\Exception $e) {

            return $this->response($e->getMessage(), Response::HTTP_BAD_REQUEST);
        }
    }

    public function create()
    {
        $data = $this->_data();
        $this->_validate($data, $message);
        if ($message != '') {
            return $this->response(
                $message,
                Response::HTTP_INTERNAL_SERVER_ERROR
            );
        }
        DB::beginTransaction();
        try {
            $result = $this->_sale->_save($data);
            DB::commit();
            $message = trans('messages.create_success');

            return $this->response($result, Response::HTTP_OK, $message);
        } catch (\Exception $e) {
            DB::rollBack();

            return $this->response($e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    private function _data($id = null)
    {
        $request = $this->_request->all();
        $result  = [
            'id'       => $id,
            'name'     => array_get($request, 'name', null),
            'email'    => array_get($request, 'email', null),
            'phone'    => array_get($request, 'phone', null),
            'status'   => array_get($request, 'status', null),
            'priority' => array_get($request, 'priority', null),
        ];

        return $result;
    }

    private function _validate($data, &$message)
    {
        $rules = [
            'name'     => [
                'required',
            ],
            'phone'    => [
                'required',
            ],
            'status'   => [
                'required',
                'integer',
                'in:'.SALSE_STATUS_PENDING.','.SALSE_STATUS_ENABLED.','.SALSE_STATUS_SUPPENDED,
            ],
            'priority' => [
                'required',
                'integer',
                'in:'.PRIORITY_LOW.','.PRIORITY_MEDIUM.','.PRIORITY_HIGH,
            ],
        ];
        if ($data['id'] == null) {
            $rules['email'] = [
                'required',
                'unique:sales,email',
            ];
        }
        $this->_validate_error($data, $rules, $message);

        return $message;
    }

    public function get($id)
    {
        try {
            $result = $this->_sale->find($id);
            if (!$result) {
                throw new \Exception(trans('messages.data_not_found'));
            }

            return $this->response($result);
        } catch (\Exception $e) {

            return $this->response($e->getMessage(), Response::HTTP_BAD_REQUEST);
        }
    }

    public function update($id)
    {
        $data = $this->_data($id);
        $this->_validate($data, $message);
        if ($message != '') {
            return $this->response(
                $message,
                Response::HTTP_INTERNAL_SERVER_ERROR
            );
        }
        DB::beginTransaction();
        try {
            $result = $this->_sale->_save($data);
            DB::commit();
            $message = trans('messages.update_success');

            return $this->response($result, Response::HTTP_OK, $message);
        } catch (\Exception $e) {
            DB::rollBack();

            return $this->response($e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function delete($id)
    {
        DB::beginTransaction();
        try {
            $result = $this->_sale->find($id);
            if (!$result) {
                throw new \Exception(trans('messages.data_not_found'));
            }
            $result->delete();
            DB::commit();
            $message = trans('messages.delete_success');

            return $this->response($message);
        } catch (\Exception $e) {
            DB::rollBack();

            return $this->response($e->getMessage(), Response::HTTP_BAD_REQUEST);
        }
    }
}