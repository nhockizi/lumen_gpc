<?php

namespace App\Http\Controllers;

use App\Http\Controllers\TraitController\ErrorTrait;
use App\Libraries\Helpers;
use App\Models\RolesPermission;
use App\Repositories\ActionsRoleRepo;
use App\Repositories\RolesControllerRepo;
use App\Repositories\RolesPermissionRepo;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Validator;

class RolesControllerController extends Controller
{
    public $_role_controller;

    public function __construct(
        RolesControllerRepo $rolesControllerRepo
    ) {
        $this->_role_controller = $rolesControllerRepo;
    }

    use ErrorTrait;

    public function list(Request $request)
    {
        try {
            $request_all = $request->all();
            $data        = [
                'length' => array_get($request_all, 'length', null),
                'order'  => array_get($request_all, 'order', null),
                'type'   => array_get($request_all, 'type', null),
            ];
            $result      = $this->_role_controller->list($data);

            return $this->response($result);
        } catch (\Exception $e) {
            return $this->response($e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    private function _validate($data)
    {
        $rules = [
            'name' => [
                'required',
            ],
        ];
        $this->_validate_error($data, $rules, $message);
        return $message;
    }

    private function _data($id = null)
    {
        $request_all = Request::capture()->all();
        $data        = [
            'id'   => $id,
            'name' => array_get($request_all, 'name', null),
        ];
        return $data;
    }

    public function create()
    {
        $data    = $this->_data();
        $message = $this->_validate($data);
        if ($message != '') {
            return $this->response(
                $message,
                HTTP_INTERNAL_SERVER_ERROR
            );
        }
        try {
            $result  = $this->_role_controller->create($data);
            $message = trans('messages.create_success');
            return $this->response($result, HTTP_OK, $message);
        } catch (\Exception $e) {
            return $this->response($e->getMessage(), HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function update($id)
    {
        $check = $this->_role_controller->find($id);
        if (!$check) {
            return $this->response(trans('data_not_found'), HTTP_BAD_REQUEST);
        }
        $data = Request::capture()->all();
        try {
            $_rolesPermission = RolesPermissionRepo::getInstance();
            RolesPermission::where('roles_controller_id', $id)->delete();
            foreach ($data as $item) {
                foreach ($item['function'] as $function) {
                    if ($function['checked'] == true) {
                        $dataInsert = [
                            'roles_id'            => $item['id'],
                            'roles_controller_id' => $id,
                            'roles_function_id'   => $function['id']
                        ];
                        $_rolesPermission->create($dataInsert);
                    }
                }
            }
            $message = trans('messages.update_success');
            return $this->response($message);
        } catch (\Exception $e) {
            return $this->response($e->getMessage(), HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function get($roleID)
    {
        try {
            $result = $this->_role_controller->find($roleID);
            if (!$result) {
                return $this->response(trans('data_not_found'), HTTP_BAD_REQUEST);
            }
            $result = $this->_role_controller->_detail($roleID);
            return $this->response($result);
        } catch (\Exception $e) {

            return $this->response($e->getMessage(), HTTP_BAD_REQUEST);
        }
    }

    public function listRoles($id)
    {
        try {
            $result = $this->_role_controller->listRoles($id);

            return $this->response($result);
        } catch (\Exception $e) {
            return $this->response($e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

}