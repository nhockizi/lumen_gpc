<?php

namespace App\Http\Controllers;

use App\Http\Controllers\TraitController\ErrorTrait;
use App\Libraries\Helpers;
use App\Models\Product;
use App\Repositories\ProductRepo;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Maatwebsite\Excel\Facades\Excel;
use Validator;

class ProductController extends Controller
{
    use ErrorTrait;
    protected $_product;

    public function __construct(
        ProductRepo $productRepo
    ) {
        $this->_product = $productRepo;
    }

    /**
     * @OA\Get(
     *     path="/product/list",
     *     summary="List all Product",
     *     tags={"Product"},
     *     operationId="product_list",
     *     security={{"Bearer":{}}},
     *     @OA\Parameter(
     *         description="page",
     *         in="query",
     *         name="page",
     *         @OA\Schema(
     *           type="integer",
     *           format="int32"
     *         )
     *     ),
     *     @OA\Parameter(
     *         description="number item length",
     *         in="query",
     *         name="length",
     *         @OA\Schema(
     *           type="integer",
     *           format="int32"
     *         )
     *     ),
     *     @OA\Parameter(
     *         description="Brand id",
     *         in="query",
     *         name="brand_id",
     *         @OA\Schema(
     *           type="string",
     *         )
     *     ),
     *     @OA\Parameter(
     *         description="Product type id",
     *         in="query",
     *         name="product_type_id",
     *         @OA\Schema(
     *           type="string",
     *         )
     *     ),
     *     @OA\Parameter(
     *         description="Group brand id",
     *         in="query",
     *         name="group_brand_id[]",
     *          @OA\Schema(
     *               type="array",
     *             @OA\Items(type="string"),
     *         )
     *     ),
     * @OA\Parameter(
     *         description="Model parent id",
     *         in="query",
     *         name="model_parent_id",
     *         @OA\Schema(
     *           type="string",
     *         )
     *     ),
     * @OA\Parameter(
     *         description="Model id",
     *         in="query",
     *         name="model_id",
     *         @OA\Schema(
     *           type="string",
     *         )
     *     ),
     * @OA\Parameter(
     *         description="Name or Description or Sku",
     *         in="query",
     *         name="filter_search",
     *         @OA\Schema(
     *           type="string",
     *         )
     *     ),
     * @OA\Parameter(
     *         description="Name",
     *         in="query",
     *         name="product_name",
     *         @OA\Schema(
     *           type="string",
     *         )
     *     ),
     * @OA\Parameter(
     *         description="Order by",
     *         in="query",
     *         name="order",
     *         @OA\Schema(
     *           type="string",
     *         )
     *     ),
     * @OA\Parameter(
     *         description="Order type",
     *         in="query",
     *         name="type",
     *         @OA\Schema(
     *           type="string",
     *         )
     *     ),
     * @OA\Response(
     *          response="200",
     *          description="{
    'status': true,
    'message': null,
    'data': object
    }",
     *     ),
     * @OA\Response(
     *          response="401",
     *          description="{
    'status': false,
    'message': 'Invalid token',
    'data': []
    }",
     *     ),
     * )
     */
    public function list(Request $request)
    {
        try {
            $request_all = $request->all();
            $data        = [
                'length'         => array_get($request_all, 'length', null),
                'filter_search'  => array_get($request_all, 'filter_search', null),
                'order'          => array_get($request_all, 'order', null),
                'type'           => array_get($request_all, 'type', null),
                'group_brand_id' => array_get($request_all, 'group_brand_id', []),
            ];
            $result      = $this->_product->list($data);

            return $this->response($result);
        } catch (\Exception $e) {

            return $this->response($e->getMessage(), Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * @OA\Get(
     *     path="/product/detail-by-sku/{sku}",
     *     summary="Detail Product",
     *     tags={"Product"},
     *     operationId="product_detail",
     *     security={{"Bearer":{}}},
     *     @OA\Parameter(
     *         description="Sku",
     *         in="path",
     *         name="sku",
     *         @OA\Schema(
     *           type="string",
     *         )
     *     ),
     *     @OA\Response(
     *          response="200",
     *          description="{
    'status': true,
    'message': null,
    'data': object
    }",
     *     ),
     *     @OA\Response(
     *          response="401",
     *          description="{
    'status': false,
    'message': 'Invalid token',
    'data': []
    }",
     *     ),
     * )
     */
    public function getBySku($sku)
    {
        $product = Product::where('sku', $sku)->first();
        if (!isset($product->product_ID)) {
            throw new \Exception(trans('messages.data_not_found'));
        }

        return $this->get($product->product_ID);
    }

    /**
     * @OA\Get(
     *     path="/product/detail-by-list-sku",
     *     summary="Detail Product By List Sku",
     *     tags={"Product"},
     *     operationId="product_getByListSku",
     *     security={{"Bearer":{}}},
     *     @OA\Parameter(
     *         description="Sku",
     *         in="query",
     *         name="sku[]",
     *         @OA\Schema(
     *           type="array",
     *           @OA\Items(type="string"),
     *         )
     *     ),
     *     @OA\Response(
     *          response="200",
     *          description="{
    'status': true,
    'message': null,
    'data': object
    }",
     *     ),
     *     @OA\Response(
     *          response="401",
     *          description="{
    'status': false,
    'message': 'Invalid token',
    'data': []
    }",
     *     ),
     * )
     */
    public function getByListSku(Request $request)
    {
        try {
            $request_all = $request->all();
            $sku         = array_get($request_all, 'sku', []);
            if(!is_array($sku)){
                throw new \Exception(trans('sku must be array'));
            }
            $result      = $this->_product->getByListSku($sku);

            return $this->response($result);
        } catch (\Exception $e) {

            return $this->response($e->getMessage(), Response::HTTP_BAD_REQUEST);
        }
    }
    /**
     * @OA\Get(
     *     path="/product/detail-by-list-id",
     *     summary="Detail Product By List ID",
     *     tags={"Product"},
     *     operationId="product_getByListId",
     *     security={{"Bearer":{}}},
     *     @OA\Parameter(
     *         description="id",
     *         in="query",
     *         name="id[]",
     *         @OA\Schema(
     *           type="array",
     *           @OA\Items(type="int"),
     *         )
     *     ),
     *     @OA\Response(
     *          response="200",
     *          description="{
    'status': true,
    'message': null,
    'data': object
    }",
     *     ),
     *     @OA\Response(
     *          response="401",
     *          description="{
    'status': false,
    'message': 'Invalid token',
    'data': []
    }",
     *     ),
     * )
     */
    public function getByListId(Request $request)
    {
        try {
            $request_all = $request->all();
            $ids         = array_get($request_all, 'id', []);
            if(!is_array($ids)){
                throw new \Exception(trans('id must be array'));
            }
            $result      = $this->_product->getByListId($ids);

            return $this->response($result);
        } catch (\Exception $e) {

            return $this->response($e->getMessage(), Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * @OA\Get(
     *     path="/product/detail/{product_id}",
     *     summary="Detail Product",
     *     tags={"Product"},
     *     operationId="product_detail",
     *     security={{"Bearer":{}}},
     *     @OA\Parameter(
     *         description="Product id",
     *         in="path",
     *         name="product_id",
     *         @OA\Schema(
     *           type="integer",
     *           format="int32"
     *         )
     *     ),
     *     @OA\Response(
     *          response="200",
     *          description="{
    'status': true,
    'message': null,
    'data': object
    }",
     *     ),
     *     @OA\Response(
     *          response="401",
     *          description="{
    'status': false,
    'message': 'Invalid token',
    'data': []
    }",
     *     ),
     * )
     */
    public function get($productID)
    {
        try {
            $result = $this->_product->detail($productID);
            if (!$result) {
                throw new \Exception(trans('messages.data_not_found'));
            }

            return $this->response($result);
        } catch (\Exception $e) {

            return $this->response($e->getMessage(), Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * @OA\Get(
     *     path="/product/max",
     *     summary="Get Max Product Id",
     *     tags={"Product"},
     *     operationId="product_maxid",
     *     security={{"Bearer":{}}},
     *     @OA\Response(
     *          response="200",
     *          description="{
    'status': true,
    'message': null,
    'data': object
    }",
     *     ),
     *     @OA\Response(
     *          response="401",
     *          description="{
    'status': false,
    'message': 'Invalid token',
    'data': []
    }",
     *     ),
     * )
     */
    public function maxId()
    {
        try {
            return $this->response(
                [
                    'product_id' => $this->_product->getNewProduct(),
                ]
            );
        } catch (\Exception $e) {

            return $this->response($e->getMessage(), Response::HTTP_BAD_REQUEST);
        }
    }

    public function create(Request $request)
    {
        $request_all = $request->all();
        $data        = [
            'product_id'           => $this->_product->getNewProduct(),
            'product_group'        => array_get($request_all, 'product_group', null),
            'product_type'         => array_get($request_all, 'product_type', null),
            'brand'                => array_get($request_all, 'brand', null),
            'brand_parent'         => array_get($request_all, 'brand_parent', null),
            'model'                => array_get($request_all, 'model', null),
            'model_child'          => array_get($request_all, 'model_child', null),
            'quality'              => array_get($request_all, 'quality', null),
            'product_name'         => array_get($request_all, 'product_name', null),
            'product_name_en'      => array_get($request_all, 'product_name_en', null),
            'description'          => array_get($request_all, 'description', null),
            'description_en'       => array_get($request_all, 'description_en', null),
            'extra_product_code'   => array_get($request_all, 'extra_product_code', null),
            'extra_model_children' => array_get($request_all, 'extra_model_children', null),
            'product_color'        => array_get($request_all, 'product_color', null),
            'extra_field'          => array_get($request_all, 'extra_field', null),
            'price_purchased'      => array_get($request_all, 'price_purchased', null),
            'sell_price'           => array_get($request_all, 'sell_price', null),
            'storage'              => array_get($request_all, 'storage', null),
            'images'               => array_get($request_all, 'images', null),
        ];
        $this->_validate($data, $message);
        if ($message != '') {
            return $this->response(
                $message,
                HTTP_INTERNAL_SERVER_ERROR
            );
        }
        if ($request->hasFile('images_file')) {
            $images_file = $request->file('images_file');
            foreach ($images_file as $key => $file) {
                Storage::disk('public')->put('product/tmp', $file);
                if ($data['images'] == null) {
                    $data['images'] = [];
                }
                $data['images'][$key]['image_url'] = $file->getClientOriginalName();
                $data['images'][$key]['source']    = $file->hashName();
            }
        } else {
            $data['images'] = null;
        }
        DB::beginTransaction();
        try {
            $result = $this->_product->_save($data);
            DB::commit();
            $message = trans('messages.create_success');

            return $this->response($result, HTTP_OK, $message);
        } catch (\Exception $e) {
            DB::rollBack();

            return $this->response($e->getMessage(), HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    private function _validate($data, &$message)
    {
        $rules = [
            'brand'        => [
                'required',
                'exists:brand,brand_ID',
            ],
            'model'        => [
                'required',
                'exists:model,model_ID',
            ],
            'product_name' => [
                'required',
            ],
            'sell_price'   => [
                'required',
                'numeric',
            ],
            'quality'      => [
                'required',
            ],
        ];
        if ($data['price_purchased'] != null) {
            $price_purchased = [
                'required',
                'numeric',
            ];
            $rules           = array_merge($rules, compact('price_purchased'));
        }
        $this->_validate_error($data, $rules, $message);

        return $message;
    }

    public function updateRow($id, Request $request)
    {
        $data  = $request->all();
        $data  = [
            'product_id'          => $id,
            'name'                => array_get($data, 'name', null),
            'price'               => array_get($data, 'price', null),
            'storage_has_product' => array_get($data, 'storage_has_product', null),
            'group_brand_id'      => array_get($data, 'brand_group', null),
        ];
        $rules = [
            'name'  => [
                'required',
            ],
            'price' => [
                'required',
                'numeric',
            ],
        ];
        $this->_validate_error($data, $rules, $message);
        if ($message != '') {
            return $this->response(
                $message,
                HTTP_INTERNAL_SERVER_ERROR
            );
        }
        DB::beginTransaction();
        try {
            $data_update = [
                'name' => $data['name'],
            ];
            foreach ($data['group_brand_id'] as $group_brand_id) {
                $is_check = $group_brand_id['is_check'] ?? false;
                if ($is_check == true) {
                    $data_update['group_brand_id'][] = $group_brand_id['id'];
                }
            }
            $data_update['group_brand_id'] = null;
            if (isset($data_update['group_brand_id'])) {
                $data_update['group_brand_id'] = json_encode($data_update['group_brand_id']);
            }
            $result = $this->_product->update($data_update, $id);

            $storageProduct = [
                'storage' => $data['storage_has_product'],
            ];
            $this->_product->_saveStorageProduct($storageProduct, $id);

            DB::commit();
            $message = trans('messages.update_success');

            return $this->response($result, HTTP_OK, $message);
        } catch (\Exception $e) {
            DB::rollBack();

            return $this->response($e->getMessage(), HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function update($id, Request $request)
    {
        $request_all = $request->all();
        $data        = [
            'product_id'           => $id,
            'product_group'        => array_get($request_all, 'product_group', null),
            'product_type'         => array_get($request_all, 'product_type', null),
            'brand'                => array_get($request_all, 'brand', null),
            'brand_parent'         => array_get($request_all, 'brand_parent', null),
            'model'                => array_get($request_all, 'model', null),
            'model_child'          => array_get($request_all, 'model_child', null),
            'quality'              => array_get($request_all, 'quality', null),
            'product_name'         => array_get($request_all, 'product_name', null),
            'product_name_en'      => array_get($request_all, 'product_name_en', null),
            'description'          => array_get($request_all, 'description', null),
            'description_en'       => array_get($request_all, 'description_en', null),
            'extra_product_code'   => array_get($request_all, 'extra_product_code', null),
            'extra_model_children' => array_get($request_all, 'extra_model_children', null),
            'product_color'        => array_get($request_all, 'product_color', null),
            'extra_field'          => array_get($request_all, 'extra_field', null),
            'price_purchased'      => array_get($request_all, 'price_purchased', null),
            'sell_price'           => array_get($request_all, 'sell_price', null),
            'storage'              => array_get($request_all, 'storage', null),
            'images'               => array_get($request_all, 'images', null),
        ];
        $message     = '';
        $this->_validate($data, $message);
        if ($message != '') {
            return $this->response(
                $message,
                HTTP_INTERNAL_SERVER_ERROR
            );
        }
        $index = -1;
        if (isset($data['images'])) {
            foreach ($data['images'] as $key => $image) {
                if ($image['id'] == null) {
                    $index++;
                    if ($image['deleted'] == false) {
                        $images_file = $request->file('images_file');
                        Storage::disk('public')->put('product/tmp', $images_file[$index]);
                        $data['images'][$key]['image_url'] = $images_file[$index]->getClientOriginalName();
                        $data['images'][$key]['source']    = $images_file[$index]->hashName();
                    }
                }
            }
        }
        DB::beginTransaction();
        try {
            $result = $this->_product->_save($data, 'update');
            DB::commit();
            $message = trans('messages.update_success');

            return $this->response($result, HTTP_OK, $message);
        } catch (\Exception $e) {
            DB::rollBack();

            return $this->response($e->getMessage(), HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function getQuantityNotification($id)
    {
        try {
            $result['quantity'] = $this->_product->getQuantityNotification($id);

            return $this->response($result);
        } catch (\Exception $e) {
            return $this->response($e->getMessage(), HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function updateQuantityNotification(Request $request)
    {
        $request_all = $request->all();
        $data_check  = [
            'id'       => array_get($request_all, 'id', null),
            'quantity' => array_get($request_all, 'quantity', null),
        ];
        $rules       = [
            'id' => [
                'required',
                'exists:product,product_ID',
            ],
        ];
        $this->_validate_error($data_check, $rules, $message);
        if ($message != '') {
            return $this->response(
                $message,
                HTTP_INTERNAL_SERVER_ERROR
            );
        }
        DB::beginTransaction();
        try {
            $result = $this->_product->_saveNotificationQuantity($data_check);
            DB::commit();
            $message = trans('messages.update_success');

            return $this->response($result, HTTP_OK, $message);
        } catch (\Exception $e) {
            DB::rollBack();

            return $this->response($e->getMessage(), HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @OA\Get(
     *     path="/product/export-list-almost-out-of-stock",
     *     summary="Download list product Almost Out Of ",
     *     tags={"Product"},
     *     operationId="product_exportList",
     *     security={{"Bearer":{}}},
     *     @OA\Response(
     *          response="200",
     *          description="{download file}",
     *     ),
     *     @OA\Response(
     *          response="401",
     *          description="{
    'status': false,
    'message': 'Invalid token',
    'data': []
    }",
     *     ),
     * )
     */
    public function exportListAlmostOutOfStock()
    {
        try {
            return Excel::create(
                'list_of_products_almost_out_of_stock', function ($excel) {
                $excel->setTitle('Our new awesome title');
                $excel->sheet(
                    'sheet 1', function ($sheet) {
                    $product = $this->_product->getAllOutOfStock(OUT_OF_STOCK);
                    $sheet->loadView('product.list_almost_out_of_stock', ['product' => $product]);
                }
                );
            }
            )->download('xlsx', ['Access-Control-Allow-Origin' => '*']);
        } catch (\Exception $e) {
            return $this->response($e->getMessage(), HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @OA\Get(
     *     path="/product/export-price-list",
     *     summary="Download list price product",
     *     tags={"Product"},
     *     operationId="product_exportPriceList",
     *     security={{"Bearer":{}}},
     *     @OA\Response(
     *          response="200",
     *          description="{download file}",
     *     ),
     *     @OA\Response(
     *          response="401",
     *          description="{
    'status': false,
    'message': 'Invalid token',
    'data': []
    }",
     *     ),
     * )
     */
    public function exportPriceList()
    {
        try {
            return Excel::create(
                'list_price_product', function ($excel) {
                $excel->setTitle('Our new awesome title');
                $excel->sheet(
                    'sheet 1', function ($sheet) {
                    $product = $this->_product->exportPriceList();
                    $sheet->loadView('product.list_price', ['product' => $product]);
                }
                );
            }
            )->download('xlsx', ['Access-Control-Allow-Origin' => '*']);
        } catch (\Exception $e) {
            return $this->response($e->getMessage(), HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @OA\Get(
     *     path="/product/get-stock-change/{product_id}",
     *     summary="Get Stock Change By Product",
     *     tags={"Product"},
     *     operationId="product_getStockChange",
     *     security={{"Bearer":{}}},
     *     @OA\Parameter(
     *         description="Product id",
     *         in="path",
     *         name="product_id",
     *         @OA\Schema(
     *           type="integer",
     *           format="int32"
     *         )
     *     ),
     *     @OA\Response(
     *          response="200",
     *          description="{
    'status': true,
    'message': null,
    'data': object
    }",
     *     ),
     *     @OA\Response(
     *          response="401",
     *          description="{
    'status': false,
    'message': 'Invalid token',
    'data': []
    }",
     *     ),
     * )
     */
    public function getStockChange($productID)
    {
        try {
            $result = $this->_product->getStockChange($productID);
            if ($result) {
                return $this->response($result);
            } else {
                throw new \Exception(trans('messages.data_not_found'));
            }
        } catch (\Exception $e) {
            return $this->response($e->getMessage(), HTTP_BAD_REQUEST);
        }
    }

    /**
     * @OA\Post(
     *     path="/product/synchronize",
     *     summary="Get Stock Change By Product",
     *     tags={"Product"},
     *     operationId="product_getStockChange",
     *     security={{"Bearer":{}}},
     *     @OA\Parameter(
     *         description="Product id",
     *         in="path",
     *         name="id",
     *         @OA\Schema(
     *           type="integer",
     *           format="int32"
     *         )
     *     ),
     *     @OA\Response(
     *          response="200",
     *          description="{
    'status': true,
    'message': null,
    'data': object
    }",
     *     ),
     *     @OA\Response(
     *          response="401",
     *          description="{
    'status': false,
    'message': 'Invalid token',
    'data': []
    }",
     *     ),
     * )
     */
    public function synchronize(Request $request)
    {
        $request_all = $request->all();
        $data        = [
            'id' => array_get($request_all, 'id', null),
        ];
        $rules       = [
            'id' => [
                'required',
                'array',
            ],
        ];
        $this->_validate_error($data, $rules, $message);
        if ($message != '') {
            return $this->response(
                $message,
                HTTP_INTERNAL_SERVER_ERROR
            );
        }
        try {
            $this->_product->synchronize($data);

            return $this->response(trans('messages.sync_success'));
        } catch (\Exception $e) {
            return $this->response($e->getMessage(), HTTP_BAD_REQUEST);
        }
    }
    /**
     * @OA\Get(
     *     path="/product/synchronize-images",
     *     summary="Synchronize Images Product",
     *     tags={"Product"},
     *     operationId="product_synchronizeImages",
     *     security={{"Bearer":{}}},
     *     @OA\Response(
     *          response="200",
     *          description="{
    'status': true,
    'message': null,
    'data': object
    }",
     *     ),
     *     @OA\Response(
     *          response="401",
     *          description="{
    'status': false,
    'message': 'Invalid token',
    'data': []
    }",
     *     ),
     * )
     */
    /**
     * @OA\Get(
     *     path="/product/synchronize-images/{product_id}",
     *     summary="Synchronize Images Product",
     *     tags={"Product"},
     *     operationId="product_synchronizeImagesID",
     *     security={{"Bearer":{}}},
     *     @OA\Parameter(
     *         description="Product id",
     *         in="path",
     *         name="product_id",
     *         @OA\Schema(
     *           type="integer",
     *           format="int32"
     *         )
     *     ),
     *     @OA\Response(
     *          response="200",
     *          description="{
    'status': true,
    'message': null,
    'data': object
    }",
     *     ),
     *     @OA\Response(
     *          response="401",
     *          description="{
    'status': false,
    'message': 'Invalid token',
    'data': []
    }",
     *     ),
     * )
     */
    public function synchronizeImages($id = null)
    {
        try {
            $message = trans('messages.sync_success');
            if ($id != null) {
                $product = $this->_product->find($id);
                if ($product) {
                    $this->_product->synchronizeImages($id);

                    return $this->response($product, HTTP_OK, $message);
                } else {
                    throw new \Exception(trans('messages.data_not_found'));
                }
            } else {
                return $this->response($message);
            }
        } catch (\Exception $e) {
            return $this->response($e->getMessage(), HTTP_BAD_REQUEST);
        }
    }

    /**
     * @OA\Get(
     *     path="/product/delete/{product_id}",
     *     summary="Delete Product",
     *     tags={"Product"},
     *     operationId="product_delete",
     *     security={{"Bearer":{}}},
     *     @OA\Parameter(
     *         description="Product id",
     *         in="path",
     *         name="product_id",
     *         @OA\Schema(
     *           type="integer",
     *           format="int32"
     *         )
     *     ),
     *     @OA\Response(
     *          response="200",
     *          description="{
    'status': true,
    'message': null,
    'data': object
    }",
     *     ),
     *     @OA\Response(
     *          response="401",
     *          description="{
    'status': false,
    'message': 'Invalid token',
    'data': []
    }",
     *     ),
     * )
     */
    public function delete($id)
    {
        DB::beginTransaction();
        try {
            $product = $this->_product->find($id);
            if ($product) {
                $sku    = $product->sku;
                $list   = [
                    [
                        'sku',
                        'magmi:delete',
                    ],
                ];
                $list[] = [
                    $sku,
                    '1',
                ];

                $delete_file = PATH_IMPORT_CSV.'sync_delete.csv';
                if (file_exists($delete_file)) {
                    unlink($delete_file);
                }
                $fp = fopen($delete_file, 'w');
                foreach ($list as $fields) {
                    fputcsv($fp, $fields);
                }
                fclose($fp);

                $product->delete();

                /*
                 * run sh sync product
                 */
                $_sh_path = PATH_SHELL_FILE.'product/cli_delete_product.sh';
                exec('sh '.$_sh_path);
            } else {
                throw new \Exception(trans('messages.data_not_found'));
            }
            DB::commit();
            $message = trans('messages.delete_success');

            return $this->response($message);
        } catch (\Exception $e) {
            DB::rollBack();

            return $this->response($e->getMessage(), HTTP_BAD_REQUEST);
        }
    }

    public function synchronizeUpdateProduct($sku)
    {
        try {
            $check = Product::where('sku', $sku)->exists();
            if ($check) {
                $this->_product->synchronizeUpdateProduct($sku);
            } else {
                throw new \Exception(trans('messages.data_not_found'));
            }
            $message = trans('messages.sync_success');

            return $this->response($message);
        } catch (\Exception $e) {
            return $this->response($e->getMessage(), HTTP_BAD_REQUEST);
        }
    }

    /**
     * @OA\Post(
     *     path="/product/get-price-by-sku",
     *     summary="Get Price By Sku",
     *     tags={"Product"},
     *     operationId="product_getPriceBySku",
     *     security={{"Bearer":{}}},
     *     @OA\Parameter(
     *         description="Sku",
     *         in="query",
     *         name="sku[]",
     *         @OA\Schema(
     *           type="array",
     *           @OA\Items(type="string"),
     *         )
     *     ),
     *     @OA\Response(
     *          response="200",
     *          description="{
    'status': true,
    'message': null,
    'data': object
    }",
     *     ),
     *     @OA\Response(
     *          response="401",
     *          description="{
    'status': false,
    'message': 'Invalid token',
    'data': []
    }",
     *     ),
     * )
     */
    public function getPriceBySku(Request $request)
    {
        try {
            $request_all = $request->all();
            $data        = [
                'sku' => array_get($request_all, 'sku', null),
            ];
            $result      = $this->_product->getPriceBySku($data);

            return $this->response($result);
        } catch (\Exception $e) {
            return $this->response($e->getMessage(), HTTP_BAD_REQUEST);
        }
    }

    /**
     * @OA\Post(
     *     path="/product/get-stock-amount-by-sku",
     *     summary="Get Stock Amount By Sku",
     *     tags={"Product"},
     *     operationId="product_getStockAmountBySku",
     *     security={{"Bearer":{}}},
     *     @OA\Parameter(
     *         description="Location ID",
     *         in="query",
     *         name="location",
     *         @OA\Schema(
     *           type="integer",
     *           format="int32"
     *         )
     *     ),
     *     @OA\Parameter(
     *         description="Sku",
     *         in="query",
     *         name="sku[]",
     *         @OA\Schema(
     *           type="array",
     *           @OA\Items(type="string"),
     *         )
     *     ),
     *     @OA\Response(
     *          response="200",
     *          description="{
    'status': true,
    'message': null,
    'data': object
    }",
     *     ),
     *     @OA\Response(
     *          response="401",
     *          description="{
    'status': false,
    'message': 'Invalid token',
    'data': []
    }",
     *     ),
     * )
     */
    public function getStockAmountBySku(Request $request)
    {
        try {
            $request_all = $request->all();
            $data        = [
                'sku'      => array_get($request_all, 'sku', null),
                'location' => array_get($request_all, 'location', null),
            ];
            $result      = $this->_product->getStockAmountBySku($data);

            return $this->response($result);
        } catch (\Exception $e) {
            return $this->response($e->getMessage(), HTTP_BAD_REQUEST);
        }
    }


    public function listDataTable(Request $request)
    {
        try {
            $request_all = $request->all();
            $data        = [
                'draw'    => array_get($request_all, 'draw', 1),
                'columns' => array_get($request_all, 'columns', null),
                'order'   => array_get($request_all, 'order', null),
                'start'   => array_get($request_all, 'start', null),
                'length'  => array_get($request_all, 'length', null),
                'search'  => array_get($request_all, 'search', null),
            ];
            $result      = $this->_product->listDataTable($data);
            Helpers::saveLog('product', 'search : '.json_encode($data), $request->auth->member_ID);

            return json_encode($result);
        } catch (\Exception $e) {

            return $this->response($e->getMessage(), HTTP_BAD_REQUEST);
        }
    }

    public function listSingle(Request $request)
    {
        try {
            $request_all = $request->all();
            $data        = [
                'name'          => array_get($request_all, 'name', null),
                'length'        => array_get($request_all, 'length', null),
                'filter_search' => array_get($request_all, 'filter_search', null),
                'order'         => array_get($request_all, 'order', null),
                'type'          => array_get($request_all, 'type', null),
            ];
            $result      = $this->_product->listSingle($data);

            return $this->response($result);
        } catch (\Exception $e) {

            return $this->response($e->getMessage(), HTTP_BAD_REQUEST);
        }
    }

    /** Antom */

    /**
     * @OA\Get(
     *     path="/product/list-not-ready",
     *     summary="List all Product Not Ready",
     *     tags={"Product"},
     *     operationId="product_listNotReady",
     *     security={{"Bearer":{}}},
     *     @OA\Parameter(
     *         description="page",
     *         in="query",
     *         name="page",
     *         @OA\Schema(
     *           type="integer",
     *           format="int32"
     *         )
     *     ),
     *     @OA\Parameter(
     *         description="number item length",
     *         in="query",
     *         name="length",
     *         @OA\Schema(
     *           type="integer",
     *           format="int32"
     *         )
     *     ),
     *     @OA\Parameter(
     *         description="Brand id",
     *         in="query",
     *         name="brand_id",
     *         @OA\Schema(
     *           type="string",
     *         )
     *     ),
     *     @OA\Parameter(
     *         description="Product type id",
     *         in="query",
     *         name="product_type_id",
     *         @OA\Schema(
     *           type="string",
     *         )
     *     ),
     *     @OA\Parameter(
     *         description="Group brand id",
     *         in="query",
     *         name="group_brand_id",
     *         @OA\Schema(
     *           type="string",
     *         )
     *     ),
     *     @OA\Parameter(
     *         description="Model parent id",
     *         in="query",
     *         name="model_parent_id",
     *         @OA\Schema(
     *           type="string",
     *         )
     *     ),
     *     @OA\Parameter(
     *         description="Model id",
     *         in="query",
     *         name="model_id",
     *         @OA\Schema(
     *           type="string",
     *         )
     *     ),
     *     @OA\Parameter(
     *         description="Name or Description or Sku",
     *         in="query",
     *         name="filter_search",
     *         @OA\Schema(
     *           type="string",
     *         )
     *     ),
     *     @OA\Parameter(
     *         description="Name",
     *         in="query",
     *         name="product_name",
     *         @OA\Schema(
     *           type="string",
     *         )
     *     ),
     *     @OA\Parameter(
     *         description="Order by",
     *         in="query",
     *         name="order",
     *         @OA\Schema(
     *           type="string",
     *         )
     *     ),
     *     @OA\Parameter(
     *         description="Order type",
     *         in="query",
     *         name="type",
     *         @OA\Schema(
     *           type="string",
     *         )
     *     ),
     *     @OA\Response(
     *          response="200",
     *          description="{
    'status': true,
    'message': null,
    'data': object
    }",
     *     ),
     *     @OA\Response(
     *          response="401",
     *          description="{
    'status': false,
    'message': 'Invalid token',
    'data': []
    }",
     *     ),
     * )
     */
    public function listTmp(Request $request)
    {
        try {
            $request_all = $request->all();
            $data        = [
                'length'        => array_get($request_all, 'length', null),
                'filter_search' => array_get($request_all, 'filter_search', null),
                'order'         => array_get($request_all, 'order', null),
                'type'          => array_get($request_all, 'type', null),
            ];
            $result      = $this->_product->list($data, false);

            return $this->response($result);
        } catch (\Exception $e) {

            return $this->response($e->getMessage(), Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * @OA\Post(
     *     path="/product/create-not-ready",
     *     summary="Create Product Not Ready",
     *     tags={"Product"},
     *     operationId="product_createTMP",
     *     security={{"Bearer":{}}},
     *     @OA\Parameter(
     *         in="query",
     *         name="name",
     *         @OA\Schema(
     *           type="string",
     *         )
     *     ),
     *     @OA\Parameter(
     *         in="query",
     *         name="price",
     *         @OA\Schema(
     *           type="numeric",
     *         )
     *     ),
     *     @OA\Response(
     *          response="200",
     *          description="{
    'status': true,
    'message': null,
    'data': object
    }",
     *     ),
     *     @OA\Response(
     *          response="401",
     *          description="{
    'status': false,
    'message': 'Invalid token',
    'data': []
    }",
     *     ),
     * )
     */
    public function createTmp(Request $request)
    {
        $request_all = $request->all();
        $data        = [
            'name'     => array_get($request_all, 'name', null),
            'price'    => array_get($request_all, 'price', null),
            'is_ready' => false,
        ];
        $this->_validateTmp($data, $message);
        if ($message != '') {
            return $this->response(
                $message,
                Response::HTTP_INTERNAL_SERVER_ERROR
            );
        }
        DB::beginTransaction();
        try {
            $result = $this->_product->create($data);
            DB::commit();
            $message = trans('messages.create_success');

            return $this->response($result, Response::HTTP_OK, $message);
        } catch (\Exception $e) {
            DB::rollBack();

            return $this->response($e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    private function _validateTmp($data, &$message)
    {
        $rules = [
            'name'  => [
                'required',
            ],
            'price' => [
                'required',
                'numeric',
            ],
        ];
        $this->_validate_error($data, $rules, $message);
    }

    /**
     * @OA\Post(
     *     path="/product/update-not-ready/{id}",
     *     summary="Update Product Not Ready",
     *     tags={"Product"},
     *     operationId="product_updateTMP",
     *     security={{"Bearer":{}}},
     *     @OA\Parameter(
     *         in="query",
     *         name="id",
     *         @OA\Schema(
     *           type="number",
     *         )
     *     ),
     *     @OA\Parameter(
     *         in="query",
     *         name="name",
     *         @OA\Schema(
     *           type="string",
     *         )
     *     ),
     *     @OA\Parameter(
     *         in="query",
     *         name="price",
     *         @OA\Schema(
     *           type="numeric",
     *         )
     *     ),
     *     @OA\Response(
     *          response="200",
     *          description="{
    'status': true,
    'message': null,
    'data': object
    }",
     *     ),
     *     @OA\Response(
     *          response="401",
     *          description="{
    'status': false,
    'message': 'Invalid token',
    'data': []
    }",
     *     ),
     * )
     */
    public function updateTmp($id, Request $request)
    {
        $request_all = $request->all();
        $data        = [
            'name'  => array_get($request_all, 'name', null),
            'price' => array_get($request_all, 'price', null),
        ];
        $this->_validateTmp($data, $message);
        if ($message != '') {
            return $this->response(
                $message,
                Response::HTTP_INTERNAL_SERVER_ERROR
            );
        }
        $check = Product::where('product_ID', $id)
                        ->where('is_ready', false)
                        ->exists();
        if (!$check) {
            return $this->response(trans('messages.data_not_found'), HTTP_INTERNAL_SERVER_ERROR);
        }
        DB::beginTransaction();
        try {
            $result = $this->_product->update($data, $id);
            DB::commit();
            $message = trans('messages.update_success');

            return $this->response($result, HTTP_OK, $message);
        } catch (\Exception $e) {
            DB::rollBack();

            return $this->response($e->getMessage(), HTTP_INTERNAL_SERVER_ERROR);
        }
    }
}