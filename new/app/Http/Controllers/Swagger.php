<?php

/**
 * @OA\OpenApi(
 *     @OA\Info(
 *         version="1.0.0",
 *         title="Document Api",
 *         @OA\License(name="MIT")
 *     ),
 * ),
 *
 * @OA\Server(
 *         description="Https server",
 *         url="https://lumen.gsmpartscenter.com/api/v1",
 *     ),
 */