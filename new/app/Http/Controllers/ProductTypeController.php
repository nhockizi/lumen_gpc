<?php

namespace App\Http\Controllers;

use App\Http\Controllers\TraitController\ErrorTrait;
use App\Repositories\ProductTypeRepo;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Validator;

class ProductTypeController extends Controller
{
    protected $_product_type;
    use ErrorTrait;
    public function __construct(
        ProductTypeRepo $productTypeRepo
    ) {
        $this->_product_type = $productTypeRepo;
    }
    /**
     * @OA\Get(
     *     path="/product-type/list",
     *     summary="List all Product Type",
     *     tags={"Product Type"},
     *     operationId="product_type_list",
     *     security={{"Bearer":{}}},
     *     @OA\Parameter(
     *         description="page",
     *         in="query",
     *         name="page",
     *         @OA\Schema(
     *           type="integer",
     *           format="int32"
     *         )
     *     ),
     *     @OA\Parameter(
     *         description="number item length",
     *         in="query",
     *         name="length",
     *         @OA\Schema(
     *           type="integer",
     *           format="int32"
     *         )
     *     ),
     *     @OA\Parameter(
     *         description="name",
     *         in="query",
     *         name="name",
     *         @OA\Schema(
     *           type="string",
     *         )
     *     ),
     *     @OA\Parameter(
     *         description="code",
     *         in="query",
     *         name="code",
     *         @OA\Schema(
     *           type="string",
     *         )
     *     ),
     *     @OA\Response(
     *          response="200",
     *          description="{
    'status': true,
    'message': null,
    'data': object
    }",
     *     ),
     *     @OA\Response(
     *          response="401",
     *          description="{
    'status': false,
    'message': 'Invalid token',
    'data': []
    }",
     *     ),
     * )
     */
    public function list(Request $request){
        try {
            $request_all = $request->all();
            $data        = [
                'length'        => array_get($request_all, 'length', null),
                'filter_search' => array_get($request_all, 'filter_search', null),
                'order'         => array_get($request_all, 'order', null),
                'type'          => array_get($request_all, 'type', null),
            ];
            $result      = $this->_product_type->list($data);

            return $this->response($result);
        } catch (\Exception $e) {

            return $this->response($e->getMessage(), Response::HTTP_BAD_REQUEST);
        }
    }

    public function delete($id)
    {
        DB::beginTransaction();
        try {
            $result = $this->_product_type->find($id);
            if (!$result) {
                throw new \Exception(trans('messages.data_not_found'));
            }
            $result->delete();
            DB::commit();
            $message = trans('messages.delete_success');

            return $this->response($message);
        } catch (\Exception $e) {
            DB::rollBack();

            return $this->response($e->getMessage(), Response::HTTP_BAD_REQUEST);
        }
    }

    public function create(Request $request)
    {
        $data    = $this->_data();
        $message = '';
        $this->_validate($data, $message);
        if ($message != '') {
            return $this->response(
                $message,
                Response::HTTP_INTERNAL_SERVER_ERROR
            );
        }
        DB::beginTransaction();
        try {
            $result = $this->_product_type->_save($data);
            DB::commit();
            $message = trans('messages.create_success');

            return $this->response($result, Response::HTTP_OK, $message);
        } catch (\Exception $e) {
            DB::rollBack();

            return $this->response($e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    private function _data($id = null)
    {
        $request     = Request::capture();
        $request_all = $request->all();
        $result      = [
            'id'             => $id,
            'name'           => array_get($request_all, 'name', null),
            'code'           => array_get($request_all, 'code', null),
            'description_nl' => array_get($request_all, 'description_nl', null),
            'description_en' => array_get($request_all, 'description_en', null),
        ];

        return $result;
    }

    private function _validate($data, &$message)
    {
        $rules = [
            'name' => [
                'required',
            ],
            'code' => [
                'required',
            ],
        ];
        $this->_validate_error($data, $rules, $message);

        return $message;
    }

    public function update($id, Request $request)
    {
        $data    = $this->_data($id);
        $message = '';
        $this->_validate($data, $message);
        if ($message != '') {
            return $this->response(
                $message,
                Response::HTTP_INTERNAL_SERVER_ERROR
            );
        }
        DB::beginTransaction();
        try {
            $result = $this->_product_type->_save($data);
            DB::commit();
            $message = trans('messages.update_success');

            return $this->response($result, Response::HTTP_OK, $message);
        } catch (\Exception $e) {
            DB::rollBack();

            return $this->response($e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
}