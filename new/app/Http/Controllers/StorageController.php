<?php

namespace App\Http\Controllers;

use App\Repositories\StorageRepo;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Maatwebsite\Excel\Facades\Excel;
use Validator;

class StorageController extends Controller
{
    protected $_storage;

    public function __construct(
        StorageRepo $storageRepo
    ) {
        $this->_storage = $storageRepo;
    }

    public function listRack($locationID)
    {
        try {
            $result = $this->_storage->listRack($locationID);

            return $this->response($result);
        } catch (\Exception $e) {

            return $this->response($e->getMessage(), Response::HTTP_BAD_REQUEST);
        }
    }

    public function list(Request $request)
    {
        try {
            $request_all = $request->all();
            $data        = [
                'location_id' => array_get($request_all, 'location_id', null),
                'rack'        => array_get($request_all, 'rack', null),
            ];
            $result      = $this->_storage->list($data);

            return $this->response($result);
        } catch (\Exception $e) {

            return $this->response($e->getMessage(), Response::HTTP_BAD_REQUEST);
        }
    }

    public function exportExcel(Request $request)
    {
        try {
            $request_all = $request->all();
            $data        = [
                'location_id' => array_get($request_all, 'location_id', null),
                'rack'        => array_get($request_all, 'rack', null),
                'rack_index'  => array_get($request_all, 'rack_index', null),
            ];

            return Excel::create(
                'rack_report_'.date('YmdHis'),
                function ($excel) use ($data) {
                    $excel->setTitle('Rack report');
                    $excel->sheet(
                        'sheet 1', function ($sheet) use ($data) {
                        $result = $this->_storage->list($data);
                        $sheet->loadView('storage.rack', ['data' => $result]);
                    }
                    );
                }
            )->download('xlsx', ['Access-Control-Allow-Origin' => '*']);
        } catch (\Exception $e) {

            return $this->response($e->getMessage(), Response::HTTP_BAD_REQUEST);
        }
    }
}