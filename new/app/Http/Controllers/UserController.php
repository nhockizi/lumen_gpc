<?php

namespace App\Http\Controllers;

use App\Http\Controllers\TraitController\ErrorTrait;
use App\Models\Member;
use App\Repositories\UserRepo;
use Illuminate\Http\Request;
use Validator;

class UserController extends Controller
{
    public $_user;

    public function __construct(
        UserRepo $userRepo
    ) {
        $this->_user = $userRepo;
    }

    use ErrorTrait;

    /**
     * @OA\Get(
     *     path="/user/list",
     *     summary="List all User",
     *     tags={"User"},
     *     operationId="user_list",
     *     security={{"Bearer":{}}},
     *     @OA\Parameter(
     *         description="page",
     *         in="query",
     *         name="page",
     *         @OA\Schema(
     *           type="integer",
     *           format="int32"
     *         )
     *     ),
     *     @OA\Parameter(
     *         description="number item length",
     *         in="query",
     *         name="length",
     *         @OA\Schema(
     *           type="integer",
     *           format="int32"
     *         )
     *     ),
     *     @OA\Response(
     *          response="200",
     *          description="{
    'status': true,
    'message': null,
    'data': object
    }",
     *     ),
     *     @OA\Response(
     *          response="401",
     *          description="{
    'status': false,
    'message': 'Invalid token',
    'data': []
    }",
     *     ),
     * )
     */
    public function list(Request $request)
    {
        try {
            $request_all = $request->all();
            $data        = [
                'length' => array_get($request_all, 'length', null),
                'order'  => array_get($request_all, 'order', null),
                'type'   => array_get($request_all, 'type', null),
            ];
            $result      = $this->_user->list($data);

            return $this->response($result);
        } catch (\Exception $e) {
            return $this->response($e->getMessage(), HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @OA\Post(
     *     path="/user/create",
     *     summary="Create User",
     *     tags={"User"},
     *     operationId="user_create",
     *     security={{"Bearer":{}}},
     *     @OA\Parameter(
     *         in="query",
     *         name="username",
     *         @OA\Schema(
     *           type="string",
     *         )
     *     ),
     *     @OA\Parameter(
     *         in="query",
     *         name="password",
     *         @OA\Schema(
     *           type="password",
     *         )
     *     ),
     *     @OA\Parameter(
     *         in="query",
     *         name="email",
     *         @OA\Schema(
     *           type="email",
     *         )
     *     ),
     *     @OA\Parameter(
     *         in="query",
     *         name="role",
     *         @OA\Schema(
     *           type="int",
     *         )
     *     ),
     *     @OA\Parameter(
     *         in="query",
     *         name="using_api",
     *         @OA\Schema(
     *           type="boolean",
     *         )
     *     ),
     *     @OA\Parameter(
     *         in="query",
     *         name="active",
     *         @OA\Schema(
     *           type="boolean",
     *         )
     *     ),
     *     @OA\Response(
     *          response="200",
     *          description="{
    'status': true,
    'message': null,
    'data': object
    }",
     *     ),
     *     @OA\Response(
     *          response="401",
     *          description="{
    'status': false,
    'message': 'Invalid token',
    'data': []
    }",
     *     ),
     * )
     */
    public function create()
    {
        $data    = $this->_data();
        $message = $this->_validate($data);
        if ($message != '') {
            return $this->response(
                $message,
                HTTP_INTERNAL_SERVER_ERROR
            );
        }
        try {
            $result  = $this->_user->create($data);
            $message = trans('messages.create_success');

            return $this->response($result, HTTP_OK, $message);
        } catch (\Exception $e) {
            return $this->response($e->getMessage(), HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    private function _data($id = null)
    {
        $request_all = Request::capture()->all();
        $data        = [
            'id'        => $id,
            'email'     => array_get($request_all, 'email', null),
            'password'  => array_get($request_all, 'password', null),
            'username'  => array_get($request_all, 'username', null),
            'role'      => array_get($request_all, 'role', null),
            'using_api' => array_get($request_all, 'using_api', null),
            'active'    => array_get($request_all, 'active', null),
        ];
        if ($id != null) {
            unset($data['email']);
            unset($data['username']);
            if ($data['password'] === null) {
                unset($data['password']);
            } else {
                $data['password'] = md5($data['password']);
            }
        } else {
            $data['password'] = md5($data['password']);
        }

        return $data;
    }

    private function _validate($data)
    {
        $rules        = [
            'role'      => [
                'required',
                'exists:roles,id',
            ],
            'using_api' => [
                'required',
                'boolean',
            ],
            'active'    => [
                'required',
                'boolean',
            ],
        ];
        $rules_create = [];
        if ($data['id'] == null) {
            $rules_create['email']    = [
                'required',
                'email',
                'unique:member,email',
            ];
            $rules_create['username'] = [
                'required',
                'unique:member,username',
            ];
        }
        if (isset($data['password'])) {
            $rules_create['password'] = [
                'required',
            ];
        }
        $rules = array_merge($rules, $rules_create);
        $this->_validate_error($data, $rules, $message);

        return $message;
    }

    /**
     * @OA\Post(
     *     path="/user/update/{user_id}",
     *     summary="Update User",
     *     tags={"User"},
     *     operationId="user_update",
     *     security={{"Bearer":{}}},
     *     @OA\Parameter(
     *         in="path",
     *         name="user_id",
     *         @OA\Schema(
     *           type="integer",
     *           format="int32"
     *         )
     *     ),
     *     @OA\Parameter(
     *         in="query",
     *         name="password",
     *         @OA\Schema(
     *           type="password",
     *         )
     *     ),
     *     @OA\Parameter(
     *         in="query",
     *         name="role",
     *         @OA\Schema(
     *           type="int",
     *         )
     *     ),
     *     @OA\Parameter(
     *         in="query",
     *         name="using_api",
     *         @OA\Schema(
     *           type="boolean",
     *         )
     *     ),
     *     @OA\Parameter(
     *         in="query",
     *         name="active",
     *         @OA\Schema(
     *           type="boolean",
     *         )
     *     ),
     *     @OA\Response(
     *          response="200",
     *          description="{
    'status': true,
    'message': null,
    'data': object
    }",
     *     ),
     *     @OA\Response(
     *          response="401",
     *          description="{
    'status': false,
    'message': 'Invalid token',
    'data': []
    }",
     *     ),
     * )
     */
    public function update($id)
    {
        $check = Member::find($id);
        if (!$check) {
            return $this->response(trans('data_not_found'), HTTP_BAD_REQUEST);
        }
        $data    = $this->_data($id);
        $message = $this->_validate($data);
        if ($message != '') {
            return $this->response(
                $message,
                HTTP_INTERNAL_SERVER_ERROR
            );
        }
        try {
            $result  = $this->_user->update($data, $id);
            $message = trans('messages.update_success');

            return $this->response($result, HTTP_OK, $message);
        } catch (\Exception $e) {
            return $this->response($e->getMessage(), HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @OA\Get(
     *     path="/user/detail/{user_id}",
     *     summary="Detail User",
     *     tags={"User"},
     *     operationId="user_detail",
     *     security={{"Bearer":{}}},
     *     @OA\Parameter(
     *         description="User id",
     *         in="path",
     *         name="user_id",
     *         @OA\Schema(
     *           type="integer",
     *           format="int32"
     *         )
     *     ),
     *     @OA\Response(
     *          response="200",
     *          description="{
    'status': true,
    'message': null,
    'data': object
    }",
     *     ),
     *     @OA\Response(
     *          response="401",
     *          description="{
    'status': false,
    'message': 'Invalid token',
    'data': []
    }",
     *     ),
     * )
     */
    public function get($userID)
    {
        try {
            $result = $this->_user->find($userID);
            if (!$result) {
                return $this->response(trans('data_not_found'), HTTP_BAD_REQUEST);
            }
            $result->roles;
            $result->active    = (boolean)$result->active;
            $result->using_api = (boolean)$result->using_api;

            return $this->response($result);
        } catch (\Exception $e) {

            return $this->response($e->getMessage(), HTTP_BAD_REQUEST);
        }
    }
}