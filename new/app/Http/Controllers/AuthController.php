<?php

namespace App\Http\Controllers;

use App\Libraries\Helpers;
use App\Models\Member;
use App\Services\UserLoggedIn\UserLoggedInFacade;
use App\User;
use Firebase\JWT\JWT;
use Illuminate\Http\Request;
use Validator;

class AuthController extends Controller
{
    /**
     * The request instance.
     *
     * @var \Illuminate\Http\Request
     */
    private $request;

    /**
     * Create a new controller instance.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return void
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    /**
     * @OA\Post(
     *     path="/auth/login",
     *     summary="Login User",
     *     tags={"Authentication"},
     *     operationId="Authentication_Login",
     *     @OA\RequestBody(
     *         required=true,
     *         @OA\MediaType(
     *              mediaType="application/x-www-form-urlencoded",
     *              @OA\Schema(
     *               type="object",
     *                  @OA\Property(
     *                      property="username",
     *                      description="User Name",
     *                      type="string",
     *                  ),
     *                  @OA\Property(
     *                      property="password",
     *                      description="Password",
     *                      type="string",
     *                  ),
     *              )
     *         )
     *     ),
     *     @OA\Response(
     *          response="200",
     *          description="{
    'status': true,
    'message': null,
    'data': object
    }",
     *     ),
     *     @OA\Response(
     *          response="500",
     *          description="{
    'status': false,
    'message': 'Oops! The account or password you entered is incorrect',
    'data': []
    }",
     *     ),
     * )
     */

    public function login()
    {
        $this->validate(
            $this->request, [
                              'username' => 'required',
                              'password' => 'required',
                          ]
        );
        $msg = trans('Oops! The account or password you entered is incorrect');
        // Find the user by email
        $user = Member::where('username', $this->request->input('username'))
                      ->where('using_api', ACTIVE)
                      ->where('active', ACTIVE)
                      ->first();
        if (isset($user) && hash("sha256", $this->request->input('password')) == $user->password) {
            //        if (Hash::check($this->request->input('password'), $user->password)) {
            $result = [
                'token' => $this->jwt($user),
                'info'  => [
                    'user_name' => $user->username,
                    'email'     => $user->email,
                ],
            ];
            Helpers::saveLog('login', 'Logged in', $user->member_ID, true);
            $msg = trans('login_successfull');

            return $this->response($result, HTTP_OK, $msg);
        }

        return $this->response($msg, HTTP_BAD_REQUEST);
    }

    /**
     * Create a new token.
     *
     * @param  \App\User $user
     *
     * @return string
     */
    protected function jwt(Member $user)
    {
        $payload = [
            'iss' => "lumen-jwt",
            // Issuer of the token
            'sub' => $user->member_ID,
            // Subject of the token
            'iat' => time(),
            // Time when JWT was issued.
            'exp' => time() + env('JWT_TTL')
            // Expiration time
        ];

        // As you can see we are passing `JWT_SECRET` as the second parameter that will
        // be used to decode the token in the future.
        return JWT::encode($payload, env('JWT_SECRET'));
    }

    public function user()
    {
        return $this->response(
            [
                'user' => UserLoggedInFacade::getUser(),
            ]
        );
    }

    /**
     * @OA\Get(
     *     path="/auth/permission",
     *     summary="Permission User",
     *     tags={"Authentication"},
     *     operationId="Authentication_permission",
     *     security={{"Bearer":{}}},
     *     @OA\Response(
     *          response="200",
     *          description="{
    'status': true,
    'message': null,
    'data': object
    }",
     *     ),
     *     @OA\Response(
     *          response="401",
     *          description="{
    'status': false,
    'message': 'Invalid token',
    'data': []
    }",
     *     ),
     * )
     */
    public function permission()
    {
        $user       = UserLoggedInFacade::getUser();
        $permission = \App\Models\RolesController::with(
            [
                'roles_permission' => function ($q) use ($user) {
                    $q->where('roles_id', $user->role);
                },
            ]
        )
                                                 ->whereHas(
                                                     'roles_permission', function ($q) use ($user) {
                                                     $q->where('roles_id', $user->role);
                                                 }
                                                 )
                                                 ->get();
        $result = [
            'roles' => $user->roles,
            'permissions' => [],
        ];
        foreach ($permission as $item) {
            $function = [];
            foreach ($item->roles_permission as $roles_permission) {
                $function[] = $roles_permission->roles_function->name ?? null;
            }
            $tmp      = (object)[
                $item->name => $function,
            ];
            $result['permissions'][] = $tmp;
        }

        return $this->response($result);
    }
}