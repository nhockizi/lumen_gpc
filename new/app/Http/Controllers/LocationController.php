<?php

namespace App\Http\Controllers;

use App\Http\Controllers\TraitController\ErrorTrait;
use App\Repositories\LocationRepo;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Validator;

class LocationController extends Controller
{
    protected $_location;
    use ErrorTrait;

    public function __construct(
        LocationRepo $locationRepo
    ) {
        $this->_location = $locationRepo;
    }
    /**
     * @OA\Get(
     *     path="/location/list",
     *     summary="List all Location",
     *     tags={"Location"},
     *     operationId="location_list",
     *     security={{"Bearer":{}}},
     *     @OA\Parameter(
     *         description="page",
     *         in="query",
     *         name="page",
     *         @OA\Schema(
     *           type="integer",
     *           format="int32"
     *         )
     *     ),
     *     @OA\Parameter(
     *         description="number item length",
     *         in="query",
     *         name="length",
     *         @OA\Schema(
     *           type="integer",
     *           format="int32"
     *         )
     *     ),
     *     @OA\Parameter(
     *         description="name",
     *         in="query",
     *         name="name",
     *         @OA\Schema(
     *           type="string",
     *         )
     *     ),
     *     @OA\Response(
     *          response="200",
     *          description="{
    'status': true,
    'message': null,
    'data': object
    }",
     *     ),
     *     @OA\Response(
     *          response="401",
     *          description="{
    'status': false,
    'message': 'Invalid token',
    'data': []
    }",
     *     ),
     * )
     */
    public function list(Request $request){
        try {
            $request_all = $request->all();
            $data        = [
                'length'        => array_get($request_all, 'length', null),
                'filter_search' => array_get($request_all, 'filter_search', null),
                'order'         => array_get($request_all, 'order', null),
                'type'          => array_get($request_all, 'type', null),
            ];
            $result      = $this->_location->list($data);

            return $this->response($result);
        } catch (\Exception $e) {

            return $this->response($e->getMessage(), Response::HTTP_BAD_REQUEST);
        }
    }

    public function create(Request $request)
    {
        $data    = $this->_data();
        $message = '';
        $this->_validate($data, $message);
        if ($message != '') {
            return $this->response(
                $message,
                Response::HTTP_INTERNAL_SERVER_ERROR
            );
        }
        DB::beginTransaction();
        try {
            $result = $this->_location->_save($data);
            DB::commit();
            $message = trans('messages.create_success');

            return $this->response($result, Response::HTTP_OK, $message);
        } catch (\Exception $e) {
            DB::rollBack();

            return $this->response($e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    private function _data($id = null)
    {
        $request     = Request::capture();
        $request_all = $request->all();
        $result      = [
            'id'         => $id,
            'name'       => array_get($request_all, 'name', null),
            'country'    => array_get($request_all, 'country', null),
            'province'   => array_get($request_all, 'province', null),
            'postalcode' => array_get($request_all, 'postal_code', null),
            'street'     => array_get($request_all, 'street', null),
            'additional' => array_get($request_all, 'additional', null),
        ];

        return $result;
    }

    private function _validate($data, &$message)
    {
        $rules = [
            'name'       => [
                'required',
            ],
            'country'    => [
                'required',
            ],
            'province'   => [
                'required',
            ],
            'postalcode' => [
                'required',
            ],
            'street'     => [
                'required',
            ],
        ];
        $this->_validate_error($data, $rules, $message);

        return $message;
    }

    public function update($id, Request $request)
    {
        $data    = $this->_data($id);
        $message = '';
        $this->_validate($data, $message);
        if ($message != '') {
            return $this->response(
                $message,
                Response::HTTP_INTERNAL_SERVER_ERROR
            );
        }
        DB::beginTransaction();
        try {
            $result = $this->_location->_save($data);
            DB::commit();
            $message = trans('messages.update_success');

            return $this->response($result, Response::HTTP_OK, $message);
        } catch (\Exception $e) {
            DB::rollBack();

            return $this->response($e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

}