<?php

namespace App\Http\Controllers;

use App\Http\Controllers\TraitController\ErrorTrait;
use App\Repositories\HistoryProductRepo;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Validator;

class HistoryProductController extends Controller
{
    protected $historyProduct;
    use ErrorTrait;

    public function __construct(
        HistoryProductRepo $historyProductRepo
    ) {
        $this->historyProduct = $historyProductRepo;
    }

    /**
     * @OA\Get(
     *     path="/history-product/list",
     *     summary="List all History Product",
     *     tags={"Location"},
     *     operationId="history_product_list",
     *     security={{"Bearer":{}}},
     *     @OA\Parameter(
     *         description="page",
     *         in="query",
     *         name="page",
     *         @OA\Schema(
     *           type="integer",
     *           format="int32"
     *         )
     *     ),
     *     @OA\Parameter(
     *         description="number item length",
     *         in="query",
     *         name="length",
     *         @OA\Schema(
     *           type="integer",
     *           format="int32"
     *         )
     *     ),
     *     @OA\Response(
     *          response="200",
     *          description="{
    'status': true,
    'message': null,
    'data': object
    }",
     *     ),
     *     @OA\Response(
     *          response="401",
     *          description="{
    'status': false,
    'message': 'Invalid token',
    'data': []
    }",
     *     ),
     * )
     */
    public function list(Request $request)
    {
        try {
            $request_all = $request->all();
            $data        = [
                'length'        => array_get($request_all, 'length', null),
                'filter_search' => array_get($request_all, 'filter_search', null),
                'order'         => array_get($request_all, 'order', null),
                'type'          => array_get($request_all, 'type', null),
            ];
            $result      = $this->historyProduct->list($data);

            return $this->response($result);
        } catch (\Exception $e) {

            return $this->response($e->getMessage(), Response::HTTP_BAD_REQUEST);
        }
    }

    public function getByProduct($productID)
    {
        try {
            $result = $this->historyProduct->getByProduct($productID);
            if (!$result) {
                throw new \Exception(trans('messages.data_not_found'));
            }

            return $this->response($result);
        } catch (\Exception $e) {

            return $this->response($e->getMessage(), Response::HTTP_BAD_REQUEST);
        }
    }

    public function rollback(Request $request)
    {
        try {
            $request_all = $request->all();
            $data        = [
                'id'        => array_get($request_all, 'id', null),
                'type' => array_get($request_all, 'type', false),
            ];
            $check = $this->historyProduct->find($data['id']);
            if(!$check){
                throw new \Exception(trans('messages.data_not_found'));
            }
            $this->historyProduct->rollback($data);
            $message = trans('messages.rollback_success');
            return $this->response($message);
        } catch (\Exception $e) {

            return $this->response($e->getMessage(), Response::HTTP_BAD_REQUEST);
        }
    }
}