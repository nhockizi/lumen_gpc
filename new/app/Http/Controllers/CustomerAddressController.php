<?php

namespace App\Http\Controllers;

use App\Repositories\BrandRepo;
use App\Repositories\CustomerAddressRepo;
use App\Repositories\CustomerRepo;
use App\Repositories\LocationRepo;
use App\Repositories\ProductRepo;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Validator;

class CustomerAddressController extends Controller
{
    protected $_customerAddress;

    public function __construct(
        CustomerAddressRepo $customerAddressRepo
    ) {
        $this->_customerAddress = $customerAddressRepo;
    }
    /**
     * @OA\Get(
     *     path="/customer-address/list",
     *     summary="List all Customer Address",
     *     tags={"Customer Address"},
     *     operationId="customer_address_list",
     *     security={{"Bearer":{}}},
     *     @OA\Parameter(
     *         description="page",
     *         in="query",
     *         name="page",
     *         @OA\Schema(
     *           type="integer",
     *           format="int32"
     *         )
     *     ),
     *     @OA\Parameter(
     *         description="number item length",
     *         in="query",
     *         name="length",
     *         @OA\Schema(
     *           type="integer",
     *           format="int32"
     *         )
     *     ),
     *     @OA\Parameter(
     *         description="name",
     *         in="query",
     *         name="name",
     *         @OA\Schema(
     *           type="string",
     *         )
     *     ),
     *     @OA\Response(
     *          response="200",
     *          description="{
    'status': true,
    'message': null,
    'data': object
    }",
     *     ),
     *     @OA\Response(
     *          response="401",
     *          description="{
    'status': false,
    'message': 'Invalid token',
    'data': []
    }",
     *     ),
     * )
     */
    public function list(){
        try {
            $result = $this->_customerAddress->list();

            return $this->response($result);
        } catch (\Exception $e) {

            return $this->response($e->getMessage(), Response::HTTP_BAD_REQUEST);
        }
    }

    public function create()
    {
        $request = Request::capture()->all();
        $data    = [
            'id'               => null,
            'customer_id'      => array_get($request, 'customer_id', null),
            'street'           => array_get($request, 'street', null),
            'city'             => array_get($request, 'city', null),
            'postal_code'      => array_get($request, 'postal_code', null),
            'country'          => array_get($request, 'country', null),
            'telephone'        => array_get($request, 'telephone', null),
            'billing_default'  => array_get($request, 'billing_default', null),
            'shipping_default' => array_get($request, 'shipping_default', null),
        ];
        DB::beginTransaction();
        try {
            $_customer             = CustomerRepo::getInstance();
            $customer              = $_customer->find($data['customer_id']);
            $data['customer_name'] = $customer->name;
            $result                = $this->_customerAddress->_save($data);

            DB::commit();
            $messages = trans('messages.create_success');

            return $this->response($result, Response::HTTP_OK, $messages);
        } catch (\Exception $e) {
            DB::rollBack();

            return $this->response($e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function update($id)
    {
        $request = Request::capture()->all();
        $data    = [
            'id'               => $id,
            'customer_id'      => array_get($request, 'customer_id', null),
            'street'           => array_get($request, 'street', null),
            'city'             => array_get($request, 'city', null),
            'postal_code'      => array_get($request, 'postal_code', null),
            'country'          => array_get($request, 'country', null),
            'telephone'        => array_get($request, 'telephone', null),
            'billing_default'  => array_get($request, 'billing_address', null),
            'shipping_default' => array_get($request, 'shipping_address', null),
        ];
        DB::beginTransaction();
        try {
            $_customer             = CustomerRepo::getInstance();
            $customer              = $_customer->find($data['customer_id']);
            $data['customer_name'] = $customer->name;
            $result                = $this->_customerAddress->_save($data);

            DB::commit();
            $messages = trans('messages.update_success');

            return $this->response($result, Response::HTTP_OK, $messages);
        } catch (\Exception $e) {
            DB::rollBack();

            return $this->response($e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function delete($id)
    {
        DB::beginTransaction();
        try {
            $result                = $this->_customerAddress->delete($id);

            DB::commit();
            $messages = trans('messages.delete_success');

            return $this->response($result, Response::HTTP_OK, $messages);
        } catch (\Exception $e) {
            DB::rollBack();

            return $this->response($e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
}