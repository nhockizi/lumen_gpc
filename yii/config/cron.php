<?php
    define('STATUS_INACTIVE', 0);
    define('STATUS_ACTIVE', 1);
    define('ADMIN_ID', 4);
    define('ADMIN_ROLE_ID', 1);
    $TABLE_PREFIX = '';
    $MYSQL_HOSTNAME = '127.0.0.1'; //Your hostname here
    $MYSQL_USERNAME = 'root'; //Your username here
    $MYSQL_PASSWORD = 'yymv1f1auWrvmibNmCiWmE'; //Your password here
    $MYSQL_DATABASE = 'system_yii'; //Your database
    return array(
        // This path may be different. You can probably get it from `config/main.php`.
        'basePath' => dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
        'name'     => 'Cron',

        'preload' => array('log'),

        'import'     => array(
            'application.components.*',
            'application.models.*',
        ),
        // We'll log cron messages to the separate files
        'components' => array(
            'log' => array(
                'class'  => 'CLogRouter',
                'routes' => array(
                    array(
                        'class'   => 'CFileLogRoute',
                        'logFile' => 'cron.log',
                        'levels'  => 'error, warning',
                    ),
                    array(
                        'class'   => 'CFileLogRoute',
                        'logFile' => 'cron_trace.log',
                        'levels'  => 'trace',
                    ),
                ),
            ),

            // Your DB connection
            'db'  => array(
                'connectionString'   => "mysql:host=$MYSQL_HOSTNAME;dbname=$MYSQL_DATABASE",
                'emulatePrepare'     => true,
                'username'           => $MYSQL_USERNAME,
                'password'           => $MYSQL_PASSWORD,
                'tablePrefix'        => $TABLE_PREFIX,
                'charset'            => 'utf8',
                'enableProfiling'    => true,
                'enableParamLogging' => true,
            ),
        ),
    );