<?php

include_once 'config_system.php';


$THEME_NAME = 'System';
$THEME      = 'system';
$TABLE_PREFIX = '';

return array(
    'basePath' => dirname(__FILE__) . DIRECTORY_SEPARATOR . '..',
    'theme' => $THEME,
    'language' => 'en',
    // preloading 'log' component
    'preload' => array('log', 'ELangHandler'),
    // autoloading model and component classes
    'import' => array(
        'application.models.*',
        'application.models.form.*',
        'application.components.*',
        'application.components.widget.*',
        'application.components.widget.view.*',
        'application.components.BaseFormatter',
        'application.components.helper.*',
        'application.extensions.phpexcel.*',
        'application.extensions.phpexcel.PHPExcel.*',
        'application.extensions.tcpdf.*',
        'application.extensions.filecsv.*',
    ),
    'modules' => array(
        'gii' => array(
            'class' => 'application.modules.gii.GiiModule',
            'password' => 'admin1234',
            // If removed, Gii defaults to localhost only. Edit carefully to taste.
            'ipFilters' => array('127.0.0.1', '::1'),
            'generatorPaths' => array(
                'application.modules.gii', // a path alias
            ),
        ),
    ),
    // application components
    'components' => array(
        // 'session' => array(
        //     'autoStart' => true,
        //     'class' => 'CDbHttpSession',
        //     'timeout' => 11800,
        // ),
        'urlManager' => array(
            'urlFormat' => 'path',
            'rules' => array(
                '<action:(error|login|logout)>' => 'site/<action>',
                '<controller:\w+>/<id:\d+>' => '<controller>/view',
                '<controller:\w+>/<action:\w+>/<id:\d+>' => '<controller>/<action>',
                '<controller:\w+>/<action:\w+>' => '<controller>/<action>',
            ),
            'showScriptName' => false,

        ),
        'db' => array(
            'connectionString' => "mysql:host=$MYSQL_HOSTNAME;dbname=$MYSQL_DATABASE",
            'emulatePrepare' => true,
            'username' => $MYSQL_USERNAME,
            'password' => $MYSQL_PASSWORD,
            'tablePrefix' => $TABLE_PREFIX,
            'charset' => 'utf8',
            'enableProfiling' => true,
            'enableParamLogging' => true,
        ),
        'dbkz' => array(
            'connectionString' => "mysql:host=$MYSQL_HOSTNAME_KZ;dbname=$MYSQL_DATABASE_KZ",
            'emulatePrepare' => true,
            'username'         => $MYSQL_USERNAME_KZ,
            'password'         => $MYSQL_PASSWORD_KZ,
            'class'            => 'CDbConnection'          // DO NOT FORGET THIS!
        ),
        'authManager' => array(
            'class' => 'CDbAuthManager',
            'connectionID' => 'db',
        ),
        'errorHandler' => array(
            // use 'site/error' action to display errors
            'errorAction' => 'site/error',
        ),
        'log' => array(
            'class' => 'CLogRouter',
            'routes' => array(
                array(
                    'class' => 'CFileLogRoute',
                    'levels'=>'error, warning, error, info',
                    'categories' => 'system.*',
                    // 'logFile' => 'db.log',
                ),
                array(
                    'class' => 'CDbLogRoute',
                    'connectionID' => 'db',
                    'autoCreateLogTable' => false,
                    'logTableName' => "logger",
                    'levels' => 'info, error, invoice, error_invoice'
                )
            ),
        ),
        'widgetFactory' => array(
            'widgets' => array(
                'XUpload'   => array(
                    'formView'     => 'application.views.layouts.inc.upload_form',
                    'downloadView' => 'application.views.layouts.inc.download_template',
                    'uploadView'   => 'application.views.layouts.inc.upload_template',
                    'options'      => array(
                        'maxFileSize'     => 3 * 1024 * 1024,
                        'acceptFileTypes' => 'js:/\.(jpg|jpeg|png|gif)$/i'
                    )
                ),
            ),
        ),
        'metadata' => array('class' => 'Metadata'),
        'format' => array(
            'class' => 'BaseFormatter'
        ),

        'user'=>array(
            'allowAutoLogin' => true,
            'class' => 'WebUser',
            // this is actually the default value
            'loginUrl'=>array('site/login'),
        ),
        'ePdf' => array(
            'class'         => 'ext.yii-pdf.EYiiPdf',
            'params'        => array(
                'mpdf'     => array(
                    'librarySourcePath' => 'application.vendors.mpdf.*',
                    'constants'         => array(
                        '_MPDF_TEMP_PATH' => Yii::getPathOfAlias('application.runtime'),
                    ),
                    'classFile'=>'mpdf.php', // the literal class filename to be loaded from the vendors folder
                    //'class' => 'mPDF',
                    'defaultParams'     => array( // More info: http://mpdf1.com/manual/index.php?tid=184
                        'mode'              => '', //  This parameter specifies the mode of the new document.
                        'format'            => 'A4', // format A4, A5, ...
                        'default_font_size' => 0, // Sets the default document font size in points (pt)
                        'default_font'      => '', // Sets the default font-family for the new document.
                        'mgl'               => 15, // margin_left. Sets the page margins for the new document.
                        'mgr'               => 15, // margin_right
                        'mgt'               => 25, // margin_top
                        'mgb'               => 16, // margin_bottom
                        'mgh'               => 0, // margin_header
                        'mgf'               => 9, // margin_footer
                        'orientation'       => 'P', // landscape or portrait orientation
                    )
                ),
            ),
        ),
    ),
    'aliases'    => array(
      'xupload' => 'application.widgets.xupload'
    ),
    // application-level parameters that can be accessed
    // using Yii::app()->params['paramName']
    'params' => array(
        'magento' => array(
            'consumer_key' => 'e6cf179da12ad0dbc58460e990ffdba2',
            'consumer_secret' => 'c80d4035be64be5dd72fc0e3e372a9cf'
        ),
    ),
);
